import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sp

start = 0.1
stop = 10
steps = 1000
stepsize = (stop - start) / steps

x = np.arange(start, stop, stepsize)
x = np.append(x,stop)


plt.close('all')

# linear x and y axis
plt.plot([0,10],[0,0], color="black")
plt.plot(x, sp.yn(0,x))
plt.plot(x, sp.yn(1,x))
plt.plot(x, sp.yn(2,x))
plt.plot(x, sp.yn(3,x))
plt.plot(x, sp.yn(4,x))
plt.plot(x, sp.yn(5,x))

plt.title('Bessel Functions of the Second Kind')
plt.xlabel("$x$")
plt.ylabel("$Y_n(x)$")
plt.text(2, 0.6, "$Y_0(x)$")
plt.text(3.5, 0.48, "$Y_1(x)$")
plt.text(4.7, 0.42, "$Y_2(x)$")
plt.text(6.1, 0.39, "$Y_3(x)$")
plt.text(7.1, 0.37, "$Y_4(x)$")
plt.text(8.6, 0.35, "$Y_5(x)$")
plt.xlim([0,10])
plt.ylim([-1,0.7])

plt.show()