import matplotlib.pyplot as plt
import numpy as np

start = 0
stop = 2
stepsize = 0.05

x = np.arange(start, stop, stepsize)
x = np.append(x,2)
y = np.sqrt(4 - np.power(x,2))

def evenExtension(x2, y2):
    
    y1 = np.copy(y2)
    y1 = np.delete(y1,1)
    #y1 = np.delete(y1,-1)
    y1 = np.flip(y1)

    x1 = np.copy(x2)
    x1 = np.delete(x1,1)
    #x1 = np.delete(x1,-1)
    x1 = np.flip(x1)
    x1 = -1 * x1

    return (np.append(x1,x2), np.append(y1,y2))

def oddExtension(x2, y2):

    y1 = np.copy(y2)
    y1 = np.delete(y1,1)
    #y1 = np.delete(y1,-1)
    y1 = np.flip(y1)
    y1 = -1 * y1

    x1 = np.copy(x2)
    x1 = np.delete(x1,1)
    #x1 = np.delete(x1,-1)
    x1 = np.flip(x1)
    x1 = -1 * x1

    return (np.append(x1,x2), np.append(y1,y2))

def regularExtension(x2, y2):

    y1 = np.copy(y2)
    y1 = np.delete(y1,1)
    #y1 = np.delete(y1,-1)
    
    x1 = np.copy(x2)
    x1 = np.delete(x1,1)
    #x1 = np.delete(x1,-1)
    x1 = np.flip(x1)
    x1 = -1 * x1

    return (np.append(x1,x2), np.append(y1,y2))

def repeatPeriod(x, y):
    
    period = x[-1] - x[0]

    yf = np.copy(y)
    xf = x + period
    
    yb = np.copy(y)
    xb = x - period

    return (np.concatenate((xb,x,xf)), np.concatenate((yb,y,yf)))



plt.close('all')

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2)

ax1.plot(x, y, color="black")
ax1.set_title('original function')
ax1.set_xlim([-5,5])
ax1.set_ylim([-2.5,2.5])
ax1.grid()

(X,Y) = evenExtension(x, y)
(X,Y) = repeatPeriod(X,Y)
ax2.plot(X, Y, color="green")
ax2.set_title('even extension')
ax2.set_xlim([-5,5])
ax2.set_ylim([-2.5,2.5])
ax2.grid()

(X,Y) = oddExtension(x, y)
(X,Y) = repeatPeriod(X,Y)
ax3.plot(X, Y, color="blue")
ax3.set_title('odd extension')
ax3.set_xlim([-5,5])
ax3.set_ylim([-2.5,2.5])
ax3.grid()

(X,Y) = regularExtension(x, y)
(X,Y) = repeatPeriod(X,Y)
ax4.plot(X, Y, color="red")
ax4.set_title('"regular" extension')
ax4.set_xlim([-5,5])
ax4.set_ylim([-2.5,2.5])
ax4.grid()

fig.suptitle("Periodic Function Extensions", fontsize=16)
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)

plt.show()