import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sp

start = 0
stop = 5
steps = 1000
stepsize = (stop - start) / steps

x = np.arange(start, stop, stepsize)
x = np.append(x,stop)


plt.close('all')

plt.plot(x, sp.iv(0,x))
plt.plot(x, sp.iv(1,x))
plt.plot(x, sp.iv(2,x))
plt.plot(x, sp.iv(3,x))
plt.plot(x, sp.iv(4,x))
plt.plot(x, sp.iv(5,x))

plt.title('Modified Bessel Functions of the First Kind')
plt.xlabel("$x$")
plt.ylabel("$I_n(x)$")
plt.legend(["$I_0(x)$", "$I_1(x)$", "$I_2(x)$", "$I_3(x)$", "$I_4(x)$", "$I_5(x)$"])

plt.xlim([0,5])
plt.ylim([0,5])

plt.show()