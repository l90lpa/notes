import matplotlib.pyplot as plt
import numpy as np

start = 0.1
stop = 1000
steps = 10000
stepsize = (stop - start) / steps

x = np.arange(start, stop, stepsize)
expX = np.exp(x)
lnX = np.log(x)

plt.close('all')

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2)

# linear x and y axis
ax1.plot(x, expX)
ax1.plot(x, x)
ax1.plot(x, lnX)
ax1.set_title('linear')
ax1.set_xlim([0,10])
ax1.set_ylim([-1,10])
ax1.grid()


# log x axis
ax2.semilogx(x, expX)
ax2.semilogx(x, x)
ax2.semilogx(x, lnX)
ax2.set_title('semilogx')
ax2.set_xlim([0.1,1000])
ax2.set_ylim([-1,10])
ax2.grid()


# log y axis
ax3.semilogy(x, expX)
ax3.semilogy(x, x)
ax3.semilogy(x, lnX)
ax3.set_title('semilogy')
ax3.set_xlim([-1,10])
ax3.set_ylim([0.1,1000])
ax3.grid()


# log x and y axis
ax4.loglog(x, expX, label="f(x)=exp(x)")
ax4.loglog(x, x, label="f(x)=x")
ax4.loglog(x, lnX, label="f(x)=ln(x)")
ax4.set_title('loglog')
ax4.set_xlim([0.1,1000])
ax4.set_ylim([0.1,1000])
ax4.grid()

plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)

handles, labels = ax4.get_legend_handles_labels()
plt.figlegend(handles, labels, loc=(0.82, 0.75))
fig.subplots_adjust(top=0.9, right=0.8)

plt.show()