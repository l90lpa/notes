import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sp

start = 0
stop = 5
steps = 1000
stepsize = (stop - start) / steps

x = np.arange(start, stop, stepsize)
x = np.append(x,stop)


plt.close('all')

plt.plot(x, sp.kn(0,x))
plt.plot(x, sp.kn(1,x))
plt.plot(x, sp.kn(2,x))
plt.plot(x, sp.kn(3,x))
plt.plot(x, sp.kn(4,x))
plt.plot(x, sp.kn(5,x))

plt.title('Modified Bessel Functions of the Second Kind')
plt.xlabel("$x$")
plt.ylabel("$K_n(x)$")
plt.legend(["$K_0(x)$", "$K_1(x)$", "$K_2(x)$", "$K_3(x)$", "$K_4(x)$", "$K_5(x)$"])
plt.xlim([0,5])
plt.ylim([0,5])

plt.show()