import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sp

start = 0
stop = 10
steps = 1000
stepsize = (stop - start) / steps

x = np.arange(start, stop, stepsize)
x = np.append(x,stop)


plt.close('all')

# linear x and y axis
plt.plot([0,10],[0,0], color="black")
plt.plot(x, sp.jv(0,x))
plt.plot(x, sp.jv(1,x))
plt.plot(x, sp.jv(2,x))
plt.plot(x, sp.jv(3,x))
plt.plot(x, sp.jv(4,x))
plt.plot(x, sp.jv(5,x))

plt.title('Bessel Functions of the First Kind')
plt.xlabel("$x$")
plt.ylabel("$J_n(x)$")
plt.text(0.8, 0.9, "$J_0(x)$")
plt.text(1.8, 0.63, "$J_1(x)$")
plt.text(3, 0.52, "$J_2(x)$")
plt.text(4.15, 0.48, "$J_3(x)$")
plt.text(5.22, 0.44, "$J_4(x)$")
plt.text(6.7, 0.42, "$J_5(x)$")
plt.xlim([0,10])

plt.show()