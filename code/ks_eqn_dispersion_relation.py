import matplotlib.pyplot as plt
import numpy as np

start = 0
stop = 1.2
steps = 1000
stepsize = (stop - start) / steps

x = np.arange(start, stop, stepsize)
y = np.power(x,2) - np.power(x,4)
constant = np.multiply(0,x)

plt.close('all')

# linear x and y axis
plt.plot(x, y)
plt.plot(x, constant, linestyle="dashed", color="black")
plt.title('Kuramoto–Sivashinsky Equation Dispersion Relation')
plt.xlabel("$k$")
plt.ylabel("i$\omega$")
plt.text(0.92, 0.15, "i$\omega=k^2-k^4$")
plt.xlim([0,1.2])

plt.show()