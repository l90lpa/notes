\documentclass[../main.tex]{subfiles}

\usepackage{multicol}

\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{Functions and their Graphs}

    \subsection{Interpreting Graphs with Various Axis Scales}

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=1.0\textwidth]{images/graph_axis_scales.pdf}}
        \caption{Plots of the same functions using various axis scales (the logarithmic axes are in base 10). Furthermore note that, $f(x)=\exp(x)=e^x$, and $f(x)=\ln(x)$ is the natural logarithm. }
    \end{figure*}
    \FloatBarrier

    \subsection{Elementary Functions}

    \textbf{Elementary functions} is the name given to the class of functions made up of the \textbf{algebraic functions} (functions that can be defined as the root of a polynomial) and the \textbf{elementary transcendental functions} i.e. the exponential function, logarithm function, trigonometric functions etc. Transcendental functions are also called nonalgebraic functions.

    \subsection{Graphs}

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=1.0\textwidth]{images/standard-functions-1.png}}
    \end{figure*}
    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=1.0\textwidth]{images/standard-functions-2.png}}
    \end{figure*}
    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=1.0\textwidth]{images/standard-functions-3.png}}
    \end{figure*}
    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=1.0\textwidth]{images/standard-functions-4.png}}
    \end{figure*}
    \FloatBarrier

    \subsection{Special Functions}

    Beyond the class of elementary functions are the \textbf{higher transcendental functions} which are often called the \textbf{special functions}. Some of the special functions have been deemed interesting enough, or crop up often enough that they have been given names, some of which are surveyed here.
    
    \subsection{WIP - Error Functions}
    
    \[\erf(z)=\frac{2}{\sqrt{\pi}}\int^{z}_{0}e^{-s^{2}} ds\]
    \[\erfc(z)=\frac{2}{\sqrt{\pi}}\int^{\infty}_{z}e^{-s^{2}} ds\]
    \[\erfc(z)=1-\erf(z)\]

    \subsection{WIP - Bessel Functions}

    The Bessel functions are the linearly independent solutions of Bessel's differential equation,
    \[x^{2}y''+xy+(x^{2}-v^{2})y=0\]
    and they come in two kinds:
    \begin{itemize}
        \item $J_{v}(x)$, Bessel functions of the first kind
        \item $Y_{v}(x)$, Bessel functions of the second kind
    \end{itemize}
    
    \subsubsection{Series Representation}
    The Bessel function of the first kind has the form,
    \[J_{v}(x)=\sum^{\infty}_{n=0}\frac{(-1)^n}{\Gamma(n+1)\Gamma(n+v+1)}\left(\frac{x}{2}\right)^{2n+v}\]
    The Bessel function of the second kind can be written in terms of the Bessel functions of the first kind and is
    used when $v$ is an integer (since $J_{v}$ and $J_{-v}$ are linearly dependent in this case),
    \[Y_{v}(x)=\frac{J_{v}(x)\cos(\pi v)-J_{-v}(x)}{\sin(\pi v)}\] 

    \begin{figure}[!htb]
        \centering
        \begin{tabular}{|c|c|}
            \hline
            \subfloat[]{\includegraphics[width=0.5\textwidth]{images/bessel_functions_first_kind.pdf}} &
            \subfloat[]{\includegraphics[width=0.5\textwidth]{images/bessel_functions_second_kind.pdf}} \\
            \hline
        \end{tabular}
        \caption{(a) Bessel Functions of the First Kind, (b) Bessel Functions of the Second Kind}
    \end{figure}
    \FloatBarrier

    \subsubsection{Roots}
    When the the index, $v$, is real the Bessel functions $J_{v}(x)$, $J_{v}'(x)$, $Y_{v}(x)$, and $Y_{v}'(x)$ have 
            infinitely many roots all of which are simple except for when there is a root at $x=0$ which might not be simple.
    
    \subsubsection{Linear Independence}
    If $v$ is an integer then the Bessel functions of the first kind are not linearly independent and 
    specifically they satisfy the relation $J_{-v}=(-1)^{v}J_{v}$. However when $v$ is not an integer they are linearly 
    independent and so this relation is not satisfied.

    \subsubsection{Recurrence Relations}
    Recurrence relations involving Bessel functions of the first kind,
    \begin{align*}
        2J'_{v}(x)&=J_{v-1}(x)-J_{v+1}(x)\\
        \frac{2v}{x}J_{v}(x)&=J_{v-1}(x)+J_{v+1}(x)
    \end{align*}
    From which we can establish the following relations,
    \begin{align*}
        \frac{d}{dx}(x^{v}J_{v}(x))&=x^{v}J_{v-1}(x)\\
        \frac{d}{dx}(x^{-v}J_{v}(x))&=x^{-v}J_{v+1}(x)\\
        J_{v}(x)&=\pm J'_{v\pm 1}(x)+\frac{v\pm 1}{x}J_{v\pm 1}(x)
    \end{align*}

    \subsubsection{Orthogonality}

    The Bessel functions of the first kind have the following orthogonality relation. Let $\mu_{vi}$ be the $i$th root 
    of $J_{v}(x)$ then,
    \[\int^{\alpha}_{0}xJ_{v}\left(\frac{\mu_{vp}}{\alpha} x\right)J_{v}\left(\frac{\mu_{vq}}{\alpha} x\right)\;dx=\left\{\begin{tabular}{l l}
        $0$,& if $p\neq q$\\
        $\frac{\alpha^{2}}{2}[J_{v\pm 1}(\mu_{vp})]^{2}$,& if $p=q$
    \end{tabular}\right.\]
    The Bessel functions of the first kind also have another orthogonality relation using the roots of the first-derivative
    of these functions. Let $\lambda_{vi}$ be the $i$th root of $J'_{v}(x)$ then,
    \[\int^{\alpha}_{0}xJ_{v}\left(\frac{\mu_{vp}}{\alpha} x\right)J_{v}\left(\frac{\mu_{vq}}{\alpha} x\right)\;dx=\left\{\begin{tabular}{l l}
        $0$,& if $p\neq q$\\
        $\frac{\alpha^{2}}{2}\left(1-\frac{v^{2}}{\lambda_{vp}^{2}}\right)[J_{v}(\lambda_{vp})]^{2}$,& if $p=q$
    \end{tabular}\right.\]
    \begin{prop*}
        \textbf{Proof of orthogonality}:\newline
        To show the orthogonality consider the Bessel equation,
        \[x^{2}y''+xy'+(a^{2}x^{2}-v^{2})y=0\quad\Rightarrow\quad y''+\frac{1}{x}y'+\left(a^{2}-\frac{v^{2}}{x^{2}}\right)y=0\]
        Then substituting in $J_{v}(ax)$ and rearranging we get,
        \[-\left(\frac{d^{2}}{dx^{2}}+\frac{1}{x}\frac{d}{dx}-\frac{v^{2}}{x^{2}}\right)J_{v}(ax)=a^{2}J_{v}(ax)\]
        So $J_{v}(ax)$ is an eigenfunction of the linear differential operator,
        \[L=-\left(\frac{d^{2}}{dx^{2}}+\frac{1}{x}\frac{d}{dx}-\frac{v^{2}}{x^{2}}\right)\]
        for eigenvalue $a^{2}$. This differential operator can be shown to be formally self-adjoint for the inner product with
        weight function $w(x)=x$ integrated over $[0,\alpha]$. \newline
        \textbf{Note} this means that the inner product has the form,
        \[\langle f,g\rangle=\int^{\alpha}_{0}xf(x)g(x)\;dx\]
        Now we find the orthogonality relation. By Lagrange's/Green's identity with distinct eigenvalue-eigenfunction pairs $(a,J_{v}(ax))$ and 
        $(b,J_{v}(bx))$ we have,
        \[0=\langle J_{v}(ax),L J_{v}(bx)\rangle-\langle L^{*} J_{v}(ax),J_{v}(bx)\rangle=\left[x\left(aJ_{v}(bx)J'_{v}(ax)-bJ'_{v}(bx)J_{v}(ax)\right)\right]^{\alpha}_{0}\]
        The last term is the conjunct, and by Largange's identity this must zero only. From this we have,
        \begin{align*}
            \langle J_{v}(ax),L J_{v}(bx)\rangle-\langle L^{*} J_{v}(ax),J_{v}(bx)\rangle &= \langle J_{v}(ax), b^{2}J_{v}(bx)\rangle-\langle a^{2}J_{v}(ax),J_{v}(bx)\rangle\\
              &= (b^{2}-a^{2})\langle J_{v}(ax), J_{v}(bx)\rangle\\
              &= \left[x\left(aJ_{v}(bx)J'_{v}(ax)-bJ'_{v}(bx)J_{v}(ax)\right)\right]^{\alpha}_{0},\quad\textrm{where this is the conjunct}\\
              &= \alpha\left(aJ_{v}(b\alpha)J'_{v}(a\alpha)-bJ'_{v}(b\alpha)J_{v}(a\alpha)\right)
        \end{align*}
        Let $a=\mu_{p}/\alpha$ and $b=\mu_{q}/\alpha$, then we see that the eigenfunctions are orthogonal (and Largange's identity is satisfied) if $\mu_{p}$ and $\mu_{q}$ 
        are roots of $J_{v}$ if they are roots of $J'_{v}$. Now to find the value $\langle J_{v}(ax),J_{v}(ax)\rangle$. Let
        $\mu_{p}$ be a root of $J_{v}$ (and so $a\alpha$ is a root of $J_{v}$) then,
        \begin{align*}
            \langle J_{v}(ax),J_{v}(ax)\rangle&=\lim_{b\rightarrow a}\frac{\alpha\left(aJ_{v}(b\alpha)J'_{v}(a\alpha)-bJ'_{v}(b\alpha)J_{v}(a\alpha)\right)}{(b^{2}-a^{2})}\\
            &=\lim_{b\rightarrow a}\frac{a\alpha J_{v}(b\alpha)J'_{v}(a\alpha)}{(b^{2}-a^{2})},\quad\textrm{since}J_{v}(a\alpha)=0\\
            &=\lim_{b\rightarrow a}\frac{a\alpha^{2} J'_{v}(b\alpha)J'_{v}(a\alpha)}{2b},\quad\textrm{by L'Hopital's rule (taking derivatives wrt } b\textrm{)} \\
            &=\frac{\alpha^{2}}{2}\left(J'_{v}(a\alpha)\right)^{2}\\
            &=\frac{\alpha^{2}}{2}\left(J_{v+1}(\mu_{p})\right)^{2},\quad\textrm{using the recurrence formula and }a=\mu_{p}/\alpha
        \end{align*}
        Finally we can use the same style argument to find the value of $\langle J_{v}(ax),J_{v}(ax)\rangle$ when $\mu_{p}$ 
        is a root of $J'_{v}$ completing the orthogonality relation.
    \end{prop*}

    \subsubsection{General Solutions of Bessel's Equation}

    The concrete representation of the general solution to Bessel's equaiton depends on the number $v$.\newline
    \newline
    \textbf{If v is non-integer}, then the general solution of Bessel's equation is,
    \[y(x)=c_{1}J_{v}(x)+c_{2}J_{-v}(x)\]
    \textbf{If v is integer}, then $J_{v}$ and $J_{-v}$ might not be linearly independent and so the Bessel function of 
    the second kind is used as the other independent solution. So the general solution has the form,
    \[y(x)=c_{1}J_{v}(x)+c_{2}Y_{v}(x)\]

    \subsubsection{Solving ODEs by Reduction to Bessel's Equation}
    Some ODEs can be solved by finding a transformation to Bessel's equation and then modifying the solution to 
    Bessel's equation to get a solution to the ODE. Here are some common examples:
    \begin{enumerate}
        \item The ODE $x^{2}y''+xy'+(a^{2}x^{2}-v^{2})y=0$, has the general solution,
        \[y(x)=c_{1}J_{v}(ax)+c_{2}Y_{v}(ax)\]
        This is found using the substitution $x=s/a$ and so $s=ax$
        \item The ODE $x^{2}y''+axy'+(x^{2}-v^{2})y=0$, has the general solution,
        \[y(x)=x^{\frac{1-a}{2}}\left[c_{1}J_{n}(ax)+c_{2}Y_{n}(ax)\right],\quad\textrm{where}\quad n^{2}=v^{2}+\frac{1}{4}(a-1)^{2}\]
        This is found using the substitution $y(x)=x^{\frac{1-a}{2}}s(x)$
        \item The ODE $y''-xy=0$, called the \textbf{Airy differential equation}, has the general solution,
        \[y(x)=c_{1}\sqrt{x}J_{\frac{1}{3}}\left(\frac{2}{3}ix^{3/2}\right)+c_{2}\sqrt{x}J_{-\frac{1}{3}}\left(\frac{2}{3}ix^{3/2}\right)\]
        \item The ODE $x^{2}y''+xy'-(x^{2}+v^{2})y=0$, called the \textbf{modified Bessels equation}, has the general solution,
        \begin{align*}
            y(x)&=c_{1}J_{v}(-ix)+c_{2}Y_{v}(-ix)\\
            &=c_{3}I_{v}(x)+c_{4}K_{v}(x)
        \end{align*}
        This is found using the substitution $x=s/-i$ and so $s=-ix$. Note that $I_{v}(x)$ are the 
        \textbf{modified Bessels functions of the first} and $K_{v}(x)$ are the \textbf{modified Bessels functions of the second kind}
        
    \end{enumerate}

    \begin{figure}[!htb]
        \centering
        \begin{tabular}{|c|c|}
            \hline
            \subfloat[]{\includegraphics[width=0.5\textwidth]{images/modified_bessel_functions_first_kind.pdf}} &
            \subfloat[]{\includegraphics[width=0.5\textwidth]{images/modified_bessel_functions_second_kind.pdf}} \\
            \hline
        \end{tabular}
        \caption{(a) Modified Bessel Functions of the First Kind, (b) Modified Bessel Functions of the Second Kind}
    \end{figure}
    \FloatBarrier
    
    \subsection{WIP - Gamma Function}

    The Gamma function is the generalization of the factorial function from integers to complex and real numbers. It is
    defined by the integral,
    \[\Gamma(z)=\int^{\infty}_{0}t^{z-1}e^{-t}\;dt\]
    This integral converges absolutely for $\Re(z)>0$.
    \begin{prop*}
        \textbf{Properties of the Gamma function}:\newline
        Let, $v,z\in\mathbb{C}$ and $n\in\mathbb{N}$ then,
        \begin{enumerate}
            \item $\Gamma(z)$ is defined an analytic in the region $\Re(z)>0$
            \item $\Gamma(n+1)=n!$
            \item $\Gamma(z+1)=z\Gamma(z)$
            \item $\Gamma(n+v+1)=(1+v)(2+v)...(n+v)\Gamma(v+1)$ (easy to show using $z=n+v$ in property 3)
            \item $\Gamma(z)\Gamma(1-z)=\frac{\pi}{\sin(\pi z)}$
        \end{enumerate}
    \end{prop*}

\end{document}