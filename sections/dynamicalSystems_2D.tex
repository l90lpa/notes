\documentclass[../main.tex]{subfiles}



\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{Dynamical Systems}

    An example of a 2D dynamical system, is the coupled system of ODEs,

    \[x'=P(x,y), y'=Q(x,y), \textrm{ where } x=x(t), y=y(t)\]
    A dynamical system is \textbf{autonomous} if the independent variable, $t$, does not explicitly appear.

    \subsection{Stability}

    Consider the n-dimensional dynamical system,
    \[\dot{x}(t)=F(x(t), t),\quad t\ge 0\]
    and let, $x^*$, be a critical point of the dynamical system. The following are definitions of various types of stability:
    \begin{itemize}
    
        \item The solution, $x^*(t)$, is \textbf{Lyapunov stable} for $t\ge t_0$ if and only if, for all $\epsilon > 0$ there exists some $\delta > 0$ such that, 
        \[\Vert x^*(t_0) - x(t_0) \Vert < \delta\Rightarrow\Vert x^*(t) - x(t) \Vert \le \epsilon\]
        for all $t\ge t_0$, where $x(t)$ represents a neighbouring solution.
        
        \item The solution, $x^*(t)$, is \textbf{(locally) asymptotically stable} if, it is Lyapunov stable and addtionally there exists $\eta > 0$ such that 
        \[\Vert x^*(t_0) - x(t_0) \Vert < \eta\Rightarrow\lim_{t\rightarrow\infty}\Vert x^*(t) - x(t) \Vert=0\].
        
        \item The solution, $x^*(t)$, is \textbf{(locally) exponentially stable} if, there exists positive constants $\alpha,\;\beta$ and $\delta$ such that,
        \[\Vert x^*(t_0) - x(t_0) \Vert < \delta\Rightarrow\Vert x^*(t) - x(t) \Vert \le \alpha\Vert x^*(t_0) - x(t_0) \Vert e^{-\beta t}\]
        for all $t\ge t_0$.
        
        \item The solution, $x^*(t)$, is \textbf{globally asymptotically stable} if it is Lyapunov stable and for all $x(t_0)\in\mathbb{R}^n$, $\lim_{t\rightarrow\infty}\Vert x^*(t) - x(t) \Vert=0$.
        
        \item The solution, $x^*(t)$, is \textbf{globally exponentially stable} if there exists positive constants $\alpha$ and $\beta$ such that $\Vert x^*(t) - x(t) \Vert \le \alpha\Vert x^*(t_0) - x(t_0) \Vert e^{-\beta t}$ for all $t\ge 0$ and $x(t_0)\in\mathbb{R}^n$.
        
        \item the critical point \textbf{unstable} if it is not Lyapunov stable.
        
    \end{itemize}
    From the above defintion we note that, 
    \[\textrm{exponentially stable}\Rightarrow\textrm{asymptotically stable}\Rightarrow\textrm{Lyapunov stable}\]
    \subsection{Stability Analysis: Linear Time-Invariant Systems}

    Here we consider only linear systems, $\dot{x}=Ax$, with constant coefficients.

    \begin{theo}
        Given the linear system $\dot{x}=Ax$, where $A$ is a constant matrix with eigenvalues $\{\lambda_1,...,\lambda_n\}$
        \begin{enumerate}
            \item The system is asymptotically stable, if and only if $Re(\lambda_i) < 0\;\;\;\forall\;\;\;i = 1, 2, ... , n$.
            \item If the system is Lyapunov stable, then $Re(\lambda_i) \le 0\;\;\;\forall\;\;\;i = 1, 2, ... , n$.
            \item If $Re(\lambda_i) > 0$ for any $i$, then the system is unstable.
        \end{enumerate}
    \end{theo}
    Recall that the eigenvalues, of $A$ are found by solving,
    $\det(A-\lambda I)=0$ and once we have the eigenvalues, the eigenvectors can be found by solving $(A-\lambda I)v=0$ for each eigenvalue.
    \newline
    \newline
    \textbf{Eigenvalues in the 2D case} are given by,
    \[\lambda=\frac{1}{2}(\tr(A)\pm\sqrt{\tr(A)^{2}-4\det(A)})\]
    Furthermore, it can be shown that, the eigenvalues both have negative real parts if and only if, 
    \[\tr(A)<0 \textrm{   and   } \det(A)>0\]
    \begin{defn}
        \textbf{2D Equilibrium Point Classification Test}:\newline
        Given the eigenvalues $\lambda_{1}$ and $\lambda_{2}$ of the system at equilibrium point $y^{*}$,
        \begin{itemize}{}{}
            \item If the eigenvalues are real and distinct:
                \begin{itemize}{}{}
                    \item If $\lambda_{1}<0$ and $\lambda_{2}>0$ then it is a \textbf{saddle}
                    \item If $\lambda_{1},\lambda_{2}>0$ then it is a \textbf{unstable node}
                    \item If $\lambda_{1},\lambda_{2}<0$ then it is a \textbf{stable node}
                \end{itemize}

            \item If there is a single real eigenvalue (with multiplicity 2):
                \begin{itemize}{}{}
                    \item If there are 2 eigenvectors:
                    \begin{itemize}{}{}
                        \item If $\lambda >0$ then it is a \textbf{unstable node}
                        \item If $\lambda <0$ then it is a \textbf{stable node}
                    \end{itemize}
                    \item If there is only 1 eigenvector:
                    \begin{itemize}{}{}
                        \item If $\lambda >0$ then it is a \textbf{improper unstable node}
                        \item If $\lambda <0$ then it is a \textbf{improper stable node}
                    \end{itemize}
                \end{itemize}
            
            \item If the eigenvalues are complex:
                \begin{itemize}{}{}
                    \item If the real part is zero then it is a \textbf{center}
                    \item If the real part is negative then it is a \textbf{stable spiral}
                    \item If the real part is positive then it is a \textbf{unstable spiral}
                \end{itemize}
        \end{itemize}  
    \end{defn}
    The solutions in the various 2D cases are,
    \begin{itemize}
        \item Real eigenvalues $\lambda_{1}$ and $\lambda_{2}$ such that $\lambda_{1}\neq\lambda_{2}$ with eigenvectors
        $V_{1}$ and $V_{2}$:
        \[X(t)=c_{1}V_{1}e^{\lambda_{1}t}+c_{2}V_{2}e^{\lambda_{2}t}\]
        \item Real eigenvalue $\lambda$ with eigenvectors $V_{1}$ and $V_{2}$:
        \[X(t)=(c_{1}V_{1}+c_{2}V_{2})e^{\lambda t}\]
        \item Real eigenvalue $\lambda$ with eigenvectors $V$ and orthogonal vector $W$ such that $(A-\lambda I)W=V$:
        \[X(t)=(c_{1}V+c_{2}W+c_{2}Vt)e^{\lambda t}\]
        \item Complex eigenvalues $\lambda_{1}=a+bi$ and $\lambda_{2}=a-bi$ with eigenvectors $W+Vi$ and $W-Vi$:
        \[X(t)=[c_{1}(W\cos(bt)-V\sin(bt)) + c_{2}(W\sin(bt)+V\cos(bt))]e^{at}\]
    \end{itemize}

    \subsection{Stability Analysis: Nonlinear Systems}

    \begin{theo}
        \textbf{Lyapunov's Direct Method}:\newline
        Consider the n-dimension nonlinear system $\dot{x}=F(x)$, where $F\in C^1$ and $x^*$ is a equlibrium point of the system. Let $D$ be a subset of the domain such that $x^*\in D$. 
        \begin{itemize}
            \item If there exists a continuously differentiable function $V:D\rightarrow\mathbb{R}$ such that,
            \begin{align*}
                V(x^*)&=0,\\
                V(x)&>0\quad\forall\quad x\in D\backslash\{x^*\},\\
                \nabla V(x)F(x)&\le 0\quad\forall\quad x\in D
            \end{align*}
            then $x^*$ is Lyapunov stable. If alternatively we replace the last condition with,
            \[\nabla V(x)F(x)< 0\quad\forall\quad x\in D\backslash\{x^*\}\quad\textrm{and}\quad \nabla V(x^*)F(x^*)\le 0\]
            then $x^*$ is asymptotically stable.
            \item If there exists a continuously differentiable function $V:D\rightarrow\mathbb{R}$, and parameters $\alpha$, $\beta$, $\varepsilon$, and $p\ge 1$ such that,
            \[\alpha\Vert x-x^*\Vert^p\le V(x) \le\beta\Vert x-x^*\Vert^p,\quad x\in D\]
            \[\nabla V(x)F(x)\le-\varepsilon V(x),\quad x\in D\]
            then $x^*$ is exponentially stable.
        \end{itemize}
        \label{theo:lyapunov_direct}
    \end{theo}

    \begin{theo}
        \textbf{Lyapunov's Indirect Method}:\newline
        Given the n-dimension nonlinear system $\dot{x}=F(x)$, where $F\in C^1$ and $x^*$ is a critical point of the system. Let $J_{x^*}$ be the Jacobian of $F$ at $x^*$, with eigenvalues $\lambda_1,...,\lambda_n$. Then $x^*$ is,
        \begin{enumerate}
            \item exponentially stable if $Re(\lambda_i) < 0\;\;\;\forall\;\;\;i = 1, 2, ... , n$
            \item unstable if $Re(\lambda_i) > 0\;\;\;\textrm{for any}\;\;\;i = 1, 2, ... , n$
        \end{enumerate}
        If any eigenvalues have $Re(\lambda_i)=0$, then further analysis is necessary. 
    \end{theo}
    \textbf{Remark}: Lyapunov's indirect method provides sufficient conditions under which the stability of the nonlinear system at critical points matches the stability of the linearised system at the same critical points.

    \begin{coro}
        Let $(x_0, y_0)$ be an isolated equilibrium point for a (2D) nonlinear system and let $A=A(x_0, y_0)$ be the Jacobian matrix for the linearization (3.1), with $det A \neq 0$. Then $(x_0, y_0)$ is a critical point of the same type and stability as the origin $(0, 0)$ for the linearization in the following cases:
        \begin{enumerate}
            \item The eigenvalues of $A$ are real, either equal or distinct, and have the same
            sign (node).
            \item The eigenvalues of $A$ are real and have opposite signs (saddle).
            \item The eigenvalues of $A$ are complex but not purely imaginary (spiral).
        \end{enumerate}
        The exceptional case is when the linearization has a center. In this case there are several interesting possibilities that can occur in the nonlinear system, and we will investigate some of them. The orbital structure for the nonlinear system near critical points mirrors that of the linearization in the nonexceptional cases indicated in the theorem, with a slight distortion in the phase plane diagram caused by the nonlinearity.
    \end{coro}

    \begin{exmp}
        \textit{Given the following system, find an equilibrium point and linearise the system about this,}
        \[x'=\sin(x)+2y,\quad y'=xy+3e^{x}+x\]
        An equilibrium point must satisfy $x'=y'=0$ and so we need to find $x,y$ that satisfies,
        \[\sin(x)+2y=0,\quad xy+3e^{x}+x=0\]
        Clearly by inspection we see that, $(x^{*},y^{*})=(0,0)$ is an equilibrium point.
        Now we linearise the system about the equilibrium point. Define $x=x^{*}+\delta x$ and $y=y^{*}+\delta y$ and 
        substitue these into the system.
        \[\frac{d}{dt}(x^{*}+\delta x)=\frac{d\delta x}{dt}=\sin(x^{*}+\delta x)+2(y^{*}+\delta y)\]
        \[\frac{d}{dt}(y^{*}+\delta y)=\frac{d\delta y}{dt}=(x^{*}+\delta x)(y^{*}+\delta y)+3(y^{*}+\delta y)e^{(x^{*}+\delta x)}+(x^{*}+\delta x)\]
        Since $(x^{*},y^{*})=(0,0)$ then this simplifies to,
        \[\frac{d\delta x}{dt}=\sin(\delta x)+2(\delta y)\]
        \[\frac{d\delta y}{dt}=(\delta x)(\delta y)+3\delta y e^{(\delta x)}+(\delta x)\]
        Taylor expanding $\sin(\delta x)$ and $e^{\delta x}$ we get,
        \[\frac{d\delta x}{dt}=(\delta x + ...)+2(\delta y)\]
        \[\frac{d\delta y}{dt}=(\delta x)(\delta y)+3\delta y(1+...)+(\delta x)\]
        And keeping only linear terms we arrive at the linearisation,
        \[\frac{d\delta x}{dt}=\delta x+2(\delta y)\]
        \[\frac{d\delta y}{dt}=3\delta y+(\delta x)\]

        
    \end{exmp}

    After linearising the system we can then apply results from linear systems to this to analysis the behaviour of the 
    system.

    \subsection{WIP - Finding a Lyapunov Function}
    \subsubsection{WIP - Sum-of Squares Method (Polynomial Dynamical Systems)}
    Here we will only consider dynamical systems, $\dot{x}=f(x)$, where $F$ is polynomial in components of $x$ (that is $f$ is a polynomial vector field). In some cases one can find a Lyapunov function by hand, but an alternative approach is to use Sum-of-Square programming to solve an equivalent feasibility problem.
    \newline
    \newline
    For $D=\mathbb{R}^n$ the conditions in theorem (\ref{theo:lyapunov_direct}) can be conservatively reformulated as SOS conditions, as follows,
    \begin{theo}
        Consider the n-dimension nonlinear system $\dot{x}=F(x)$, where $F\in C^1$ and the origin is an equlibrium point of the system, and $x\in\mathbb{R}^n$. Also let $\varphi(x)$ be defined as in theorem (\ref{theo:positive_polynomial}). Then if there exists a continuously differentiable function $V:\mathbb{R}^n\rightarrow\mathbb{R}$ such that,
        \begin{align*}
            V(0)&=0,\\
            V(x)-\varphi(x)&\textrm{ is a SOS},\\
            -\nabla V(x)F(x)&\textrm{ is a SOS}
        \end{align*}
        then the origin is (globally) asymptotically stable.
        \newline
        \textbf{Note}: to apply this to an equilibrium other than the origin, first shift the coordinates to move the equilibrium to the origin.
    \end{theo}
    \textbf{Remark}: First note that, SOS polynomials that are not strictly positive only belong to the boundary of the cone of SOS polynomials. And second note that, interior point methods aim for the analytic center of the feasible set, and so are unlikely to find a element of the domain boundary unless the problem is only marginally feasible. 
    
    The, $\varphi(x)$ term in the second condition is an addition to ensure that $V$ is strictly positive. Now, if the second condition were relaxed to, $V(x)\textrm{ is a SOS}$, and an interior point method were used to solve the feasibility problem then it is quite likely that the $V$ found will in fact be strictly positive. The positivity of $V$ could be verifed by checking the eigenvalues of the Gram matrix of $V$ (which arguably one should be doing anyway) for positive definitness. One may choose to solve this simplified feasibility problem as the conditions are less cumbersome and so it may help to speed up the solver.

    \begin{strt}
        Finding a Lyapunov function for the polynomial dynamical system,
        \[\dot{x}=F(x)\]
        To form a concrete feasibility problem, we introduce a \textbf{Lyapunov function candidate}, $V(x,q)$. This is a polynomial whos coefficients, $q$ (a vector of decision variables), will be the decision variables, and so we aim for it to have sufficient structure to be able to represent a Lyapunov function of the system.  
        \newline
        \newline
        The feasibility problem is then,
        \begin{align*}
            &\min_{q\in \mathbb{S}^n}1\\
            &\textrm{subject to:}\\
            &V(x,q)-\varphi(x)\in SOS\\
            &-\nabla V(x,q)F(x)\in SOS
        \end{align*}
        Normally one starts with a Lyapunov function candidate of low degree monomials say all degree 2 monomials and then incrementally includes all addtional higher degree monomials until a Lyapunov function is found, or the search is terminated. Furthermore, due to the structure of the problem, constant and linear terms don't need to be considered.
    \end{strt}

    \subsubsection{WIP - Variable Gradient Method}
    
    The Variable Gradient Method works by first assuming a particular form for the gradient of the Lyapunov function candiate, and then integrating this to find a Lyapunov function.

    \begin{strt}
        \textbf{Variable Gradient Method}\newline
        Given the $n$-dimensional dynamical system,
        \[\dot{x}=F(x)\]
        Define $h:\mathbb{R}^n\rightarrow\mathbb{R}^n$ such that,
        \begin{itemize}
            \item $h(x)=\nabla V(x)$
            \item $\frac{\partial h_i}{\partial x_j}=\frac{\partial h_j}{\partial x_i}\quad\textrm{for}\quad i,j=1,...,n$
            \item $\dot{V}(x)=\frac{\partial V}{\partial x}\dot{x}=h^T F < 0$
        \end{itemize}
        Now one must use creativity to determine a form for $h$ such that it $h(x)$ satsifies the conditions above.
        Finally, $V$ is recovered by computing the line integral,
        \[V(x)=\int^{x}_{x^*} h^T(s)\;ds=\int^{x}_{x^*} \sum^n_{i=1}h_i(s)\;ds_i\]
        recalling that the line integral of a gradient vector $g:\mathbb{R}^n\rightarrow\mathbb{R}^n$ is path independent.
    \end{strt}

    \subsubsection{WIP - Other Methods}

    Some other methods are:
    \begin{itemize}
        \item Krasovskii’s Method
        \item Zubov’s Method
        \item Energy-Casimir Method
    \end{itemize}

    \subsection{WIP - Bifurcation Analysis}

    \begin{exmp}
        \textit{Given the following nonlinear system paramterised by $\mu$ find the bifurcation values of $\mu$ for the
        equilibrium point at the origin}
        \begin{align*}
            &x'=-y-x(x^{2}+y^{2}-\mu)\\
            &y'=x-y(x^{2}+y^{2}-\mu)            
        \end{align*}
        This system has the linearisation (about the origin),
        \begin{align*}
            &x'=\mu x-y\\
            &y'=x-\mu y
        \end{align*} 
        And from this we find the eigenvalues, $\lambda=\mu\pm i$. Now we consider the changes in the solution as $\mu$ 
        changes,
        \begin{itemize}
            \item if $\mu<0$ then the equlibrium is a stable spiral\\
            \item if $\mu=0$ then the equlibrium is a center\\
            \item if $\mu>0$ then the equlibrium is an unstable spiral
        \end{itemize}
    \end{exmp}

    \subsection{Visualization}

    \subsubsection{Phase-Planes}

    We can visualise the solutions as \textbf{time-series} or \textbf{phase-plane} diagrams.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.7\textwidth]{images/phase-plane.png}}
    \end{figure*}
    \FloatBarrier

    A constant solution, $x(t)=x_{0}, y(t)=y_{0}$ is an equilibrium point of the system. Futhermore we see that 
    $P(x_{0},y_{0})=Q(x_{0},y_{0})=0$.

    For linear 2D dynamical systems there are 4 types of isolated critical points (Spiral, Saddle, Node, Center). A
    example of what these look like can be seen below.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.9\textwidth]{images/linear-2d-equilibrium-types.png}}
    \end{figure*}
    \FloatBarrier

    For nonlinear 2D dynamical systems the isolated critical points can exhibit more complex behaviour, having aspects 
    from multiple types in the linear case.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.4\textwidth]{images/nonlinear-2d-equilibrium-example.png}}
    \end{figure*}
    \FloatBarrier

    \subsubsection{Constructing Phase Planes}

    The construction of a phase plane is usually done by identitying features of the system and then interpolating 
    between these to construct the remainder.

    \begin{defn}
        \textbf{$x,y$-nullcline}: The $x$-nullcline is the set of all points in the phase-plane for which $x'=0$. And 
        analagously for the $y$-nullcline.
    \end{defn}
    Given a nonlinear system, some useful features to plot are:
    \begin{itemize}{}{}
        \item equlibrium points
        \item $x$ and $y$ nullclines
        \item direction field along nullclines: i.e. at a given point on the $x$-nullcline we can use the value of $y'$ 
        at that point to draw the arrow at that point. Because we are on the $x$-nullcline the arrow should point 
        parallel to the $y$ axis and so we only need the value of $v'$ for its sign(/direction) and magnitude.
        \item initial conditions
    \end{itemize}

    \subsection{Reduction of Higher Order ODE to a System of First Order ODEs}
    
    If an ODE can be solved (rearranged) for its highest derivative term then it can be systematically reduced to a system of first order ODEs.
    \begin{enumerate}
        \item First write the ODE as,
        \[\frac{d^ny}{dt^n}=F\left(\frac{d^{n-1}y}{dt^{n-1}},...,\frac{dy}{dt},y,t\right)\]
        \item Next introduce $n+1$ new variables,
        \begin{align*}
            y_0&=y\\
            y_1&=\frac{dy}{dt}=\frac{dy_0}{dt}\\
            &\vdots\\
            y_{n-1}&=\frac{dy^{n-1}}{dt^{n-1}}=\frac{dy_{n-2}}{dt}\\
            y_n&=\frac{dy^n}{dt^n}=F\left(\frac{d^{n-1}y}{dt^{n-1}},...,\frac{dy}{dt},y,t\right)=F\left(y_{n-1},...,y_1,y_0,t\right)=\frac{dy_{n-1}}{dt}
        \end{align*}
        \item Finally, the system of first order ODEs is,
        \[\frac{d}{dt}\left(\begin{matrix}
            y_0\\
            y_1\\
            \vdots\\
            y_{n-2}\\
            y_{n-1}
        \end{matrix}\right)=\left(\begin{matrix}
            y_1\\
            y_2\\
            \vdots\\
            y_{n-1}\\
            F\left(y_{n-1},...,y_1,y_0,t\right)
        \end{matrix}\right)\] 
    \end{enumerate}
\end{document}