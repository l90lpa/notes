\documentclass[../main.tex]{subfiles}

\usepackage{multicol}

\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{Algebra}
    \subsection{Polynomials}

    \begin{theo}
        \textbf{Fundamental Theorem of Algebra}:\newline
        Every polynomial equation with complex coefficients has atleast one (possibly complex) solution. That is to say,
        let $p$ be a polynomial with complex coefficients, then there is atleast one $z\in\mathbb{C}$ such that $p(z)=0$
    \end{theo}

    \begin{theo}
        \textbf{Polynomial Factorization (Complex)}:\newline
        Let $p(z)$ be a polynomial of degree $n$ with complex coefficients and let $z,\alpha\in\mathbb{C}$. Then
        $p(\alpha)=0$ iff $p(z)=(z-\alpha)q(z)$ where $q(z)$ is a polynomial of degree $n-1$ with coefficients in $\mathbb{C}$
    \end{theo}

    \begin{coro}
        Let $p(z)$ be a complex-vauled, degree $n$ polynomial with complex coefficients. Then $p$ has the factorization
        \[p(z)=c_{n}\prod^r_{j}(z-\alpha_{j})^{n_j}\]
        where $\alpha_{i}\in\mathbb{C}$ are the roots of $p(z)$ and the degree is related to the multiplicities by 
        $n_1+...+n_r=n$
    \end{coro}

    \begin{coro}
        For a complex-valued, real-coefficient polynomials we have that $\overline{p(z)}=p(\bar{z})$. It follows from this that
        the complex conjugate of each root is also a root of the polynomial, i.e. roots come in complex conjugate pairs. 
    \end{coro}

    \begin{theo}
        \textbf{Polynomial Factorization (Real)}:\newline
        Let $p(x)$ be a real-valued, degree $n$ polynomial with real coefficients. Then $p$ has the factorization 
        (into linear and irreducible quadratic factors),
        \[p(x)=c_{n}\left(\prod^r_{j=1}(x-a_{j})^{n_j}\right)\left(\prod^s_{j=1}(x^2+b_{j}x+c_{j}^2)^{m_j}\right)\]
        where $a_{j},b_{j},c_{j}\in\mathbb{R}$. The real roots of $p(x)$ are $a_{j}$, and the complex roots of $p(x)$ are
        $\lambda_{j}=-2b_{j}+i\sqrt{c_j^2-4b_j}$ and $\bar{\lambda}_{j}=-2b_{j}-i\sqrt{c_j^2-4b_j}$. Finally the degree 
        is related to the multiplicities by $n_1+...+n_r+2(m_1+...+m_s)=n$.
    \end{theo}

    \begin{defn}
        \textbf{Polynomial Division}:\newline
        Let $a(z)$ and $b(z)$ be polynomials then, $a(z)/b(z) = q(z) + r(z)/b(z)$ where $q(z)$ is the \textbf{quotient} 
        and $r(z)$ is the \textbf{remainder}. Suppose $a(z)=a_{n}z^{n}+...+a_{0}$ and $b(z)=b_{m}z^{m}+...+b_{0}$, 
        a systematic method for finding the quotient and remainder is:
        \newline
        \newline
        \begin{algorithmic}
            \Procedure{polynomialDivision}{$a(z),b(z)$}
                \State $q(z) \gets 0$
                \State $r(z) \gets a(z)$
                \While{$\textrm{degree}(r(z)) \ge \textrm{degree}(b(z))$} \Comment{stop if r(z) is lower degree than b(z)}
                    \State $Q \gets \frac{\textrm{highestDegreeTerm}(r(z))}{\textrm{highestDegreeTerm}(b(z))}$ \Comment{divide highest degree terms of r(z) and b(z)}
                    \State $q(z) \gets q(z) + Q$
                    \State $r(z) \gets r(z) - Q\cdot a(z)$
                \EndWhile
                \State \Return $q(z)$,$r(z)$
            \EndProcedure
        \end{algorithmic}
        At termination $q(z)$ and $r(z)$ will contian the quotient and remainder.
    \end{defn}

    \begin{theo}
        \textbf{Abel-Ruffini Theorem}:\newline
        There is no solution in radicals to general polynomial equations of degree five or higher with arbitrary coefficients.
    \end{theo}

    \begin{defn}
        \textbf{Partial Fraction Decomposition (Complex)}:\newline
        The partial fraction decomposition expresses the fraction of two polynomials as a sum of fractions of polynomials
        with linear polynomial denominators. Let $F(z)$ and $G(z)$ be polynomials, where $G(z)=c_{n}\prod^r_{j}(z-\alpha_{j})^{n_j}$

        \begin{enumerate}
            \item If $\deg(F)\ge\deg(G)$, then we first perform polynomial division to get, $F/G=Q+R/G$ where $Q$ is the
            quotient and $R$ is the remainder. Now we perform partial fraction decomposition on $R/G$.
            \item Express $R(z)/G(z)$ as a sum of fractions. This sum is constructed by adding, for each factor $(z-\alpha_{j})^{n_j}$ in $G(z)$, the terms,
            \[\frac{A_{j,1}}{(z-\alpha_{j})}+\frac{A_{j,2}}{(z-\alpha_{j})^2}+...+\frac{A_{j,n_j}}{(z-\alpha_{j})^{n_j}}\]
            where $A_{j,k}$ are unknown constants to be solved for. Doing this gives,
            \[\frac{R(z)}{G(z)}=\sum^r_{j=1}\left(\frac{A_{j,1}}{(z-\alpha_{j})}+\frac{A_{j,2}}{(z-\alpha_{j})^2}+...+\frac{A_{j,n_j}}{(z-\alpha_{j})^{n_j}}\right)\]
            \item Multiply through by $G(z)$,
            \[R(z)=G(z)\sum^r_{j=1}\left(\frac{A_{j,1}}{(z-\alpha_{j})}+\frac{A_{j,2}}{(z-\alpha_{j})^2}+...+\frac{A_{j,n_j}}{(z-\alpha_{j})^{n_j}}\right)\]
            \item Solve for the unknown constants. This could possibly be done by equating the coefficients of terms with the same degree or by 
            substituting in suitable values of $z$. 
        \end{enumerate}        
    \end{defn}

    \begin{defn}
        \textbf{Partial Fraction Decomposition (Real)}:\newline
        Referring to the case for a complex polynomial, the procedure for a real polynomial is the same expect that the 
        factorization of $G$ is now $G(x)=c_{n}\left(\prod^r_{j=1}(x-a_{j})^{n_j}\right)\left(\prod^s_{j=1}(x^2+b_{j}x+c_{j}^2)^{m_j}\right)$
        and so step 2 has the addition that: also for each factor $(x^2+b_{j}x+c_{j}^2)^{m_j}$ in $G(z)$, we add to the sum the terms,
        \[\frac{B_{j,1}x+C_{j,1}}{(x^2+b_{j}x+c_{j}^2)}+\frac{B_{j,2}x+C_{j,2}}{(x^2+b_{j}x+c_{j}^2)^2}+...+\frac{B_{j,n_j}x+C_{j,m_j}}{(x^2+b_{j}x+c_{j}^2)^{m_j}}\]
        where $B_{j,k},C_{j,k}$ are unknown constants to be solved for. Doing this in addition gives,
        \begin{align*}
            \frac{R(z)}{G(z)}=\sum^r_{j=1}&\left(\frac{A_{j,1}}{(z-\alpha_{j})}+\frac{A_{j,2}}{(z-\alpha_{j})^2}+...+\frac{A_{j,n_j}}{(z-\alpha_{j})^{n_j}}\right)\\
            &+\sum^s_{j=1}\left(\frac{B_{j,1}x+C_{j,1}}{(x^2+b_{j}x+c_{j}^2)}+\frac{B_{j,2}x+C_{j,2}}{(x^2+b_{j}x+c_{j}^2)^2}+...+\frac{B_{j,m_j}x+C_{j,n_j}}{(x^2+b_{j}x+c_{j}^2)^{m_j}}\right)\\
            =\sum^r_{j=1}&\sum^{n_j}_{k=1}\frac{A_{j,k}}{(z-\alpha_{j})^k}+\sum^s_{j=1}\sum^{m_j}_{k=1}\frac{B_{j,k}x+C_{j,k}}{(x^2+b_{j}x+c_{j}^2)^k}
        \end{align*}
        At this point we can now continue from step 3 for the complex-valued polynomial case.
    \end{defn}

    \begin{theo}
        \textbf{Binomial Theorem (Positive Integers)}:\newline
        \[(b+a)^n=\sum^n_{k=0}\left(\begin{matrix}
            n\\
            k
        \end{matrix}\right)b^ka^{n-k},\quad\textrm{where}\quad\left(\begin{matrix}
            n\\
            k
        \end{matrix}\right)=\frac{n!}{(n-k)!k!}\]
    \end{theo}

    \begin{theo}
        \textbf{Binomial Theorem (Negative Integers)}:\newline
        \[(b+a)^{-n}=\sum^\infty_{k=0}\left(\begin{matrix}
            -n\\
            k
        \end{matrix}\right)b^ka^{-n-k},\quad\textrm{for}\quad|b|<a,\quad n\ge 0\]
        where,
        \[\left(\begin{matrix}
            -n\\
            k
        \end{matrix}\right)=(-1)^k\left(\begin{matrix}
            n+k-1\\
            k
        \end{matrix}\right)\]
    \end{theo}

    \begin{exmp}
        \textit{Find the power series expansion for $(1-x)^{-1}$ about $x=0$.}\newline
        Using the binomial theorem for negative integers we have that $-n=-1$, $a=1$, $b=-x$ hence,
        \begin{align*}
                (1-x)^{-1}&=\sum^{\infty}_{k=0}\left(\begin{matrix}
                -1\\
                k
            \end{matrix}\right)(-x)^k\\
            &=\sum^{\infty}_{k=0}(-1)^k\left(\begin{matrix}
                k\\
                k
            \end{matrix}\right)(-1)^k x^k\\
            &=\sum^{\infty}_{k=0} x^k,\quad\textrm{since}\quad (-1)^{2k}=1,\;\left(\begin{matrix}
                k\\
                k
            \end{matrix}\right)=1
        \end{align*}

    \end{exmp}

    \begin{exmp}
        \textit{Find the power series expansion for $(z-2)^{-1}$ about $z=1$.}\newline
        First we note that we can rewrite the expression as,
        \begin{align*}
            (z-2)^{-1}&=(z-1-1)^{-1}\\
            &=-(1-(z-1))^{-1}
        \end{align*}
        Using the binomial theorem for negative integers we have that $-n=-1$, $a=1$, $b=-(z-1)$. Hence by the same 
        approach as the example above,
        \begin{align*}
                (z-2)^{-1}&=-(1-(z-1))^{-1}\\
            &=-\sum^{\infty}_{k=0} (z-1)^k
        \end{align*}

    \end{exmp}


    \begin{defn}
        A polynomial, $p(x)$, has a \textbf{sum-of-squares (SOS) decomposition}, if it can be written as,
        \[p(x)=\sum_i q_i^2(x)\]
        where $q_i(x)$ are polynomials
    \end{defn}
    It is common to say that a polynomial \textit{is a SOS}.
    \begin{theo}
        An $n$-variable polynomial of degree $2d$ has an sos decomposition if and only if, there exists a positive semi-definite  matrix $Q$ such that,
        \[p(x)=z^TQz,\quad x\in\mathbb{C}^n\]
        where $z$ is the vector of monomials of degree up to $d$,
        \[z=(1, x_1, ..., x_n,x_1^2,x_1x_2,...x_1x_n,...,x_n^d)^T\]
        The components of $z$ being all combinations of $x_1^{\alpha_1} x_2^{\alpha_2} ... x_n^{\alpha_n}$
    \end{theo}

    \begin{theo}
        The set of all nonnegative $n$-variable polynomials of degree $d$ is equal to the set of sum-of-squares polynomials if and only if,
        \begin{itemize}
            \item $n=1$, or
            \item $d=2$, or
            \item $n=2,d=4$
        \end{itemize}
        \label{theo:positive_vs_sos}
    \end{theo}

\end{document}