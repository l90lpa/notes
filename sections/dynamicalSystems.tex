\documentclass[../main.tex]{subfiles}



\graphicspath{{\subfix{../images/}}}

\begin{document}
    
    \section{Dynamical Systems: Introduction}
    
    \subsection{Existence and Uniqueness of Solutions}
    
    \begin{theo}
        \textbf{Global Existence and Uniqueness}:\newline
        The $n$-dimensional system,
        \[\dot{x}=f(t,x(t)),\quad x(t_0)=x_0\]
        has a unique solution over the interval $[t_0,t_1]$ if:
        \begin{itemize}
            \item $f$ is piecewise continuous in $t$
            \item $f$ is Lipschitz continuous in $x$,
            \[\Vert f(t,x)-f(t,y)\Vert\le L\Vert x-y\Vert\quad\forall\quad x,y\in\mathbb{R}^n\;\textrm{and}\;t\in[t_0,t_1]\]
            \item $f$ satisfies,
            \[\Vert f(t,x_0)\Vert\le h\quad\forall\quad t\in[t_0,t_1]\]
        \end{itemize}
    \end{theo}

    \subsection{Equilibrium and Nullcline}

    \begin{defn}
        \textbf{Equilibrium Point}:\newline
        An \textbf{equilibrium point}, $x^*$, of the $n$-dimensional system, $\dot{x}=f(t,x(t))$ is a special kind of solution that satisfies,
        \[\dot{x^*}=f(t,x^*)=0\]
        Equilibrium points are also known as, critical points, stationary points, or fixed points.
    \end{defn}

    \begin{defn}
        \textbf{Nullcline}:\newline
        Given the $n$-dimensional system, $\dot{x}=f(t,x(t))$, the $x_j$-\textbf{nullcline} is the curve defined by,
        \[\dot{x_j}=f_j(t,x(t))=0\]
        where $x_j$ and $f_j$ are the $j$th components of $x$ and $f$ respectively.
    \end{defn}

    \subsection{Converting a Higher-Order ODE to a System of First-Order ODEs}
    
    If an ODE can be solved (rearranged) for its highest derivative term then it can be systematically reduced to a system of first order ODEs.
    \begin{enumerate}
        \item First write the ODE as,
        \[\frac{d^ny}{dt^n}=F\left(\frac{d^{n-1}y}{dt^{n-1}},...,\frac{dy}{dt},y,t\right)\]
        \item Next introduce $n+1$ new variables,
        \begin{align*}
            y_0&=y\\
            y_1&=\frac{dy}{dt}=\frac{dy_0}{dt}\\
            &\vdots\\
            y_{n-1}&=\frac{dy^{n-1}}{dt^{n-1}}=\frac{dy_{n-2}}{dt}\\
            y_n&=\frac{dy^n}{dt^n}=F\left(\frac{d^{n-1}y}{dt^{n-1}},...,\frac{dy}{dt},y,t\right)=F\left(y_{n-1},...,y_1,y_0,t\right)=\frac{dy_{n-1}}{dt}
        \end{align*}
        \item Finally, the system of $n$ first-order ODEs is,
        \[\frac{d}{dt}\left(\begin{matrix}
            y_0\\
            y_1\\
            \vdots\\
            y_{n-2}\\
            y_{n-1}
        \end{matrix}\right)=\left(\begin{matrix}
            y_1\\
            y_2\\
            \vdots\\
            y_{n-1}\\
            F\left(y_{n-1},...,y_1,y_0,t\right)
        \end{matrix}\right)\] 
    \end{enumerate}

    \section{Dynamical Systems: Time Invariant Systems}

    A dynamical system, $\dot{x}=f(t,x(t))$, is time-invariant if $f$ doesn't explicitly depend on $t$. In this case we can write the system as $\dot{x}=f(x(t))$. Time-invariant dynamical systems are also called \textbf{autonomous} dynamical systems.

    \subsection{Stability Definitions for Equilibrium Points}

    \begin{defn}
        \textbf{Stability of Equilibrium Points}:\newline
        \begin{itemize}
            \item An equilibrium point, $x^*$, is \textbf{Lyapunov stable} for $t\ge t_0$ if and only if, for all $\epsilon > 0$ there exists some $\delta > 0$ such that, 
            \[\Vert x^* - x(t_0) \Vert < \delta\Rightarrow\Vert x^* - x(t) \Vert \le \epsilon\quad\forall\quad t\ge t_0\]
            where $x(t)$ represents a neighbouring solution.
            
            \item An equilibrium point, $x^*$, is \textbf{asymptotically stable} for $t\ge t_0$ if, it is Lyapunov stable and addtionally there exists $\eta > 0$ such that 
            \[\Vert x^* - x(t_0) \Vert < \eta\Rightarrow\lim_{t\rightarrow\infty}\Vert x^* - x(t) \Vert=0\]
            
            \item An equilibrium point, $x^*$, is \textbf{exponentially stable} for $t\ge t_0$ if, there exists positive constants $\alpha,\;\beta$ and $\delta$ such that,
            \[\Vert x^* - x(t_0) \Vert < \delta\Rightarrow\Vert x^* - x(t) \Vert \le \alpha\Vert x^* - x(t_0) \Vert e^{-\beta t}\quad\forall\quad t\ge t_0\]
            
            \item the critical point \textbf{unstable} if it is not Lyapunov stable.    
        \end{itemize}
        \label{def:stability_equilibrium}
    \end{defn}
    \textbf{Remark}: an equilibrium is globally Lyapunov/asymptotically/exponentially stable if the respective conditions are satisifed for all $x(t_0)\in\mathbb{R}^n$.
    \newline
    \textbf{Remark}: from the above defintions we note that, 
    \[\textrm{exponentially stable}\Rightarrow\textrm{asymptotically stable}\Rightarrow\textrm{Lyapunov stable}\]

    \subsection{Stability Analysis}

    Often times it is difficult (or impossible) to solve dynamical systems. Hence, much of the theory on dynamical systems revolves around gaining qualtative insights into the behaviour of a dynamical system. One of the most common features and useful features to understand about a dynamical system is the stability of its equilibrium points.

    Characterising the stability of equilibrium points from the stability definitions (\ref{def:stability_equilibrium}) is only possible if we have a closed form solution to the system. In cases where we don't have a closed form solution, Lyapunov's Direct Method and Lyapunov's Indirect Method provide ways to establish the stability of equilibrium points.

    \subsection{Lyapunov's Indirect Method (Linearization)}

    Lyapunov's Indirect Method can only be used to establish local stability. Lyapunov's Indirect Method effectively gives sufficient conditions under which the stability of the equilibrium of a nonlinear system matches that of the linearised system about that equilibrium.
    
    \begin{theo}
        \textbf{Lyapunov's Indirect Method}:\newline
        Let $x^*$ be an equlibrium point of the autonomous $n$-dimension dynamical system $\dot{x}=f(x)$, where $f$ locally Lipschitz continuous. And let $J_f(x^*)$ be the Jacobian of $f$ at $x^*$, with eigenvalues $\lambda_1,...,\lambda_n$. Then $x^*$ is,
        \begin{enumerate}
            \item exponentially stable if $Re(\lambda_i) < 0\;\;\;\forall\;\;\;i = 1, 2, ... , n$
            \item unstable if $Re(\lambda_i) > 0\;\;\;\textrm{for any}\;\;\;i = 1, 2, ... , n$
        \end{enumerate}
        If any eigenvalues have $Re(\lambda_i)=0$, then further analysis is necessary. 
    \end{theo}
        
    \begin{defn}
        \textbf{Hyperbolic Equilibrium Point}:\newline
        An equilibrium point, $x^*$, of the dynamical system, $\dot{x}=f(x)$ is said to be \textbf{hyperbolic} (or \textbf{nondegenerate}) if the Jacobian of $f$ at $x^*$, $J_f(x^*)$, has no eigenvalues with real part equal to zero.
    \end{defn}
    For 1D systems $J_f(x^*)=f'(x^{*})$ and so Lyapunov's Indirect Method becomes,
    \begin{defn}
        \textbf{Lyapunov's Indirect Method for 1D Systems}:\newline
        Given an equilibrium point $x^{*}$,    
        \begin{itemize}{}{}
            \item If $f'(x^{*})<0$ then $x^{*}$ is an \textbf{stable} equilibria
            \item If $f'(x^{*})>0$ then $x^{*}$ is an \textbf{unstable} equilibria
            \item If $f'(x^{*})=0$ then the test is \textbf{inconclusive} (we need to consider higher order terms to figure of 
            the nature of the equilibria)
        \end{itemize}
    \end{defn}
    For 2D linear dynamical systems, it is common to more finely classify the stability of the equlibrium point at the origin. Given the 2D LTI system $\dot{x}=Ax$ the eigenvalues of $A$ are given by,
    \[\lambda=\frac{1}{2}(\tr(A)\pm\sqrt{\tr(A)^{2}-4\det(A)})\]
    It can be shown that, the eigenvalues both have negative real parts if and only if, 
    \[\tr(A)<0 \textrm{   and   } \det(A)>0\]
    \begin{defn}
        \textbf{Equilibrium Stability Classification for 2D LTI Systems}:\newline
        Consider the 2D LTI system $\dot{x}=Ax$, which has equilibrium point $x^*=0$. Now let, $\lambda_{1}$ and $\lambda_{2}$ be the eigenvalues of $A$,
        \begin{itemize}{}{}
            \item If the eigenvalues are real and distinct:
                \begin{itemize}{}{}
                    \item If $\lambda_{1}<0$ and $\lambda_{2}>0$ then the origin is \textbf{saddle}
                    \item If $\lambda_{1},\lambda_{2}>0$ then the origin is \textbf{unstable node}
                    \item If $\lambda_{1},\lambda_{2}<0$ then the origin is \textbf{stable node}
                \end{itemize}

            \item If there is a single real eigenvalue (with multiplicity 2):
                \begin{itemize}{}{}
                    \item If there are 2 eigenvectors:
                    \begin{itemize}{}{}
                        \item If $\lambda >0$ then the origin is \textbf{unstable node}
                        \item If $\lambda <0$ then the origin is \textbf{stable node}
                    \end{itemize}
                    \item If there is only 1 eigenvector:
                    \begin{itemize}{}{}
                        \item If $\lambda >0$ then the origin is \textbf{improper unstable node}
                        \item If $\lambda <0$ then the origin is \textbf{improper stable node}
                    \end{itemize}
                \end{itemize}
            
            \item If the eigenvalues are complex (then they are complex conjugates and so share the same real part):
                \begin{itemize}{}{}
                    \item If the real part is zero then the origin is \textbf{center}
                    \item If the real part is negative then the origin is \textbf{stable spiral}
                    \item If the real part is positive then the origin is \textbf{unstable spiral}
                \end{itemize}
        \end{itemize}  
    \end{defn}
    \textbf{Remark}: the behaviour of a nonlinear system is the same as that of the linear approximation, except when the linear system has a centre. If the linear system has a centre, then the equilibrium point of the original non-linear system may be a centre, a spiral sink or a spiral source.
    
    \subsubsection{Linearization}

    \begin{strt}
        \textbf{Linearising a Nonlinear System}:\newline
        Consider the nonlinear system,
        \[\dot{x}=f(x)\]
        where $x\in\mathbb{R}^n$ and $f:\mathbb{R}^n\rightarrow \mathbb{R}^n$. 
        Suppose $x^*$ is a critical point of the nonlinear system, and defined $x=x^*+\xi$ where $\Vert\xi\Vert$ is small. Then substituting for $x$ in the nonlinear system we get,
        \[\frac{d}{dt}(x^*+\xi)=f(x^*+\xi)\quad\Rightarrow\quad\dot{\xi}=f(x^*+\xi),\quad\textrm{since  }\dot{x^*}=0\]
        Taylor expanding $f$ about $x^*$ we get,
        \begin{align*}
            \dot{\xi}&=f(x^*)+J_f(x^*)\cdot\xi+o(\Vert \xi\Vert ),\quad\textrm{where  }J_f(x^*)\textrm{  is the Jacobian of }f\textrm{ at }x^*\\
            &=J_f(x^*)\cdot\xi+o(\Vert \xi\Vert ),\quad\textrm{since  }f(x^*)
        \end{align*}
        Therefore, on discarding higher order terms we get the linearised system,
        \[\dot{\xi}=J_f(x^*)\xi\]
        where $\xi=0$ correspondes to $x=x^*$ in the nonlinear system.
    \end{strt}
    
    \subsection{Lyapunov's Direct Method (Lyapunov Functions)}

    Lyapunov's Direct Method can be used to estabilish local or global stability. The idea behind Lyapunov's Direct Method is to search for a nonnegative function of the state such that the value of the function decreases along all trajectories within a given region of an equlibrium point. This function is analogous to the energy of a physical system. If such a function can be found then the equilibrium is stable.
    
    \begin{theo}
        \textbf{Lyapunov's Direct (or First) Method (For Local Analysis)}:\newline
        Let $x^*$ be an equlibrium point of the autonomous $n$-dimension dynamical system $\dot{x}=f(x)$, where $f$ locally Lipschitz continuous. And let $D$ be a subset of the domain such that $x^*\in D$ (i.e. some neighbourhood of $x^*$). 
        \begin{itemize}
            \item If there exists a continuously differentiable function $V:D\rightarrow\mathbb{R}$ such that,
            \begin{align*}
                V(x^*)&=0,\\
                V(x)&>0\quad\forall\quad x\in D\backslash\{x^*\},\\
                \dot{V}(x)=\nabla V(x)f(x)&\le 0\quad\forall\quad x\in D
            \end{align*}
            then $x^*$ is \textbf{Lyapunov stable}. If additionally $V(x)$ satisfies the condition,
            \[\dot{V}(x)=\nabla V(x)f(x)< 0\quad\forall\quad x\in D\backslash\{x^*\}\]
            then $x^*$ is \textbf{(locally) asymptotically stable}.
            \item If there exists a continuously differentiable function $V:D\rightarrow\mathbb{R}$, and parameters $\alpha$, $\beta$, $\varepsilon$, and $p\ge 1$ such that,
            \[\alpha\Vert x-x^*\Vert^p\le V(x) \le\beta\Vert x-x^*\Vert^p,\quad x\in D\]
            \[\nabla V(x)f(x)\le-\varepsilon V(x),\quad x\in D\]
            then $x^*$ is \textbf{(locally) exponentially stable}.
        \end{itemize}
        The function $V(x)$ is called a \textbf{Lyapunov function}, and this theorem is also known as \textbf{Lyapunov's stability theorem}
        \label{theo:lyapunov_direct_local}
    \end{theo}
    \textbf{Remark}: generally there are many possible Lyapunov functions that satisfy the conditions of the theorem, which is what makes the theorem more attractive to apply than solving the system. But it can still be very difficult to find a Lyapunov function as there is no single systemic process.
    \newline
    \textbf{Remark}: Lyapunov's direct method cannot be used to prove instability.

    \begin{theo}
        \textbf{Lyapunov's Direct (or First) Method (For Global Analysis)}:\newline
        Let $x^*$ be an equlibrium point of the autonomous $n$-dimension dynamical system $\dot{x}=f(x)$, where $f$ locally Lipschitz continuous.
        \begin{itemize}
            \item If there exists a continuously differentiable function $V:\mathbb{R}^n\rightarrow\mathbb{R}$ such that,
            \begin{align*}
                V(x^*)&=0,\\
                V(x)&>0\quad\forall\quad x\in \mathbb{R}^n\backslash\{x^*\},\\
                \dot{V}(x)=\nabla V(x)f(x)&< 0\quad\forall\quad \mathbb{R}^n\in D\backslash\{x^*\}\\
                V(x)&\rightarrow\infty\textrm{ as }\Vert x\Vert\rightarrow\infty
            \end{align*}
            then $x^*$ is \textbf{globally asymptotically stable}.
            \item If there exists a continuously differentiable function $V:\mathbb{R}^n\rightarrow\mathbb{R}$, and parameters $\alpha$, $\beta$, $\varepsilon$, and $p\ge 1$ such that,
            \[\alpha\Vert x-x^*\Vert^p\le V(x) \le\beta\Vert x-x^*\Vert^p,\quad x\in \mathbb{R}^n\]
            \[\nabla V(x)f(x)\le-\varepsilon V(x),\quad x\in \mathbb{R}^n\]
            then $x^*$ is \textbf{globally exponentially stable}.
        \end{itemize}
        \label{theo:lyapunov_direct_global}
    \end{theo}
    \textbf{Remark}: this theorem doesn't contain a definition for global Lyapunov stability because it is inherinetly a local form of stability.

    \subsubsection{Finding Lyapunov Functions: Methods for LTI}

    \begin{theo}
        \textbf{Global Asymptotic Stability in Linear Time-Invariant (LTI) Systems}\newline
        Consider the $n$-dimension linear system $\dot{x}=Ax$ where the origin is an equilibrium point. A Lyapunov function candidate for this system is,
        \[V(x)=x^TPx\]
        If the matrix $P$ (and hence $V$) can be found such that it satisfies,
        \begin{itemize}
            \item $P\succ 0\quad\Leftrightarrow\quad V(x)=x^TPx<0$
            \item $PA+A^TP\prec 0 \quad\Leftrightarrow\quad\dot{V}(x)=x^TPAx+x^TA^TPx<0$
        \end{itemize}
        Then the origin is globally asymptotically stable.
        \label{theo:stability_lti}
    \end{theo}

    The most efficient way to search for a Lyapunov function for LTI systems, takes a numerical linear algebra approach. Another way is using semi-definite programming, which though likely less efficient, has some benefits such as it can be easily used to search for a common Lyapunov function over a parameterised system, or multiple different systems.

    \begin{theo}
        Given any matrix $Q\succ 0$, there exists a unique $P\succ 0$ satisfying $PA+A^TP+Q=0$ if and only if the linear system $\dot{x}=Ax$ is globally asymptotically stable. The equation $PA+A^TP+Q=0$ is called the \textbf{continuous Lyapunov equation}
        \label{theo:stability_lyapunov_eqn}
    \end{theo}
    \textbf{Remark}: $PA+A^TP=-Q$ is a special case of a Sylvester equation.

    \begin{strt}
        \textbf{Lyapunov function search for LTI systems by solving the Lyapunov equation}:\newline
        Consider the $n$-dimension linear system $\dot{x}=Ax$ where the origin is an equilibrium point. 
        \begin{enumerate}
            \item Choose a postive definite matrix $Q$. A simple choice would be $\epsilon I$, where $\epsilon >0$ is small and $I$ is the identity matrix. 
            \item Solve the Lyapunov equation, 
            \[PA+A^TP+Q=0\] 
            where the unknown is $P$. We could try to solve the Lyapunov equation using standard matrix factorization techniques, but there are also specialized algorithms such as the Bartels-Stewart algorithm. There are routines in various math libraries for solving Lyapunov and Sylvester which use algorithms such as Bartels-Stewart.
            \item If a solution is found then the origin is globally asymptotically stable by theorem (\ref{theo:stability_lyapunov_eqn}).
        \end{enumerate}
    \end{strt}

    \begin{strt}
        \textbf{Lyapunov function search for LTI systems by Semi-Definite Programming}:\newline
        Consider the $n$-dimension linear system $\dot{x}=Ax$, where the origin is an equilibrium point. 
        \begin{enumerate}
            \item Choose positive definite matrices $M_1$ and $M_2$. A simple choice would be $M_1=M_2=\epsilon I$, where $\epsilon >0$ is small and $I$ is the identity matrix.
            \item Program the SDP feasibility problem. The Lyapunov stability conditions of the Lyapunov Direct Methods (theorem (\ref{theo:stability_lti})) can be reformulated as the SDP feasibility problem,
            \begin{align*}
                &\min_{P}1\\
                &\textrm{subject to:}\\
                &P\succeq M_1\\
                &PA+A^TP\preceq M_2
            \end{align*}
            The constraint $P\succeq M_1$ is used to ensure that $P\succ 0$, since constraints of semi-definite programs cannot be strict definitness constraints (and similarly for the other constrain)
            \item Solve the feasibility problem. If a feasible solution is found then the origin is globally asymptotically stable, by theorem (\ref{theo:stability_lti}).
        \end{enumerate}
    \end{strt}
    \textbf{Remark}: recall that $P\succ 0\Rightarrow P^T=P$, and so when setting up the SDP problem for some solver, the same decision varibles used in the upper triangle of the decision matrix must also be used in the lower triangle (the lower triangle shouldn't contain new variables).

    \subsubsection{Finding Lyapunov Functions: Methods for PTI}
    
    In the case of searching for Lyapunov functions for polynomial time-invariant (PTI) dynamical systems, Sums-of-Squares Programming (SOSP) has proven to be an effective tool.
    \newline
    \newline
    SOSP is a natural generalization of SDP, to optimizing over nonnegative polynomials.

    \begin{defn}
        It is common in dynamical systems liturature to say that a function, $g(x)$, is \textbf{positive-definite} if, $g(0)=0$ and $g(x)>0\;\forall\; x\neq 0$.
    \end{defn}
    Suppose that $h(x)$ is some positive definite polynomial, then,
    \[p(x)-h(x)\textrm{ is SOS}\Rightarrow p(x)\ge h(x)\]
    Furthermore, if $p(x)$ is a polynomial without a constant term then $p(0)=0$ and so it is also positive definite.

    \begin{strt}
        \textbf{WIP - Lyapunov function search for PTI systems by Sum-of-Squares Programming}:\newline
        Consider the $n$-dimension system $\dot{x}=f(x)$, with equilibrium point $x^*=0$. To search for a Lyapunov function for the equilibrium point of this system, we reformulate the Lyapunov stability conditions of theorem (\ref{theo:lyapunov_direct_local}) as an SOSP feasibility problem, and use an SOSP solver to look for a feasible solution.
        \begin{enumerate}
            \item Choose a Lyapunov function candidate, $V_q(x)$. This is a fixed degree $n$-variable polynomial parameterised by its coefficients, $q$. The coefficients of $V_q(x)$ will act as the decision variables. The chose of $V_q(x)$ should not contain constant or linear terms, this is becasue the Lyapunov function must satisfy $V_q(0)=0$ which implies the constant term is zero, $V_q(0)=0\Rightarrow \nabla V_q(0)=0$ hence the linear terms must also be zero.
            \item Choose a positive-definite polynomial $\varphi(x)$. This function is used to force the Lyapunov function to be positive-definite. A simple choice is $\varphi(x)=\epsilon x^T x$, where $\epsilon >0$ is a small positive number. Suppose the Lyapunov function candidate has degree $2d$, an alternaitve proposal is,
            \[\varphi (x)=\sum_{i=1}^n\sum_{j=1}^d \epsilon_{ij}x_i^{2j}\]
            such that,
            \[\sum_{j=1}^d \epsilon_{ij}>\gamma\quad\forall\quad i\in\{1,...,n\}\]
            where one chooses $\gamma >0$, and $\epsilon_{ij}\ge 0$ for all $i,j$.
            \item Program the feasibility problem,
            \begin{align*}
                &\min_{q}1\\
                &\textrm{subject to:}\\
                &V_q(x)-\varphi(x)\in SOS\\
                &-\nabla V_q(x)f(x)\in SOS
            \end{align*}
            \item Solve the feasibility problem. If a feasible Lyapunov function is found then the origin is 'globally' Lyapunov stable.
        \end{enumerate}
        If we want to restrict the search to only Lyapunov functions that establish global asymptotic stability then we replace the final constrain of the feasibility with $-\nabla V_q(x)f(x)-\phi(x)\in SOS$ where $\phi(x)$ is a positive-definite polynomial similar to $\varphi(x)$.
        
        To practically search for a Lyapunov function, one normally starts with a Lyapunov function candidate of low degree monomials say all degree 2 monomials and then incrementally includes additional higher degree monomials until a Lyapunov function is found, or the search is terminated.
    \end{strt}
    \textbf{Remark}: first the SOSP feasibility problem is a conservative reformulation because it restricts the conditions on $V(x)$ beyond those of the Lyapunov Direct Method. The restriction takes two forms: first, not all polynomial vector fields admit polynomial Lyapunov functions for certifying the stability of their equilibrium points (for example the Motzkin equation), second, not all positive polynomials have a sum-of-squares representation, see theorem (\ref{theo:positive_vs_sos}).

    \subsubsection{Finding Lyapunov Functions: General Methods}

    The Variable Gradient Method works by first assuming a particular form for the gradient of the Lyapunov function candiate, and then integrating this to find a Lyapunov function.

    \begin{strt}
        \textbf{Variable Gradient Method}\newline
        Given the $n$-dimensional dynamical system,
        \[\dot{x}=F(x)\]
        Define $h:\mathbb{R}^n\rightarrow\mathbb{R}^n$ such that,
        \begin{itemize}
            \item $h(x)=\nabla V(x)$
            \item $\frac{\partial h_i}{\partial x_j}=\frac{\partial h_j}{\partial x_i}\quad\textrm{for}\quad i,j=1,...,n$
            \item $\dot{V}(x)=\frac{\partial V}{\partial x}\dot{x}=h^T F < 0$
        \end{itemize}
        Now one must use creativity to determine a form for $h$ such that it $h(x)$ satsifies the conditions above.
        Finally, $V$ is recovered by computing the line integral,
        \[V(x)=\int^{x}_{x^*} h^T(s)\;ds=\int^{x}_{x^*} \sum^n_{i=1}h_i(s)\;ds_i\]
        recalling that the line integral of a gradient vector $g:\mathbb{R}^n\rightarrow\mathbb{R}^n$ is path independent.
    \end{strt}

    \subsection{WIP - Region of Attraction}

    Alongside wishing to know the stability of an equilibrium point it is often useful to also know the region of attraction too. Finding the region of attraction can be difficult and so one typically looks for an estimate.

    \begin{theo}
        The region of attraction of an asymptotically stable equilibrium point is an open, connected, invariant set, of which the boundary is formed by trajectories of the system.
    \end{theo}

    For low dimension systems one why to find the region of attraction is to run simulations of the system - for various initial conditions - to collect trajectories, and then look at the visualization of the trajectories to identify the region of attraction.

    Alternatively we can estimate the region of attraction in a number of ways.

    \subsection{WIP - Bifurcation}


    \section{WIP - Dynamical Systems: Time-Varying Systems}
    

    \section{WIP - Dynamical Systems: Simulation}

    \subsection{WIP - Continuation}

    \section{WIP - Dynamical Systems: Visualization}

    \subsection{WIP - 1D Systems}

    A helpful way to examine the behaviour of equlibria is to construct a \textbf{phase-line}. Here we construct a line 
    indicating the flow of the solution $x(t)$ were we plot the equilibria points and inbetween these, arrows indicting
    the flow direction. If $\dot{x} >0$ at a given $x$ then the arrow points towards increasing $x$.
    If $\dot{x} <0$ at a given point $x$ then the arrow points in the decreasing $x$ direction. Else we are at an 
    equilibria. 
    \newline
    \newline
    It can be helpful to think of $\dot{x}$ as the velocity of $x$, hence we use the sign of $\dot{x}$ for direction.
    \newline
    \newline
    Below is an example of a phase-line with the graph $\dot{x}$ over $x$ plotter on top.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.9\textwidth]{images/1d-phase-line-and-time-series.png}}
    \end{figure*}
    \FloatBarrier

    \subsection{WIP - 2D Systems}

    We can visualise the solutions as \textbf{time-series} or \textbf{phase-plane} diagrams.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.7\textwidth]{images/phase-plane.png}}
    \end{figure*}
    \FloatBarrier

    A constant solution, $x(t)=x_{0}, y(t)=y_{0}$ is an equilibrium point of the system. Futhermore we see that 
    $P(x_{0},y_{0})=Q(x_{0},y_{0})=0$.

    For linear 2D dynamical systems there are 4 types of isolated critical points (Spiral, Saddle, Node, Center). A
    example of what these look like can be seen below.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.9\textwidth]{images/linear-2d-equilibrium-types.png}}
    \end{figure*}
    \FloatBarrier

    For nonlinear 2D dynamical systems the isolated critical points can exhibit more complex behaviour, having aspects 
    from multiple types in the linear case.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.4\textwidth]{images/nonlinear-2d-equilibrium-example.png}}
    \end{figure*}
    \FloatBarrier

    \subsubsection{Constructing Phase Planes}

    The construction of a phase plane is usually done by identitying features of the system and then interpolating 
    between these to construct the remainder.

    Given a nonlinear system, some useful features to plot are:
    \begin{itemize}{}{}
        \item equlibrium points
        \item $x$ and $y$ nullclines
        \item direction field along nullclines: i.e. at a given point on the $x$-nullcline we can use the value of $y'$ 
        at that point to draw the arrow at that point. Because we are on the $x$-nullcline the arrow should point 
        parallel to the $y$ axis and so we only need the value of $v'$ for its sign(/direction) and magnitude.
        \item initial conditions
    \end{itemize}

\end{document}