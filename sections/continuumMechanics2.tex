\documentclass[../main.tex]{subfiles}

\usepackage{multicol}

\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{WIP - Continuum Mechanics 2}

    Continuum mechanics is the modelling of materials, and their mechanical behaviour, under the assumption of continuous mass. It unifies solid mechanics, fluid mechanics, thermodynamics, and heat transfer. 
    \newline
    \newline
    \textbf{Note}: throughout this section we assume a global Cartesian coordinate system, that is the coordinate systems of the reference and deformed configurations are equal. This simplifies the tensor algebra involved, since there is no distinction between covariant, and contravariant tensors.

    \subsection{Notational Conventions, and Symbols}

    There is no standard notational convention when it comes to continuum mechanics literature, but we will define the general rules this material follows here.

    \begin{itemize}
        \item scalars, written in italics, $a$
        \item vectors, written in bold italics, $\vect{x}$
        \item matrices/tensors, written in font sans serif, $\tens{F}$
        \item Generally quantities expressed in the Lagrangian description are in uppercase
        \item Generally quantities expressed in the Eulerian description are in lowercase 
    \end{itemize}

    \begin{tabular}{|c|c|c|}
        \hline
        Quantity & Reference Configuration & Deformed Configuration \\
        \hline
        Material region & $\Omega_0$ & $\Omega_t$ \\
        Position vector & $\vect{X}$ & $\vect{x}$ \\
        Infinitesimal line element & $\mathbf{d}\vect{X}$ & $\mathbf{d}\vect{x}$ \\
        Density & $\rho_0$ & $\rho$ \\
        Infinitesimal area element & $\mathrm{d}A$ & $\mathrm{d}a$ \\
        Infinitesimal volume element & $\mathrm{d}V$ & $\mathrm{d}v$ \\
        Surface unit normal & $\hat{\vect{N}}$ & $\hat{\vect{n}}$ \\
        \hline
    \end{tabular}

    \begin{tabular}{|c|c|c|}
        \hline
        Quantity & Lagrangian Form & Eulerian Form \\
        \hline
        Reference position vector & $\vect{X}$ & $\vect{X}(\vect{x},t)$ \\
        Deformed position vector & $\vect{x}(\vect{X},t)$ & $\vect{x}$ \\
        Displacement field & $\vect{U}(\vect{X},t)$ & $\vect{u}(\vect{x},t)$ \\
        Velocity field & $\vect{V}(\vect{X},t)$ & $\vect{v}(\vect{x},t)$ \\
        Acceleration field & $\vect{A}(\vect{X},t)$ & $\vect{a}(\vect{x},t)$ \\
        \hline
    \end{tabular}

    \subsection{Kinematics}

    Kinematics concerns the motion of objects without reference to the forces that induce the motion.

    \subsubsection{Configurations}
    
    When working with continuum bodies, \textbf{configuration} refers to the position/arangement of a body (i.e the simultaneous position of all particles within the body). The \textbf{reference configuration}, $\Omega_0$, is the original position of the body, and the \textbf{deformed configuration}, $\Omega_t$, is the current state of the body. Furthermore, throughout this section we will use the following convention,
    \begin{itemize}
        \item $\vect{X}\in\Omega_0$, is the position vector of a material point in the reference configuration.
        \item $\vect{x}\in\Omega_t$, is the position vector of a material point in the deformed configuration.
    \end{itemize}
    The \textbf{deformation map} is a function mapping points in the reference configuration to points in the deformed configuration.
    \[\vect{x}=\vect{\kappa}(\vect{X},t)\]
    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.7\textwidth]{images/configuration.png}}
    \end{figure*}
    \FloatBarrier

    \subsubsection{Descriptions}
    
    There are two ways in which we can formulate (describe) the physics in continuum mechanics, the Lagrangian description, and the Eulerian description:
    \begin{itemize}
        \item \textbf{Lagrangian description}, AKA the \textbf{material description}: individual particles (or infinitesimal fluid parcels) are tracked. That means, we treat time, $t$, and the reference position, $\vect{X}$ as independent variables, and express $\vect{x}$ in terms of these. Furthermore, other fields are also treated as functions of the reference position and time, and so have the general form $\phi=\phi(\vect{X},t)$. The Lagrangian description description is typically used in solid mechanics where the refernce configuration is usually easily defined, and the material points are easily tracked.
        \item \textbf{Eulerian description}, AKA the \textbf{spatial description}: the behaviour of the system at fixed positions is obsevered. That means we treat, $\vect{x}$ and $t$ as independent variables, and express $\vect{X}$ in terms of these. Furthermore, other fields are also described in terms of the deformed position and time, and so have the general form $\phi=\phi(\vect{x},t)$. The Eulerian description is usually used in fluid mechanics, and is mosre suitable for situations where the original position of particles isn't known. In fluid mechanics, streamlines are a purely Eulerian concept, while pathlines are a purely Lagrangian concept.
    \end{itemize}

    \subsubsection{Velocity, and Acceleration}

    The Lagrangian description of the velocity and acceleration fields are respectively,
    \[\vect{V}(\vect{X},t)=\frac{\mathrm{d}}{\mathrm{d}t}\vect{x}(\vect{X},t)=\frac{\mathrm{d}}{\mathrm{d}t}\vect{\kappa}(\vect{X},t),\quad\quad \vect{A}(\vect{X},t)=\frac{\mathrm{d}}{\mathrm{d}t}\vect{V}(\vect{X},t)=\frac{\mathrm{d^2}}{\mathrm{d}t^2}\vect{\kappa}(\vect{X},t)\]

    \subsubsection{Displacment}

    If one uses the same coordinate frame for the reference and deformed configurations, then we can define a displacement field. The Lagrangian description of the displacement field is,
    \[\vect{U}(\vect{X},t)=\vect{x}(\vect{X},t)-\vect{X}\]
    and this tells us how the particle starting at $\vect{X}$ has been displaced at time $t$. The Eulerian description of the displacement field is,
    \[\vect{u}(\vect{x},t)=\vect{x}-\vect{X}(\vect{x},t)\]
    and this tells us how the particle occupying position $\vect{x}$ at time $t$, has been displaced. Furthermore, note that if we fix $\vect{x}$ and $t$ then, $\vect{u}(\vect{x},t)=\vect{U}(\vect{X}(\vect{x},t),t)$, and if we fix $\vect{X}$ and $t$ then, $\vect{U}(\vect{X},t)=\vect{u}(\vect{x}(\vect{X},t),t)$

    \subsubsection{Deformation Gradient}

    \paragraph{Defintion} 
    
    The deformation gradient characteries how the neighbourhood of a point in the reference configuration deforms to the neighbourhoods of the point in the deformed configuration. In fact it provides the best linear approximation of this deformation. The deformation gradient is a two-point 2nd order tensor, and it maps line elements and vectors in the reference configuration to line elements and vectors in the deformed configuration.
    \newline
    \newline
    The \textbf{(Lagrangian) deformation gradient} is,
    \[\tens{F}(\vect{X},t)=\frac{\partial \vect{x}}{\partial \vect{X}}(\vect{X},t)=\frac{\partial x_i}{\partial X_j}\]
    and it satisfies the relationship,
    \[\mathbf{d}\vect{x}=\tens{F}(\vect{X},t)\cdot \mathbf{d}\vect{X}\]
    where, $\mathbf{d}\vect{X}$, a line element in the reference configuration, and $\mathbf{d}\vect{x}$ is a line element in the deformed configuration.
    \newline
    \newline
    The \textbf{Eulerian deformation gradient} is,
    \[\tens{H}(\vect{x},t)=\frac{\partial \vect{X}}{\partial \vect{x}}(\vect{x},t)=\frac{\partial X_i}{\partial x_j}\]
    which has the analogous relation,
    \[\mathbf{d}\vect{X}=\tens{H}(\vect{x},t)\cdot \mathbf{d}\vect{x}\]
    Furthermore, combining these result it is easy to show that,
    \[\tens{F}^{-1}=\tens{H}\]

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.7\textwidth]{images/deformation_gradient.png}}
    \end{figure*}
    \FloatBarrier

    \paragraph{Derivation}

    Consider a line element in the reference configuration, $\mathbf{d}\vect{X}$, that connects two material points, and suppose $\vect{X}$ is the position vector of one end of the line element. The position of the other end of the line element is then, $\vect{X}+\mathbf{d}\vect{X}$. Using the Lagrangian description, under deformation the point $\vect{X}$ maps to $\vect{x}(\vect{X}, t)$, and $\vect{X}+\mathbf{d}\vect{X}$ maps to $\vect{x}(\vect{X}+\mathbf{d}\vect{X},t)$. Therefore the line element in the deformed configuration is,
    \[\mathbf{d}\vect{x}=\vect{x}(\vect{X}+\mathbf{d}\vect{X},t)-\vect{x}(\vect{X}, t)\]
    Taylor expanding the RHS about $\vect{X}$ we get,
    \[\vect{x}(\vect{X}, t)+\mathbf{d}\vect{x}=\vect{x}(\vect{X}, t)+\frac{\partial \vect{x}}{\partial \vect{X}}(\vect{X},t)\cdot \mathbf{d}\vect{X}+O(\Vert\mathbf{d}\vect{X}\Vert^2)\]
    And discarding higher order term (under the assumption that $\Vert\mathbf{d}\vect{X}\Vert\ll 1$ and so higher order terms are negligible) we get,
    \begin{align*}
        \mathbf{d}\vect{x}&=\frac{\partial \vect{x}}{\partial \vect{X}}(\vect{X},t)\cdot \mathbf{d}\vect{X}\\
        &=\tens{F}(\vect{X},t)\cdot \mathbf{d}\vect{X}\\
    \end{align*}

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.7\textwidth]{images/derivation_deformation_gradient.png}}
    \end{figure*}
    \FloatBarrier

    \paragraph{Change in Volume and Area} 
    
    The local change in volume due to deformation can be computed from the deformation gradient by simply computing its determinant. This local change characterised by the ratio of the deformed volume about the deformed point to a volume about a reference point, and is known as the \textbf{Jacobian},
    \[J(\vect{X},t)=\det F(\vect{X},t)=\frac{\mathrm{d}v}{\mathrm{d}V}\]
    where $\mathrm{d}V$ is an infinitesimal volume element in the reference configuration and $\mathrm{d}v$ an infinitesimal volume element in the deformed configuration. While the Jacobian characterises local change in volumn. \textbf{Nanson's Formula} characterises local change in surface area,
    \[\hat{\vect{n}}\mathrm{d}a=J \tens{F}^{-T}\cdot \hat{\vect{N}}\mathrm{d}A\]

    \paragraph{Rotation and Stretch} 
    
    The deformation gradient encodes the local rotation and and stretch of the deformation map about each point. If we consider the matrix representation of the deformation gradient, and compute the polar decomposition then we get,
    \[\tens{F}=\tens{R}\cdot\tens{P}=\tens{Q}\cdot\tens{R}\]
    where $R$ is a rotation tensor (unitary matrix), and $P$ and $Q$ are right and left stretching tensors (positive semidefinite matrices) respectively. $\tens{R}\cdot\tens{P}$ is the right polar decomposition, and $\tens{Q}\cdot\tens{R}$ is the left polar decomposition.

    \paragraph{Connection to the displacement field} If a displacement field is defined then we can express the deformation gradient in terms of the displacement field.
    \begin{align*}
        \tens{F}&=\frac{\partial }{\partial \vect{X}}\vect{x}(\vect{X},t)\\
        &=\frac{\partial }{\partial \vect{X}}\left(\vect{X}+\vect{U}(\vect{X},t)\right)\\
        &=\tens{I}+\frac{\partial }{\partial \vect{X}}\vect{U}(\vect{X},t)\\
        &=\tens{I}+\nabla_{\vect{X}}\vect{U}(\vect{X},t)
    \end{align*}
    Similarly for the Eulerian deformation gradient we have,
    \[\tens{H}=\tens{I}-\nabla_{\vect{x}}\vect{u}(\vect{x},t)\]
    
    \subsubsection{Cauchy-Green Deformation Tensors}

    The Cauchy-Green deformation tensors provide measures of the change in length of line elements between the reference and deformed configurations.
    \newline
    \newline
    Given an infinitesimal line element in the reference configuration, $\mathbf{d}\vect{X}$, suppose we want to determine its length under deformation. Recall from the section on the deformation tensor that,
    \[\mathbf{d}\vect{x}=\tens{F}\cdot\mathbf{d}\vect{X}\]
    From this we have that the square length of $\mathbf{d}\vect{x}$ is given by,
    \begin{align*}
        \mathbf{d}\vect{x}\cdot\mathbf{d}\vect{x}&=\tens{F}\cdot\mathbf{d}\vect{X}\cdot\tens{F}\cdot\mathbf{d}\vect{X}\\
        &=F_{ij}\mathrm{d}X_jF_{ik}\mathrm{d}X_k\\
        &=\mathrm{d}X_jF_{ij}F_{ik}\mathrm{d}X_k\\
        &=\mathrm{d}\vect{X}\cdot\tens{F}^T\cdot\tens{F}\cdot\mathrm{d}\vect{X}
    \end{align*}
    The \textbf{right Cauchy-Green deformation tensor} is the factor, 
    \[\tens{C}=\tens{F}^T\cdot\tens{F}\quad\Leftrightarrow\quad C_{ij}=F_{ij}F_{ik}\]
    If instead we wish to find the length of $\mathbf{d}\vect{X}$ as a function of $\mathbf{d}\vect{x}$ we get,
    \[\mathbf{d}\vect{X}\cdot\mathbf{d}\vect{X}=\mathrm{d}\vect{x}\cdot\tens{F}^{-T}\cdot\tens{F}^{-1}\cdot\mathrm{d}\vect{x}\]
    The factor $\tens{B}^{-1}=\tens{F}^{-T}\cdot\tens{F}^{-1}$ is the inverse of the \textbf{left Cauchy-Green deformation tensor},
    \[\tens{B}=\tens{F}\cdot\tens{F}^T\]
    
    Furthermore, we note that the Cauchy-Green deformation tensors independent of rotations induced by deformation. To see this, consider right Cauchy-Green deformation tensor (under right polar decompositions of the deformation gradient),
    \begin{align*}
        \tens{F}^T\cdot\tens{F}&=\tens{P}^T\tens{R}^T\cdot\tens{R}\cdot\tens{P}\\
        &=\tens{P}^T\cdot\tens{P},\quad\textrm{since $\tens{R}$ is a unitary matrix}
    \end{align*}
    Therefore the right Cauchy-Green deformation tensor eliminates the rotations due to deformation. A similar argument can be taken using the left polar decomposition to show the rotational independence of the left Cauchy-Green deformation tensor.

    \subsubsection{Strain Measures (Strain Tensors)}

    \paragraph{Green-Lagrange Strain Tensor}

    The Green-Lagrange strain tensor is defined as, 
    \[\tens{E}=\frac{1}{2}\left(\tens{C}-\tens{I}\right)\quad\Leftrightarrow\quad E_{ij}=\frac{1}{2}\left(C_{ij}-\delta_{ij}\right)=\frac{1}{2}\left(\frac{\partial x_k}{\partial X_i}\frac{\partial x_k}{\partial X_j}-\delta_{ij}\right)\]
    where $\tens{C}$ is the right Cauchy deformation tensor. And is a Lagrangian description of strain. Furthermore, the Green-Lagrange strain tensor inherits the rotation independence of the right Cauchy-Green deformation tensor.
    We can also defined the Green-Lagrange strain tensor in terms of the displacement field,
    \begin{align*}
        \tens{E}&=\frac{1}{2}\left(\tens{F}^T\cdot\tens{F}-\tens{I}\right)\\
        &=\frac{1}{2}\left((\tens{I}+(\nabla_{\vect{X}}\vect{U})^T)\cdot(\tens{I}+\nabla_{\vect{X}}\vect{U})-\tens{I}\right)\\
        &=\frac{1}{2}\left((\nabla_{\vect{X}}\vect{U})^T+\nabla_{\vect{X}}\vect{U}+(\nabla_{\vect{X}}\vect{U})^T\cdot\nabla_{\vect{X}}\vect{U}\right)
    \end{align*}
    which in index notation is,
    \[E_{ij}=\frac{1}{2}\left(\frac{\partial U_j}{\partial X_i}+\frac{\partial U_i}{\partial X_j}+\frac{\partial U_k}{\partial X_i}\frac{\partial U_k}{\partial X_j}\right)\]

    \paragraph{Eulerian-Almansi Strain Tensor}

    The Almansi strain tensor is defined as, 
    \[\tens{e}=\frac{1}{2}\left(\tens{I}-\tens{B}^{-1}\right)\quad\Leftrightarrow\quad e_{ij}=\frac{1}{2}\left(\delta_{ij}-\frac{\partial X_k}{\partial x_i}\frac{\partial X_k}{\partial x_j}\right)\]
    where $\tens{B}$ is the left Cauchy deformation tensor. And is a Eulerian description of strain. Furthermore, the Almansi strain tensor inherits the rotation independence of the left Cauchy-Green deformation tensor. To see where this comes from note that,
    
    \[\tens{B}^{-1}=\tens{F}^{-T}\tens{F}^{-1}=\tens{H}^T\tens{H}=\frac{\partial X_k}{\partial x_i}\frac{\partial X_k}{\partial x_j}\]
    We can also defined the Almansi strain tensor in terms of the displacement,
    \begin{align*}
        \tens{e}&=\frac{1}{2}\left(\tens{I}-\tens{F}^{-T}\cdot\tens{F}^{-1}\right)\\
        &=\frac{1}{2}\left(\tens{I}-\tens{H}^{T}\cdot\tens{H}\right)\\
        &=\frac{1}{2}\left(\tens{I}-(\tens{I}-(\nabla_{\vect{x}}\vect{u})^T)\cdot(\tens{I}-\nabla_{\vect{x}}\vect{u})\right)\\
        &=\frac{1}{2}\left((\nabla_{\vect{x}}\vect{u})^T+\nabla_{\vect{x}}\vect{u}-(\nabla_{\vect{x}}\vect{u})^T\cdot\nabla_{\vect{x}}\vect{u}\right)
    \end{align*}
    which in index notation is,
    \[e_{ij}=\frac{1}{2}\left(\frac{\partial u_j}{\partial x_i}+\frac{\partial u_i}{\partial x_j}+\frac{\partial u_k}{\partial x_i}\frac{\partial u_k}{\partial x_j}\right)\]

    \paragraph{Infinitesimal Strain Tensor}

    The infinitesimal Strain Tensor, AKA small strain tensor, is a linearization of the Green-Lagrange, and Almansi strain tensors. If we assume that displacements are small, $\Vert \vect{U}\Vert\ll 1$ and $\Vert \vect{u}\Vert\ll 1$, then $\vect{X}\approx \vect{x}$. If we also assume that displacement gradients are small, $\Vert\nabla_{\vect{X}}\vect{U}\Vert\ll 1$ and $\Vert\nabla_{\vect{x}}\vect{u}\Vert\ll 1$, then $\nabla_{\vect{X}}\vect{U}\approx \nabla_{\vect{x}}\vect{u}$ and the nonlinear terms become negligible leading to the linearization called the \textbf{infinitesimal strain tensor},
    \[\tens{E}\approx\tens{e}\approx \upvarepsilon=\frac{1}{2}\left((\nabla_{\vect{X}}\vect{U})^T+\nabla_{\vect{X}}\vect{U}\right)\]
    which in index notation is,
    \[\varepsilon_{ij}=\frac{1}{2}\left(\frac{\partial U_j}{\partial X_i}+\frac{\partial U_i}{\partial X_j}\right)\]
    We can write this in terms of the deformation gradient, by again using $\tens{F}=\tens{I}+\nabla_{\vect{X}}\vect{U}$,
    \begin{align*}
        \upvarepsilon&=\frac{1}{2}\left((\nabla_{\vect{X}}\vect{U})^T+\nabla_{\vect{X}}\vect{U}\right)\\
        &=\frac{1}{2}\left(\tens{F}^T-\tens{I}+\tens{F}-\tens{I}\right)\\
        &=\frac{1}{2}\left(\tens{F}^T+\tens{F}\right)-\tens{I}
    \end{align*}
    By working under the assumption of samll displacement we were able to find a linear strain tensor. However, this strain tensor has a problem, it is not independent of rigid body roations in the same fashion that the Green-Lagrange, and Almansi strain tensors are.  

    \paragraph{Corotational Strain Tensor}

    The corotional strain tensor is a modification of the infinitesimal strain tensor so as to make it independent of rotations, and thus allow it to be a reasonable measure of strain in a broader set of situations. The modification introduces some nonlinearity, but not to the same degree as in the Green-Lagrange strain tensor.
    \newline
    \newline
    Consider the infinitesimal strain tensor, with the deformation gradient expressed as a polar decomposition,
    \begin{align*}
        \upvarepsilon&=\frac{1}{2}\left(\tens{F}^T+\tens{F}\right)-\tens{I}\\
        &=\frac{1}{2}\left(\tens{P}^T\tens{R}^T+\tens{R}\tens{P}\right)-\tens{I}
    \end{align*}
    if we now extract the rotational factors then we get the corotated strain tensor,
    \begin{align*}
        \upeta&=\upvarepsilon(\tens{R}^T\tens{F})\\
        &=\frac{1}{2}\left(\tens{P}^T+\tens{P}\right)-\tens{I}\\
        &=\tens{P}-\tens{I},\quad\textrm{since $\tens{P}$ is symmetric}
    \end{align*}

    \subsubsection{Material Time Derivatives}

    The \textbf{material (time) derivative} is the rate of change recorded by an observer that travels with a material point. The material derivative can be computed on property fields that are expressed in either Lagrangian or Eulerian descriptions. 
    \begin{itemize}
        \item Given some property field in Lagrangian description form, $\phi(\vect{X},t)$, the material derivative is,
        \[\frac{\mathrm{D}\phi(\vect{X},t)}{\mathrm{D}t}=\frac{\partial\phi(\vect{X},t)}{\partial t}=\dot{\phi}(\vect{X},t)\]
        \item Given some property field in Eulerian description form, $\phi(\vect{x},t)$, the material derivative is,
        \[\frac{\mathrm{D}\phi(\vect{x},t)}{\mathrm{D}t}=\frac{\partial\phi(\vect{x},t)}{\partial \vect{x}}\cdot\frac{\partial\vect{x}}{\partial t}+\frac{\partial\phi(\vect{x},t)}{\partial t}=\nabla_{\vect{x}}\phi(\vect{x},t)\cdot\vect{v}+\dot{\phi}(\vect{x},t)\]
        where $\vect{v}$ is the velocity vector of the material point in Eulerian description.
    \end{itemize}
    When computing the material derivative (which is simply the totatl derivative wrt $t$, from multivariable calculus), we are moving with the material point, and so we treat $X$ as constant. Furthermore, the symbol $\frac{D\phi}{Dt}$ is sometimes used to represent the material derivative in other literature.
    \newline
    \newline
    Instead of travelling with a material point, we can fix the observation position and record the rate of change of property fields as material points pass through this position, this is called the \textbf{spatial (time) derivative}. By fixing the observation position, we must treat $\vect{x}$ as fixed when computing the derivative. Given some property field in Eulerian description form, $\phi(\vect{x},t)$, the spatial derivative is,
    \[\frac{\mathrm{d}\phi(\vect{x},t)}{\mathrm{d}t}=\frac{\partial\phi(\vect{x},t)}{\partial t}=\dot{\phi}(\vect{x},t)\]
    Notice that this is the second term in the material derivative of a property field in Eulerian description.

    \subsection{Mass, Density, and Forces} 

    \paragraph{Mass and Density} Given a domain $\Omega$, and density field, $\rho(\vect{x},t)$, over the domain, the \textbf{total mass} is,
    \[M_\Omega=\int_{\Omega}\rho(\vect{x},t)\;\mathrm{d}V\]
    and the \textbf{total volume} is,
    \[V_\Omega=\int_{\Omega}1\;\mathrm{d}V\]
    The \textbf{density} at a point $\vect{x}$, at time $t$, in the domain is given by,
    \[\rho(\vect{x},t)=\lim_{\Omega\rightarrow 0}\frac{M_\Omega}{V_\Omega}\]
    where $\Omega\rightarrow 0$ means a sequence of domains decreasing in size, such that $\vect{x}$ lies in their intersection.

    \paragraph{Forces} The mass, force, and acceleration of a point mass are related via Newton's Second Law,
    \[F=ma\]
    The two categories of external forces are:
    \begin{itemize}
        \item \textbf{body forces} act 'at a distance' on the body of an object, for example gravity, and have a magnitude that is proportional to the mass.
        \item \textbf{surface forces} are contact forces acting on the surface, for example the collision of two objects, and have a magintude proportional to area. 
    \end{itemize}

    \subsection{Traction Vectors}

    When studying the stress experienced by a body we use the concept of \textbf{traction vectors}. By the Euler-Cauchy Stress Principle, if we consider a body being made up of two then the action of one part on the other is equal to the system of forces and couples on the surface dividing the two parts. This system is represented by a \textbf{traction vector} field defined on the surface. 
    
    Furthermore, by Cauchy's Fundamental Lemma, the traction vector acting one side of the surface is equal in magnitude but opposite in direction to the traction vector acting on the other side of the surface.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.7\textwidth]{images/resultant_traction_vectors.png}}
    \end{figure*}
    \FloatBarrier

    A traction vector is simply a force per unit area acting on a surface. By considering virtual planes inside a domain we can use traction vectors to understand the internal resultant forces due to applied external forces (surface and body forces).

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.7\textwidth]{images/deformed_traction_vectors.png}}
    \end{figure*}
    \FloatBarrier

    \subsection{Stress Measures}

    Stress is force per orientated area. Stress tensors fields encode this information, and can be used to determine the traction vector acting on a given plane at a given point. Furthermore there is no unique stress tensor, as it depended on what frame the force and area are taken from i.e. the reference frame or the deformed frame.

    \paragraph{Cauchy Stress Tensor} 
    
    The Cauchy stress tensor, $\upsigma$, provides a measure of stress based on the actual force per deformed area. Let $\hat{\vect{n}}$ be the unit normal vector of some surface in the deformed configuration, then the Cauchy traction vector on the surface due to stress is,
    \[\vect{t}_{\vect{x}}=\hat{\vect{n}}\cdot\upsigma=\upsigma^T\cdot\hat{\vect{n}}\]

    \paragraph{First Piola-Kirchhoff Stress Tensor}
    
    The first Piola-Kirchhoff stress tensor, $\tens{P}$, provides a measure of the actual force per unit reference area. Let $\hat{\vect{N}}$ be the unit normal vector of some surface in the reference configuration, then the first Piola-Kirchhoff traction vector on the surface due to stress is,
    \[\vect{t}_{\vect{X}}=\tens{P}\cdot\hat{\vect{N}}=\hat{\vect{N}}\cdot\tens{P}^T\]
    Furthermore, the first Piola-Kirchhoff stress tensor is related, through an invertable relationship, to the Cauchy stress tensor by,
    \[\tens{P}=J\upsigma\cdot\tens{F}^{-T}\]
    In addition, we can also relate the first Piola-Kirchhoff traction vector to the Cauchy traction vector,
    \begin{align*}
        \vect{t}_{\vect{X}}&=\tens{P}\cdot\hat{\vect{N}}\\
        &=\tens{P}\cdot\hat{\vect{N}}\\
        &=J\upsigma\cdot\tens{F}^{-T}\cdot\hat{\vect{N}}\\
        &=\upsigma\cdot\hat{\vect{n}}\frac{\mathrm{d}a}{\mathrm{d}A},\quad\textrm{by Nanson's formula}\\
        &=\vect{t}_{\vect{x}}\frac{\mathrm{d}a}{\mathrm{d}A},\quad\textrm{by definition of Cacuhy stress tensor}\\
    \end{align*}
    which leads to,
    \[\vect{t}_{\vect{X}}\mathrm{d}A=\vect{t}_{\vect{x}}\mathrm{d}a\]
    From this we can see that the Cauchy traction vector and the Piola-Kirchhoff traction vector have the same direction but different magnitudes.

    \paragraph{Second Piola-Kirchhoff Stress Tensor} 
    
    The second Piola-Kirchhoff stress tensor, $\tens{S}$, provides a measure of the transformed force per unit reference area. Let $\hat{\vect{N}}$ be the unit normal vector of some surface in the reference configuration, then the second Piola-Kirchhoff traction vector on the surface due to stress is,
    \[\vect{t}^*_{\vect{X}}=\tens{S}\cdot\hat{\vect{N}}=\hat{\vect{N}}\cdot\tens{S}^T\]
    Furhtermore, the second Piola-Kirchhoff stress tensor is related, through an invertable relationship, to the Cauchy stress tensor by,
    \[\tens{S}=J\tens{F}^{-1}\cdot\upsigma\cdot\tens{F}^{-T}\]

    \subsection{WIP - Why So Many Stress and Strain Definitions}

    Consider for a moment determining the mechanical properties of a solid material. It is common to used standardized tests, for example the uniaxial tension test. But to make to results independent of the specimen size they are usually presented as \textit{stress vs strain}. Now, for the uniaxial tension test this quickly raises a question, if sufficiently large deformation occurs then the cross-sectional area of the specimen will change, and so do we compute stress based on the original cross-section or the new cross-section. Usually both results based on both are reported, with stress calculated based on the original area called nominal stress, and stress calculated on the new area called true stress. 

    \subsection{Conservation and Balance Laws}

    \subsubsection{Conservation of Mass (Continuity Equation)}

    The principle of mass conservation states that for any system that is closed to matter and energy transfer, the mass of the system will remain constant. This implies that mass cannot be created nor destroyed in these closed systems.
    
    \paragraph{Eulerian Description}
    
    Let $\rho(\vect{x},t)$ be the mass density field, $\Omega_t$ the domain that contains the body, and $m(t)$ be the mass of the system over time then,
    \[\frac{\mathrm{D}}{\mathrm{D}t}m(t)=\frac{\mathrm{D}}{\mathrm{D}t}\int_{\Omega_t}\rho(\vect{x},t)\;\mathrm{d}v\]
    For a system to satisfy the mass conservation principle we have $\frac{\mathrm{D}}{\mathrm{D}t}m(t)=0$ which gives using the \textbf{integral form of the mass conservation condition} in Eulerian description,
    \[\int_{\Omega_t}\frac{\mathrm{D}}{\mathrm{D}t}\rho(\vect{x},t)+\rho(\vect{x},t)\nabla_{\vect{x}}\cdot\vect{v}\;\mathrm{d}v=0\]
    Since this must hold for an arbitary choice of $\Omega_t$, the integrand must be zero giving the \textbf{differential form of the mass conservation condition} in Eulerian description, AKA \textbf{continuity equation},
    \[\frac{\mathrm{D}}{\mathrm{D}t}\rho(\vect{x},t)+\rho(\vect{x},t)\nabla_{\vect{x}}\cdot\vect{v}=0\]
    
    \paragraph{Lagrangian Description}
    
    Let $\rho_0(\vect{X})$ and $\rho(\vect{x},t)$ be the mass density field in the reference and deformed configurations respectively, and let $\Omega_0$ and $\Omega_t$ the domain containing the body in the reference and deformed configurations respectively. If mass remains constant in the system then,
    \[\int_{\Omega_0}\rho_0(\vect{X})\;\mathrm{d}V=\int_{\Omega_t}\rho(\vect{x},t)\;\mathrm{d}v\]
    If we use the relation, $\mathrm{d}v=JdV$, where $J$ is the Jacobian then the \textbf{integral form of the mass conservation condition} in Lagrangian description is,
    \[\int_{\Omega_0}\rho_0(\vect{X})\;\mathrm{d}V=\int_{\Omega_0}\rho(\vect{x}(\vect{X},t),t)J(\vect{X},t)\;\mathrm{d}V\]
    Since this must hold for an arbitary choice of $\Omega_0$, the integrand must be zero giving the \textbf{differential form of the mass conservation condition} in Lagrangian description,
    \[\rho_0=\rho J\]
    
    \subsubsection{Conservation of Linear Momentum}
    
    The principle of balance of linear momentum, AKA Newton's Second Law, states that sum of forces acting on a body is equal to the time rate of change of linear momentum of the body. Therefore, if the total force acting on a body is zero, then the time rate of change of linear momentum is zero, and so linear momentum must remain constant. The conservation of linear momentum equations are also called the equations of motion.

    \paragraph{Eulerian Description} 

    The applying the principle of balance of linear momentum to a body $\Omega_t$ we get,
    \[\frac{\mathrm{D}}{\mathrm{D}t}\int_{\Omega_t}\rho(\vect{x},t)\vect{v}(\vect{x},t)\;\mathrm{d}v=\int_{\partial\Omega_t}\vect{t}_{\vect{x}}(\vect{x},t)\;\mathrm{d}a+\int_{\Omega_t}\rho(\vect{x},t)\vect{b}(\vect{x},t)\;\mathrm{d}v\]
    where $\rho(\vect{x},t)$ is density, $\vect{t}_{\vect{x}}$ is a Cauchy traction vector, and $\vect{b}$ is a body force per unit mass. The reason the Cauchy traction vector appears as the surface force is because in Eulerian form the surface force is computed as the actual force per deformed surface area (since we are computing against the deformed configuration, $\Omega_t$). Applying Reynold's Transport theorm to the LHS, and using the Cauchy stress tensor formula in the surface integral on the RHS we get,
    \[\int_{\Omega_t}\rho(\vect{x},t)\frac{\partial\vect{v}}{\partial t}\;\mathrm{d}v=\int_{\partial\Omega_t}\upsigma^T\cdot\hat{\vect{n}}\;\mathrm{d}a+\int_{\Omega_t}\rho(\vect{x},t)\vect{b}(\vect{x},t)\;\mathrm{d}v\]
    Finally, using the Divergence Theorem on surface integral we arrive at the \textbf{integral form of the conservation of linear momentum equations} in Eulerian description,
    \[\int_{\Omega_t}\rho\dot{\vect{v}}-\nabla_{\vect{x}}\cdot\upsigma^T-\rho\vect{b}\;\mathrm{d}v=0\]
    Since this must be vaild for arbitrary choices of $\Omega_t$ the integrand must be zero leading to the \textbf{differential form of the conservation of linear momentum equations} in Eulerian description,
    \[\rho\dot{\vect{v}}-\nabla_{\vect{x}}\cdot\upsigma^T-\rho\vect{b}=0\]

    \paragraph{Lagrangian Description} 

    Converting the principle of balance of linear momentum above from Eulerian form to Lagranian form, using $\mathrm{d}v=J(\vect{X},t)\mathrm{d}V$ and $\vect{t}_{\vect{x}}\mathrm{d}a=\vect{t}_{\vect{X}}\mathrm{d}A$ we get,

    \[\frac{\mathrm{D}}{\mathrm{D}t}\int_{\Omega_0}\rho(\vect{x}(\vect{X},t),t)\vect{v}(\vect{x}(\vect{X},t),t)\;J(\vect{X},t)\;\mathrm{d}V=\int_{\partial\Omega_0}\vect{t}_{\vect{X}}(\vect{X},t)\;\mathrm{d}A+\int_{\Omega_0}\rho(\vect{x}(\vect{X},t),t)\vect{b}(\vect{x}(\vect{X},t),t)\;J(\vect{X},t)\;\mathrm{d}V\]
    By the conservation of mass principle this becomes,
    \[\frac{\mathrm{D}}{\mathrm{D}t}\int_{\Omega_0}\rho_0(\vect{X})\vect{v}(\vect{x}(\vect{X},t),t)\;\mathrm{d}V=\int_{\partial\Omega_0}\vect{t}_{\vect{X}}(\vect{X},t)\;\mathrm{d}A+\int_{\Omega_0}\rho_0(\vect{X})\vect{b}(\vect{x}(\vect{X},t),t)\;\mathrm{d}V\]
    Using Reynold's Transport theorm on the LHS, the definition of the first Piola-Kirchhoff stress tensor on the surface integral term, and defining $\vect{V}(\vect{X},t)=\vect{v}(\vect{x}(\vect{X},t),t)$ and $\vect{B}(\vect{X},t)=\vect{b}(\vect{x}(\vect{X},t),t)$, then we get,
    \[\int_{\Omega_0}\rho_0\dot{\vect{V}}\;\mathrm{d}V=\int_{\partial\Omega_0}\tens{P}\cdot\hat{\vect{N}}\;\mathrm{d}A+\int_{\Omega_0}\rho_0\vect{B}\;\mathrm{d}V\]
    Finally we use the divergence theorem to change the surface integral into a volume integral, arriving at the \textbf{integral form of the conservation of linear momentum equation} in Lagragian form,
    \[\int_{\Omega_0}\rho_0\dot{\vect{V}}-\nabla_{\vect{X}}\cdot\tens{P}-\rho_0\vect{B}\;\mathrm{d}V=0\]
    Since this must be vaild for arbitrary choices of $\Omega_0$ the integrand must be zero leading to the \textbf{differential form of the conservation of linear momentum equations} in Lagrangian form,
    \[\rho_0\dot{\vect{V}}-\nabla_{\vect{X}}\cdot\tens{P}-\rho_0\vect{B}=0\]
    
    \subsubsection{Conservation of Angular Momentum}

    The principle of conservation of angular momentum states that the time rate of change of the total angular momentum of a body is equal to the sum of the moments acting on the body. 
    
    \paragraph{Eulerian Description}
    
    In the Eulerian description, principle of conservation of angular momentum distills down to the condition that the Cauchy stress tensor must be symmetric,
    \[\upsigma=\upsigma^T\quad\Leftrightarrow\quad\epsilon_{ijk}\upsigma_{jk}=0\]
    Note here that $\epsilon_{ijk}$ is the Levi-Civita symbol.
    
    \paragraph{Lagrangian Description}
    
    In the Lagrangian description, principle of conservation of angular momentum distills down to the conditions,
    \[\tens{F}\cdot\tens{P}^T=\tens{P}\cdot\tens{F}^T\]
    \[\tens{S}=\tens{S}^T\]
    where $\tens{F}$ is the deformation gradient, $\tens{P}$ is the first Piola-Kirchhoff stress tensor, and $\tens{S}$ is the second Piola-Kirchhoff stress tensor.
    
    \subsection{Solid Mechanics}
    \subsubsection{Constitutive Equations}
    \subsubsection{Linear Elasticity}
    \subsection{Fluid Mechanics}

\end{document}