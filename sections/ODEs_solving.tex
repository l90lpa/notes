
\documentclass[../main.tex]{subfiles}

\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{ODEs: General Solutions}
    \subsection{First-Order Linear ODEs}
    \subsubsection{Exact Differential Equations}

    Given the function $f(x,y(x))$ the total differential of $f$ is,
    \[\frac{df}{dx}=\frac{\partial f}{\partial x}+\frac{\partial f}{\partial y}\frac{dy}{dx},\quad\textrm{since }\frac{dx}{dx}=1\]
    Now suppose that we have an ODE of the form,
    \[ p(x,y)+q(x,y)\frac{dy}{dx}=0 \]
    If we can find a function $f$ such that, $f_x(x,y)=p(x,y)$ and $f_y(x,y)=q(x,y)$ then the left hand side is the total differential of $f$. Therefore, we can write, $\frac{df}{dx}=c$, and so if we integrate both side we get the implicit solution, $f(x,y)=c$. To find $f$, we can either integrate, $f_x(x,y)=p(x,y)$ or $f_y(x,y)=q(x,y)$. Effectively process is attempting to invert the chain rule applied to $f(x,y(x))$. Furthermore, $c$ can be thought of as parameterising a family of implicit solutions $f_c(x,y)$.
    
    \begin{defn}
        \textbf{Exact Differential Equation}:\newline
        An ODE of the form, $p(x,y)+q(x,y)\frac{dy}{dx}=0$ is called \textbf{exact} if, 
        \[\frac{\partial p}{\partial y}=\frac{\partial q}{\partial x}\]
        If an ODE is exact then there exists a function, $f$, which we call the \textbf{potential function}, such that,
        \[\frac{\partial f}{\partial x}=p\quad\textrm{and}\quad \frac{\partial f}{\partial y}=q\]
    \end{defn}

    \begin{strt}
        Solving the ODE,
        \[p(x,y)+q(x,y)\frac{dy}{dx}=0\]
        \begin{enumerate}
            \item check if the ODE is exact. If not this method cannot be used.
            \item find the potential function by resolving the integral in either of the equations,
            \[f(x,y)=\int p(x,y)\;dx\quad\textrm{OR}\quad f(x,y)=\int q(x,y)\;dy\]
            \item the implicit solution is,
            \[f(x,y(x))=c\]
            where the constant, $c$, parameterises a family of implicit solutions. Now rearrange the implicit solution to find $y(x)$ explicitly.
        \end{enumerate}
    \end{strt}

    \begin{exmp}
        \textit{Find the general solution to the problem},
        \[2xy-9x^2+(2y+x^2+1)y'=0\]
        \newline
        \newline
        First, we check that the ODE is exact. Let $p(x,y)=2xy-9x^2$ and $q(x,y)=2y+x^2+1$, then,
        \[p_y=2x,\quad q_x=2x\]
        hence the ODE is exact. Next, we look for the potential function, $f(x,y)$,
        \begin{align*}
            f(x,y)&=\int p(x,y)\;dx\\
            &=\int 2xy-9x^2\;dx\\
            &=x^2y-3x^3+h(y)
        \end{align*}
        where $h(y)$ is the 'constant' of integration. Using the fact that, $f$, must satisfy, $f_y=q$ then,
        \begin{align*}
            f_y=q\quad&\Rightarrow\quad x^2+h'(y)=2y+x^2+1\\
            &\Rightarrow\quad h'(y)=2y+1\\
            &\Rightarrow\quad h(y)=y^2+y+k
        \end{align*}
        where $k$ is the constant of integration. The implicit solution is then,
        \begin{align*}
            x^2y-3x^3+y^2+y+k=c\quad&\Rightarrow\quad x^2y-3x^3+y^2+y+k-c=0\\
            &\Rightarrow\quad y^2+(x^2+1)y-3x^3+c_1=0,\quad\textrm{where }c_1=k-c\textrm{ is constant}
        \end{align*}
        Finally, rearrange to find $y(x)$ explicitly. To find an explicit solution we note than the solution is quadratic in $y$ and so by the quadratic formula we get,
        \[y(x)=\frac{-(x^2+1)\pm\sqrt{(x^2+1)^2-4(-3x^3+c_1)}}{2}\]
        At this point if we had an intial value, we could find an explicit $c_1$, Furthermore, if we are only interested in real solutions then we could find the domains on which $(x^2+1)^2-4(-3x^3+c_1)\ge 0$. Also, if we had an initial value we could use this to determine which sign we require in front of the square root, by looking at the two values of the solution at the initial value point, and thus picking the sign that make the solutions value, the same as the initial value.
    \end{exmp}

    \subsubsection{Integrating Factor}

    Suppose we have an ODE of the form,

    \[ \frac{dy}{dx}+p(x)y=q(x) \]
    The integrating factor method is based on the observation that, if we could find some function $I(x)$, which we call the \textbf{integrating factor}, such that $I'(x)=I(x)p(x)$ then on multiplying the ODE by $I(x)$ we get,
    \begin{align*}
        I(x)\frac{dy}{dx}+I(x)p(x)y=I(x)q(x)\quad&\Rightarrow\quad I(x)\frac{dy}{dx}+I'(x)y=I(x)q(x)\\
        &\Rightarrow\quad\frac{d}{dx}\left(I(x)y\right)=I(x)q(x)\quad\textrm{inverse product rule}\\
        &\Rightarrow\quad y=\frac{1}{I(x)}\int I(x)q(x)\;dx
    \end{align*}
    From the definition of the integrating factor we find that it has the form,
    \[I'(x)=I(x)p(x)\quad\Rightarrow\quad I(x)=e^{\int p(x)\;dx}\]

    \begin{strt}
        Solving the ODE,
        \[ \frac{dy}{dx}+p(x)y=q(x) \]
        \begin{enumerate}
            \item compute the integrating factor (leaving out the constant of integration),
            \[I(x)=e^{\int p(x)\;dx}\]
            \item resolve the integral in the equation,
            \[y=\frac{1}{I(x)}\int I(x)q(x)\;dx\]
        \end{enumerate}
    \end{strt}

    \begin{exmp}
        \textit{Find the general solution to the ODE},
        \[xy'+2y=x^2-x+1\]
        \newline 
        \newline
        First, on rearranging we get,
        \[y'+\frac{2}{x}y=x-1+\frac{1}{x}\]
        hence $p(x)=2x^{-1}$ and $q(x)=x-1+x^{-1}$. Next, forming the integrating factor (where we skip the introduction of an integrating factor),
        \begin{align*}
            I(x)&=\exp\left(\int 2x^{-1}\;dx\right)\\
            &=\exp(2\ln|x|)\\
            &=x^2
        \end{align*}
        Finally, the solution is,
        \begin{align*}
            y&=\frac{1}{x^2}\int x^2(x-1+x^{-1})\;dx\\
            &=\frac{1}{x^2}\int x^3-x^2+x\;dx\\
            &=\frac{1}{x^2}\left(\frac{1}{4}x^4-\frac{1}{3}x^3+\frac{1}{2}x^2+c\right)\\
            &=\frac{1}{4}x^2-\frac{1}{3}x+\frac{1}{2}+\frac{c}{x^2}
        \end{align*}
        where $c$ is a constant of integration.
    \end{exmp}

    \subsubsection{Separation of Variables}

    Suppose we have an ODE of the form,

    \[\frac{dy}{dx}=f(y)g(x)\]
    then this ODE is called \textbf{separable}. We can rearrange its to have all of the $y$ terms on one side and all of the $x$ terms on the other as so,
    \begin{align*}
        &\frac{dy}{dx}=f(y)g(x)\\
        \Rightarrow& 1/f(y)\frac{dy}{dx}=g(x)\\
        \Rightarrow& 1/f(y)dy=g(x)dx
    \end{align*}
    Now if integrating both sides and rearrange in terms of $y$ gives the solution to the ODE.
    
    \begin{strt}
        Solving the ODE,
        \[\frac{dy}{dx}=f(y)g(x)\]
        \begin{enumerate}
            \item if $f(y)=0$ for any $y$ then this method cannot be used.
            \item resolve the integrals in the following equation and rearrange for $y$,
            \[\int 1/f(y) dy = \int g(x) dx\]
        \end{enumerate}
    \end{strt}

    

    \subsection{First-Order Nonlinear ODEs}
    
    \subsubsection{Bernoulli Differential Equations}
    Nonlinear ODEs the form,
    \[\frac{dy}{dx}+p(x)y=q(x)y^n\]
    are called \textbf{Bernoulli equations}. These equation can be reduced to linear ODEs under the change of variable, $v=y^{1-n}$ (from which we also have $v'=(1-n)y^{-n}y'$). To see this, first divide through by $y^n$,
    \[y^{-n}y'+p(x)y^{1-n}=q(x)\]
    then make the substitutions, $v=y^{1-n}$, and $v'=(1-n)y^{-n}y'$, giving,
    \[\frac{1}{1-n}v'+p(x)v=q(x)\quad\Rightarrow\quad v'+(1-n)p(x)v=q(x)(1-n)\]
    At this point we can use the integrating factor method with $I(x)=e^{\int(1-n)p(x)dx}$ to solve for $v$,
    \[v(x)=\frac{1-n}{I(x)}\int I(x)q(x)\;dx\]
    And hence applying the reverse change of variables we get,
    \[y(x)=v(x)^{\frac{1}{1-n}}=\left(\frac{1-n}{I(x)}\int I(x)q(x)\;dx\right)^\frac{1}{1-n}\]

    \begin{strt}
        Solving the ODE,
        \[\frac{dy}{dx}+p(x)y=q(x)y^n\]
        \begin{enumerate}
            \item compute the integrating factor,
            \[I(x)=e^{\int(1-n)p(x)dx}\]
            \item resolve the integrals in the following equation,
            \[y(x)=\left(\frac{1-n}{I(x)}\int I(x)q(x)\;dx\right)^\frac{1}{1-n}\]
        \end{enumerate}
    \end{strt}

    \begin{exmp}
        \textit{Find the general solution of the ODE},
        \[y'+\frac{y}{x}-\sqrt{y}=0\]
        \newline
        \newline
        First on rearranging,
        \[y'+\frac{1}{x}y=y^{1/2}\]
        we see that this has the form of a Beroulli equation, with $p(x)=x^{-1}$, $q(x)=1$, and $n=1/2$. Next, we compute the integrating factor,
        \begin{align*}
            I(x)&=\exp\left(\int \left(1-\frac{1}{2}\right)\frac{1}{x}\;dx\right)\\
            &=\exp\left(\frac{1}{2}\ln|x|\right)\\
            &=x^{1/2}
        \end{align*}
        Finally, the solution is,
        \begin{align*}
            y&=\left(\frac{1}{2}x^{-1/2}\int x^{1/2} \;dx\right)^{2}\\
            &=\left(\frac{1}{2}x^{-1/2}\left(\frac{2}{3}x^{3/2}+c\right)\right)^{2}\\
            &=\left(\frac{1}{3}x^2+\frac{c}{2}x^{-1/2}\right)^{2}
        \end{align*}
    \end{exmp}

    \subsection{Homogeneous Second-Order Linear ODEs}

    An homogeneous second-order linear ODE has the form,

    \begin{equation}
        a(x)y''+b(x)y'+c(x)y=0
        \label{eqn:general_second_order_linear_ode}
    \end{equation}

    \begin{defn}
        \textbf{Linear Independence}:\newline
        Two functions $f(x)$ and $g(x)$ on the interval $x\in[a,b]$ are linearly dependent if one is a constant multiple of the other. Otherwise they are linearly independent.
    \end{defn}

    \begin{theo}
        Let $y_1(x)$ and $y_2(x)$ be any two solutions of equation (\ref{eqn:general_second_order_linear_ode}) on the interval $x\in[a,b]$, and $W(y_1,y_2)$ be the Wronskian of them. Then either $W(y_1,y_2)=0$ for all $x\in[a,b]$ OR $W(y_1,y_2)\neq 0$ for all $x\in[a,b]$.
    \end{theo}

    \begin{theo}
        Let $y_1(x)$ and $y_2(x)$ be any two solutions of equation (\ref{eqn:general_second_order_linear_ode}) on the interval $x\in[a,b]$, and $W(y_1,y_2)$ be the Wronskian of them. Then $W(y_1,y_2)=0$ for all $x\in[a,b]$ if and only if $y_1(x)$ and $y_2(x)$ are linearly dependent.
    \end{theo}

    \begin{theo}
        Let $y_1(x)$ and $y_2(x)$ be two linearly independent solutions of equation (\ref{eqn:general_second_order_linear_ode}) on the interval $x\in[a,b]$, these solutions will be call \textbf{fundamental solutions}. Then all other solutions are linear combinations of the two fundamental solutions, and the generic linear combination is called the \textbf{general solution},
        \[c_1y_1(x)+c_2y_2(x)\]
    \end{theo}

    \subsubsection{Constructing a Linearly Independent Basis From a Known Solution}
    \label{sec:constructing_ode_basis}

    Let $y_1(x)$ be a known solution to equation (\ref{eqn:general_second_order_linear_ode}). Define the other solution as $y_2(x)=v(x)y_1(x)$ where $v(x)$ is an unknown function. If we can find a function $v$ such that $y_2$ is a solution to the ODE then $y_1$ and $y_2$ are linearly independent.

    \begin{exmp}
        \textit{Consider the ODE,}
        \[a(x)y''+b(x)y'+c(x)y=0\]
        \textit{and assume that $y_1(x)$ is a solution of this ODE. Find another linearly independent solution.}
        \newline
        \newline
        Define, $y_2(x)=v(x)y_1(x)$, and note that, 
        \[\frac{d}{dx}(vy_1)=v'y_1+vy_1'\quad\textrm{and}\quad\frac{d^2}{dx^2}(vy_1)=v''y_1+2v'y_1+vy_1''\]
        Now substituting $y_2$ into the ODE,
        \begin{align*}
            a(x)y_2''+b(x)y_2'+c(x)y_2=0\quad&\Rightarrow\quad a(x)(v''y_1+2v'y_1'+vy_1'')+b(x)(v'y_1+vy_1')+c(x)vy_1=0\\
            &\Rightarrow\quad v''a(x)y_1+v'(2a(x)y_1'+b(x)y_1)+v(a(x)y_1''+b(x)y_1'+c(x)y_1)=0\\
            &\Rightarrow\quad v''a(x)y_1+v'(2a(x)y_1'+b(x)y_1)=0\\
            &\Rightarrow\quad \frac{v''}{v'}=-2\frac{y_1'}{y_1}-\frac{b(x)}{a(x)}\\
            &\Rightarrow\quad \log(v')=-2\log(y_1)-\int\frac{b(x)}{a(x)}\;dx,\quad\textrm{on integrating both sides}\\
            &\Rightarrow\quad v'=\frac{1}{y_1^2}e^{-\int\frac{b(x)}{a(x)}\;dx},\quad\textrm{on taking the exponential of both sides}\\
            &\Rightarrow\quad v=\int\frac{1}{y_1^2}e^{-\int\frac{b(x)}{a(x)}\;dx}\;dx,\quad\textrm{on integrating both sides}
        \end{align*}
        Thus we have,
        \[y_2(x)=y_1(x)\int\frac{1}{y_1^2(x)}e^{-\int\frac{b(x)}{a(x)}\;dx}\;dx\]
    \end{exmp}

    \begin{exmp}
        \textit{Consider the ODE,}
        \[ay''+by'+cy=0\]
        \textit{such that $b^2=4ac$, and $y_1(x)=e^{-\frac{b}{2a}x}$ is a solution. Find another linearly independent solution.}
        \newline
        \newline
        From example above we have, 
        \begin{align*}
            y_2(x)&=y_1(x)\int\frac{1}{y_1^2(x)}e^{-\int\frac{b(x)}{a(x)}\;dx}\;dx\\
            &=e^{-\frac{b}{2a}x}\int\frac{1}{e^{-\frac{b}{a}x}}e^{-\int \frac{b}{a}\;dx}\;dx\\
            &=e^{-\frac{b}{2a}x}\int\frac{1}{e^{-\frac{b}{a}x}}e^{-\frac{b}{a}x-c_1}\;dx,\quad c_1\textrm{ is a constant of integration}\\
            &=e^{-\frac{b}{2a}x}\int e^{-c_1}\;dx\\
            &=e^{-\frac{b}{2a}x}(e^{-c_1}x+c_2),\quad c_2\textrm{ is a constant of integration}
        \end{align*}
        Now, $y_2$ is linearly independent with respect to $y_1$ for any $c_1$ and $c_2$ and so to simplify things we choose $c_1=c_2=0$ to give,
        \[y_2(x)=xe^{-\frac{b}{2a}x}\]
    \end{exmp}

    \subsubsection{Constant Coefficients Type}

    Suppose we have an ODE of the form,

    \[ay''+by'+cy=0\]
    Now assume that $y=e^{\lambda x}$ is a solution to ODE for some unknown $\lambda$. To find $\lambda$ we substitute this solution in to the ODE giving,
    \begin{align*}
        a\lambda^2e^{\lambda x}+b\lambda e^{\lambda x}+ce^{\lambda x}=0\quad&\Rightarrow\quad a\lambda^2+b\lambda +c=0,\quad\textrm{dividing by }e^{\lambda x}\\
        &\Rightarrow\quad \lambda=\frac{-b\pm\sqrt{b^{2}-4ac}}{2a}
    \end{align*}
    The equation, $a\lambda^2+b\lambda +c=0$, is called the \textbf{characteristic equation} of the ODE. Clearly when $b^2-4ac\neq 0$ there are 2 distinct roots to the characteristic equation, and so 2 solutions to the ODE. Furthermore, these 2 solutions are linearly independent. If $b^2-4ac= 0$ then we have found only one soltuion, but we can construct another by using the procedure in section (\ref{sec:constructing_ode_basis})

    \begin{strt}
        Solving the ODE,
        \[ay''+by'+cy=0\]
        \begin{enumerate}
            \item compute the roots of the characteristic equation,
            \[\lambda=\frac{-b\pm\sqrt{b^{2}-4ac}}{2a}\]
            \item construct the general solution,
            \begin{itemize}
                \item if the characteristic equation has real and distinct roots ($b^{2}>4ac$), $\lambda_{1}$ and $\lambda_{2}$,
                    \[y(x)=Ae^{\lambda_{1}x}+Be^{\lambda_{2}x}\]
                    furthermore, if $\lambda_{1}=-\lambda_{2}=\lambda$, i.e. they are symmetric about $0$, then the general solution can 
                    also be expressed as,
                    \[y(x)=C\sinh(\lambda x)+D\cosh(\lambda x)\]
                \item if the characteristic equation has a single repeated real root ($b^{2}=4ac$), $\lambda$,
                    \[y(x)=Axe^{\lambda x}+Be^{\lambda x}=(Ax+B)e^{\lambda x}\]
                \item if the characteristic equation has complex roots ($b^{2}<4ac$), $\lambda_{1}=p+qi$ and $\lambda_{2}=p-qi$,
                    \[y(x)=Ae^{px}\cos(qx)+Be^{px}\sin(qx)\]
            \end{itemize}
        \end{enumerate}
    \end{strt}    

    \subsubsection{Cauchy-Euler Type}

    Suppose we have an ODE of the form,

    \[ax^{2}y''+bxy'+cy=0\]
    Now assume that $y=x^m$ is a solution to ODE for some unknown $m$. To find $m$ we substitute this solution in to the ODE giving,
    \begin{align*}
        ax^2m(m-1)x^{m-2}+bxmx^{m-1}+cx^m=0\quad&\Rightarrow\quad am^2+(b-a)m +c=0,\quad\textrm{dividing by }x^m\\
        &\Rightarrow\quad m=\frac{-(b-a)\pm\sqrt{(b-a)^{2}-4ac}}{2a}
    \end{align*}
    The equation, $am^2+(b-a)m +c=0$, is called the \textbf{auxillary equation} of the ODE. As in the constant coefficient case, the auxillary equation can have 1 or 2 roots. And similarly, if the auxillary equation only has 1 root then we can use the procedure in section (\ref{sec:constructing_ode_basis}) to construct a second linearly independent solution.

    \begin{strt}
        Solving the ODE,
        \[ax^{2}y''+bxy'+cy=0\]
        \begin{enumerate}
            \item compute the roots of the auxillary equation,
            \[m=\frac{-(b-a)\pm\sqrt{(b-a)^{2}-4ac}}{2a}\]
            \item construct the general solution,
            \begin{itemize}
                \item if the auxilary equation has real and distinct roots, $m_1$ and $m_2$,
                    \[y(x)=Ax^{m_1}+Bx^{m_2}\]
                \item if the auxilary equation has a single repeated real root $m$,
                    \[y(x)=Ax^{m}+B\ln(x)x^{m}\]
                \item if the auxilary equation has complex roots, $m_1=p+qi$ and $m_2=p-qi$,
                    \[y(x)=Ax^{p}\cos(q\ln(x))+Bx^{p}\sin(q\ln(x))\]
            \end{itemize}
        \end{enumerate}
    \end{strt}
    

    \subsubsection{Reducible Type}

    Suppose we have an ODE of the form

    \[a(x)y^{''}+b(x)y'=0\]
    Under the change of variables $z=y'$ the ODE reduces to the first order ODE,

    \[a(x)z'+b(x)z=0\quad\Rightarrow\quad z'=-\frac{b(x)}{a(x)}z\]
    This reduced form, is separable, and so can be solved by the separation of variables method giving,

    \[z = e^{-\int \frac{b(x)}{a(x)} dx}\]
    If we know reverse the change of variables, $y'=z$, and integrating both sides we get,

    \[y' = e^{-\int \frac{b(x)}{a(x)} dx}\quad\Rightarrow\quad y= \int e^{-\int \frac{b(x)}{a(x)} dx} dx\]

    \begin{strt}
        Solving the ODE,
        \[a(x)y^{''}+b(x)y'=0\]
        Simply resolve the integrals in the equations,
        \[y= \int e^{-\int \frac{b(x)}{a(x)} dx} dx\]
    \end{strt}
    
    \subsection{Inhomogeneous Second-Order Linear ODEs}
    
    Suppose we have an ODE of the form,

    \[y''+ p(x)y' + q(x)y = f(x)\]
    And let $y_h(x)=c_1y_1(x)+c_2y_2(x)$ be the general solution to the homogenuous form $y''+ p(x)y' + q(x)y =0$. For interest we note that the fundamental solutions, $y_1$ and $y_2$, form a basis for the kernel of the differential operator, $L=\frac{d^2}{dx^2}+p(x)\frac{d}{dx}+q(x)$. Now if we had we could find any \textbf{particular solution}, $y_{p}$, to the inhomogeneous ODE, that is some $y_p$ such that $y''_{p}+ p(x)y'_{p} + q(x)y_{p}=f(x)$, then the complete soltuion would be given by, $y=y_h+y_p$.
    \newline
    \newline
    The Method of Undetermined Coefficients, and Method of Variation of Parameters, are two common methods for finding particular solutions.

    \subsubsection{Method of Underdetermined Coefficients}

    This method is generally only successful when the coefficients of the ODE are constant. Furthermore, it imposes quite strict requirements on the forms that the nonhomogenuous term, $f(x)$, can take for the method to work.

    \begin{enumerate}
        \item Find the general solution, $y_{h}=c_1y_1(x)+c_2y_2(x)$, to the homogeneous ODE, $y''+ p(x)y' + q(x)y=0$.
        \item Make a guess at what a particular solution, $y_{p}$, to the inhomogeneous ODE might be, based on the 
            inhomogeneous part $f(x)$.
            \begin{itemize}
                \item \textbf{Basic components and associated guesses}:
                \begin{center}
                    \begin{tabular}{|c|c|}
                        \hline
                        \textbf{$f(x)$} & \textbf{$y_{p}$} guess\\
                        \hline
                        $ae^{bx}$ & $Ae^{bx}$\\
                        \hline
                        $a\cos(cx)$ & $A\cos(cx)+B\sin(cx)$\\
                        \hline
                        $b\sin(cx)$ & $A\cos(cx)+B\sin(cx)$\\
                        \hline
                        $a\cos(cx)+b\sin(cx)$ & $A\cos(cx)+B\sin(cx)$\\
                        \hline
                        nth degree polynomial & $A_{n}x^{n}+\dots+A_{0}$\\
                        \hline
                    \end{tabular}
                \end{center}
                \textbf{Remark}: if $f(x)$ is an nth degree polynomial, even if some of its coefficients (for degree less 
                than $n$) are zero the guess should contain all degrees.
                \item \textbf{Products of basics components}: if $f(x)$ is a product of the basic components listed in the table above then we 1) 
                determine the guess for each 'factor', 2) multiply the guesses together, and 3) 'merge' coefficients 
                together to reduce the number of unknowns. E.g. suppose $f(x)=xe^{-4x}$. The guesses for the individual 
                'factors' are $Ax+B$ and $Ce^{-4x}$. Multipling the guesses together we have, $(Ax+B)Ce^{-4x}=(ACx+BC)e^{-4x}$. 
                Finally we reduce unknowns by letting, $A_1=AC$ and $A_0=BC$, giving the guess, $(A_1x+A_0)e^{-4x}$ which 
                has only 2 unknowns.
                \item \textbf{Sum of basic/product components}: suppose that the nonhomogenuous part, $f(x)$ can be written as $f(x)=f_1(x)+f_2(x)$. If $y_{p1}$ is a particular solution to $y''+ p(x)y' + q(x)y=f_1(x)$, and $y_{p2}$ is a particular solution to $y''+ p(x)y' + q(x)y=f_2(x)$, then $y_{p1}+y_{p2}$ is a particular solution to $y''+ p(x)y' + q(x)y=f(x)$. Hence we handle sums of basic/product components by finding particular solutions for each term in the nonhomogenuous part separately, and then sum these particular solutions to form a particular solution for the original problem.
                \item \textbf{Linearly dependent guesses}: suppose the nonhomogenuous part is, $f(x)$, and the guess $y_p(x)$ is at most a product of basic components. If some term in the product of basic components, is also present in the homogeneous solution, then we should multiply the guess by $x^n$ to make the guess linearly independent, where $n$ is chosen to be as small as possible. For example, if $f(x)=9x^2e^{-4x}$ and the homogeneous solution is, $c_1e^{4x}+c_2xe^{-4x}$, the naive guess would be, $y_p(x)=(Ax^2+Bx+C)e^{-4x}$. Clearly, the term $Bxe^{-4x}$ is linearly independent with $c_2xe^{-4x}$. If we chose to multiple the guess by $x$ then the term $Cxe^{-4x}$ would be linearly dependent with $c_2xe^{-4x}$, and so we must instead choose to multiply by $x^2$, giving a guess of, $y_p(x)=x^2(Ax^2+Bx+C)e^{-4x}$.
            \end{itemize} 
        \item Substitue the guess for $y_{p}$ into the ODE and equate coefficients to make the LHS 
            ($y''_{p}+ p(x)y'_{p} + q(x)y_{p}$) equal the RHS ($f(x)$). (Note that we should have no undetermined 
            coefficients)
        \item The solution to the inhomogeneous equation is then, $y=y_{h}+y_{p}$
    \end{enumerate}

    \begin{exmp}
        \textit{Find a particular solution to the ODE},
        \[y''-4y'-12y=xe^{4x}\]
        \newline
        \newline
        First, the general soltuion to the homogeneous problem is,
        \[y_h(x)=c_1e^{2x}+c_2e^{6x}\]
        Next, the guess for the particular solution is,
        \[y_p(x)=(Ax+B)e^{4x}\]
        Substituting this into the ODE we get,
        \begin{align*}
            &e^{4x}\left(16Ax + 16B + 8A\right) - 4\left( e^{4x}\left(4Ax + 4B + A\right) \right) - 12\left( e^{4x}\left( Ax + B\right)\right) = xe^{4x}\\
            \Rightarrow\quad&\left( 16A - 16A - 12A\right)te^{4x} + \left( 16B + 8A - 16B - 4A - 12B\right)e^{4x} = xe^{4x}\\
            \Rightarrow\quad&- 12Axe^{4x} + \left( 4A - 12B \right)e^{4x} = xe^{4x}
        \end{align*}
        Finally, equating coefficients we have,
        \[-12A=1\quad\Rightarrow\quad A=-\frac{1}{12}\]
        \[4A-12B=0\quad\Rightarrow\quad B=-\frac{1}{36}\]
        Thus a particular solution is,
        \[y_p(x)=-\frac{1}{36}(3x+1)e^{4x}\]
    \end{exmp}

    \begin{exmp}
        \textit{Find an appropriate guess of the form of the particular solution for the ODE},
        \[y''-100y=9x^2e^{10x}+\cos(x)-x\sin(x)\]
        \textit{where the general solution is},
        \[y_h(x)=c_1e^{10x}+c_2e^{-10x}\]
        \newline
        \newline
        For the term $9x^2e^{10x}$ the guess is,
        \[x(Ax^2+Bx+C)e^{10x}\]
        where we have multiplyed by $x$ to make the terms linearly independent from the general solution. For the term, $\cos(x)$ the guess is,
        \[D\cos(x)+E\sin(x)\]
        And for the term, $x\sin(x)$ the guess is,
        \[(Fx+G)\cos(x)+(Hx+I)\sin(x)\]
        Summing these together we get,
        \[y_p(x)=x(Ax^2+Bx+C)e^{10x}+(Fx+D)\cos(x)+(Gx+E)\sin(x)\]
        where we have abused notation and replaced $D+G$ with $D$, and $E+I$ with $E$.
    \end{exmp}

    \subsubsection{Method of Variation of Parameters}

    This method is more tricky than the Method of Underdetermined Coefficients but works on a wider range of functions.

    \begin{enumerate}
        \item Find the general solution, $y_{h}=c_1y_1(x)+c_2y_2(x)$, to the homogeneous ODE, $y''+ p(x)y' + q(x)y = 0$,
        \item Compute the Wronskian of the fundamental solutions, $y_1(x)$ and $y_2(x)$, 
            \[W(y_{1},y_{2})=\det\left(\begin{bmatrix}
                y_{1}  & y_{2}  \\
                y'_{1} & y'_{2}
                \end{bmatrix}\right)=y_{1}y'_{2}-y_{2}y'_{1}\]
        \item A particular solution is then given by the formula,
                \[y_{p}(x)=-y_{1}(x)\int\frac{y_{2}(x)f(x)}{W(y_{1},y_{2})}dx+y_{2}(x)\int\frac{y_{1}(x)f(x)}{W(y_{1},y_{2})}dx\]
        \item The solution to the inhomogeneous equation is then, $y=y_{h}+y_{p}$
    \end{enumerate}

    \begin{exmp}
        \textit{Find a particular solution to the ODE},
        \[xy''-(x+1)y'+y=x^2\]
        \textit{where the fundamental solutions are}, $y_1=e^x$ and $y_2=x+1$.
        \newline
        \newline
        First, rearranging to get,
        \[y''-\frac{x+1}{x}y'+\frac{1}{x}y=x\]
        Now, computing the Wronskian of the fundamental solutions,
        \[W(y_1,y_2)=\left|\begin{matrix}
            e^x & x+1 \\
            e^x & 1 
        \end{matrix}\right|=e^x-e^x(x+1)=-xe^x\]
        The particular solution is then,
        \begin{align*}
            y_p(x) & =  - e^x\int\frac{\left( x+1 \right)x}{-xe^x}\;dx + \left( x+1 \right)\int\frac{e^x\left( x \right)}{ - xe^x}\;dx\\
            &  = e^x\int\left( x+1 \right)e^{-x}\;dx - \left( x+1 \right)\int 1\;dx\\
            &  = e^x\left(-e^{-x}\left(x+2\right)\right) - \left( x+1 \right)x\\
            &  =  -x^2-2x-2\end{align*}
    \end{exmp}


\end{document}