\documentclass[../main.tex]{subfiles}

\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{Analysis}

    \subsection{Set Theory}

    \begin{defn}
        \textbf{Set Operations}:\newline
        Let $I$ be a set of indices into the collection of sets $\{S_{i}\}$.
        \begin{enumerate}
            \item The \textbf{union}, $\bigcup_{i\in I}S_{i}$, of the $S_{i}$ indexed by $I$ is the set,
                \[\bigcup_{i\in I}S_{i}=\{s : s\in S_{i} \textrm{  for some  } i\in I\}\]
            \item The \textbf{intersection}, $\bigcap_{i\in I}S_{i}$, of the $S_{i}$ indexed by $I$ is the set,
            \[\bigcap_{i\in I}S_{i}=\{s : s\in S_{i} \textrm{  for all  } i\in I\}\]
        \end{enumerate}
    \end{defn}

    \begin{prop*}
        \textbf{Remark}: It is worth noting that the infinite intersection of open sets can result in sets that are 
        open, closed, empty, either open nor closed, etc.
    \end{prop*}

    \begin{exmp}
        Examples of countably infinite unions and intersections of sets:\newline
        \begin{enumerate}
            \item \textit{prove} $\bigcup^{\infty}_{n=1}\left(0,\frac{1}{n}\right)=(0,1)$:\newline
            First we show that $\bigcup^{\infty}_{n=1}\left(0,\frac{1}{n}\right)\subset(0,1)$. 
            Let $x\in\bigcup^{\infty}_{n=1}\left(0,\frac{1}{n}\right)$. Then for some $n$ we have $x\in\left(0,1/n\right)$,
            hence $0<x<1/n$. Since $n\ge 1$ this implies that $0<x<1$ therefore $x\in(0,1)$.\newline\newline
            Next we show that $(0,1)\subset\bigcup^{\infty}_{n=1}\left(0,\frac{1}{n}\right)$. 
            Let $x\in(0,1)$. Since $(0,1)$ is the first set in $\bigcup^{\infty}_{n=1}\left(0,\frac{1}{n}\right)$ then 
            by the definition of union $x\in\bigcup^{\infty}_{n=1}\left(0,\frac{1}{n}\right)$. We have shown the
            subset relation in both direction, thus $\bigcup^{\infty}_{n=1}\left(0,\frac{1}{n}\right)=(0,1)$.
        \end{enumerate}
    \end{exmp}

    \subsection{Sequences}

    \begin{defn}
        \textbf{Null Sequence}:\newline
        A sequence $\{a_{n}\}$ is \textbf{null} if for each positive $\epsilon$, there is an integer $N$ such that, 
        $|a_{n}|<\epsilon\quad\forall\quad n>N$
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.4\textwidth]{images/null_sequence.png}
        \end{figure}
    \end{defn}

    The sequence $\{a_{n}\}$ is null iff the sequence $\{|a_{n}|\}$ is null, and also iff the sequence 
    $\{(-1)^{n}a_{n}\}$ is null.

    The null sequence $\{a_{n}\}$ remains null if we add, remove, or alter a finite number of terms.

    \begin{prop*}
        \textbf{Combination Rules}:\newline
        Let $a_{n}$ and $b_{n}$ be null sequences, then,\newline
        \begin{tabular}{l l}
            sum rule& $\{a_{n}+b_{n}\}$ is null\\
            multiple rule& $\{\lambda a_{n}\}$ is null, for $\lambda\in\mathbb{R}$\\
            product rule& $\{a_{n} b_{n}\}$ is null\\
            power rule& $\{a_{n}^{p}\}$ is null, if $a_{n}\ge 0 \;\forall\; n$ and $p>0$\\
        \end{tabular}
    \end{prop*}

    \begin{defn}
        \textbf{Squeeze Rule}:\newline
        If $\{b_{n}\}$ is null and $|a_{n}|\le b_{n}$, for $n=1,2,...$ then $a_{n}$ is \textbf{dominated} by $b_{n}$ 
        and so $a_{n}$ is null.
    \end{defn}

    \textbf{Basic null sequences} (where the index is $n=1,2,...$):
    \begin{itemize}
        \item $\{1/n^{p}\}$, for $p>0$
        \item $\{c^{n}\}$, for $|c|<1$
        \item $\{n^{p}c^{n}\}$, for $p>0$ and $|c|<1$
        \item $\{c^{n}/n!\}$, for $c\in\mathbb{R}$
        \item $\{n^{p}/n!\}$, for $p>0$
        \item $\{(a^{1/n}) - 1\}$, for $a>0$
        \item $\{(n^{1/n}) - 1\}$
    \end{itemize}

    \begin{defn}
        \textbf{Convergent Sequence}:\newline
        The sequence, $\{a_{n}\}$ is convergent with limit $l$ if the sequence $\{a_{n}-l\}$ is a null sequence. That is
        $\lim_{n\rightarrow\infty}a_{n}=l$
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.4\textwidth]{images/convergent_sequence.png}
        \end{figure}  
    \end{defn}

    A sequence is \textbf{bounded} if there is a number $K$ such that $|a_{n}|<K$ for $n=1,2,...$ . All convergent 
    sequences are bounded but not all bounded sequences are convergent.

    \begin{prop*}
        \textbf{Combination Rules}:\newline
        Let $\lim_{n\rightarrow\infty}a_{n}=l$ and $\lim_{n\rightarrow\infty}b_{n}=m$ then,\newline
        \begin{tabular}{l l}
            sum rule& $\lim_{n\rightarrow\infty}(a_{n}+b_{n})=l+m$\\
            multiple rule& $\lim_{n\rightarrow\infty}(\lambda a_{n})=\lambda l$, for $\lambda\in\mathbb{R}$\\
            product rule& $\lim_{n\rightarrow\infty}(a_{n} b_{n})=lm$\\
            power rule& $\lim_{n\rightarrow\infty}(a_{n}^{p})=l^{p}$, if $a_{n}\ge 0 \;\forall\; n$ and $p>0$\\
            quotient rule& $\lim_{n\rightarrow\infty}(a_{n}/b_{n})=l/m$, if $m\ne 0$\\
        \end{tabular}
    \end{prop*}

    \begin{defn}
        \textbf{Squeeze Rule}:\newline
        If $\{a_{n}\}$, $\{b_{n}\}$, and $\{c_{n}\}$ satisfy:
        \begin{itemize}
            \item $b_{n} \le a_{n} \le c_{n}$, for $n=1,2,...$
            \item $\lim_{n\rightarrow\infty}b_{n}=\lim_{n\rightarrow\infty}c_{n}=l$
        \end{itemize}
        then $\lim_{n\rightarrow\infty}a_{n}=l$
    \end{defn}

    \begin{defn}
        \textbf{Limit Inequality Rule}:\newline
        If $a_{n}\le b_{n}$ for $n=1,2,...$ and the limits of the sequences exists $\lim_{n\rightarrow\infty}a_{n}=l$ 
        and $\lim_{n\rightarrow\infty}b_{n}=m$, then $l\le m$
    \end{defn}

    \begin{defn}
        \textbf{Divergent Sequence}:\newline
        A sequence is divergent if it is not convergent.
    \end{defn}

    All unbounded sequences are divergent, but not all divergent sequences are unbounded.

    \begin{defn}
        \textbf{Reciprocal Rule}:\newline
        If the sequence $\{a_{n}\}$ satisfies:
        \begin{itemize}
            \item $\{a_{n}\}$ is eventually positive
            \item $\{1/a_{n}\}$ is a null sequence
        \end{itemize}
        then $\lim_{n\rightarrow\infty}a_{n}=\infty$
    \end{defn}

    \begin{defn}
        \textbf{Squeeze Rule}:\newline
        If $b_{n}\rightarrow\infty$ and $a_{n}\ge b_{n}$ then $\lim_{n\rightarrow\infty}a_{n}=\infty$
    \end{defn}

    \begin{defn}
        \textbf{Subsequence}:\newline
        The sequence $\{a_{n_{k}}\}$ is a \textbf{subsequence} of $\{a_{n}\}$ if $n_{k}$ is a strictly increasing 
        sequence of positive integers i.e. $n_{1}<n_{2}<n_{3}<...$
    \end{defn}

    \begin{theo}
        For any subsequence $\{a_{n_{k}}\}$ of $\{a_{n}\}$:
        \begin{itemize}
            \item if $a_{n}\rightarrow l$ as $n\rightarrow\infty$ then $a_{n_{k}}\rightarrow l$ as $k\rightarrow\infty$
            \item if $a_{n}\rightarrow \infty$ as $n\rightarrow\infty$ then $a_{n_{k}}\rightarrow \infty$ as $k\rightarrow\infty$
        \end{itemize}
    \end{theo}

    \begin{coro}
        \textbf{Subsequence Rules}:
        \begin{list}{}{}
            \item \textbf{First Subsequence Rule} The sequence $\{a_{n}\}$ is divergent if $\{a_{n}\}$ has two 
            convergent subsequences with different limits
            \item \textbf{Second Subsequence Rule} The sequence $\{a_{n}\}$ is divergent if $\{a_{n}\}$ has a 
            subsequence which tends to $\infty$ or $-\infty$
        \end{list}
    \end{coro}

    \begin{theo}
        \textbf{Monotone Convergence Theorem}:\newline
        If $\{a_{n}\}$ is:
        \begin{list}{}{}
            \item EITHER increasing and bounded above,
            \item OR decreasing and bounded below,
        \end{list}
        then $\{a_{n}\}$ is convergent.
    \end{theo}

    \begin{exmp}
        \textit{Find the limit of the following sequence}\newline
        \[\lim_{n\rightarrow\infty}\frac{2^{n+1}+3^{n+1}}{2^n+3^n}\]
        First rewrite it as,
        \[\lim_{n\rightarrow\infty}\frac{2\cdot2^{n}+3\cdot3^{n}}{2^n+3^n}\]
        Now extract the fastest increasing factor,
        \begin{align*}
            \lim_{n\rightarrow\infty}\frac{2\cdot2^{n}+3\cdot3^{n}}{2^n+3^n}&=\lim_{n\rightarrow\infty}\frac{3^n(2\cdot\frac{2^{n}}{3^{n}}+3)}{3^n(\frac{2^{n}}{3^{n}}+1)}\\
            &=\lim_{n\rightarrow\infty}\frac{2\cdot\frac{2^{n}}{3^{n}}+3}{\frac{2^{n}}{3^{n}}+1}\\
            &=3
        \end{align*}
    \end{exmp}

    \subsection{Series}

    \begin{defn}
        \textbf{Geometric Series}:\newline
        A series is geometric if there is a common ratio between adjacent terms,
        \[\sum^{\infty}_{n=0}ar^{n}=a+ar+ar^{2}+...\]
        Convergence properties:
        \begin{itemize}
            \item If $|r|<1$, then $\sum^{\infty}_{n=0}ar^{n}$ is convergent, with limit $a/(1-r)$
            \item If $|r|\ge 1$ and $a\ne 0$, then $\sum^{\infty}_{n=0}ar^{n}$ is divergent
        \end{itemize}
    \end{defn}

    \begin{theo}
        If $\sum^{\infty}_{n=1}a_{n}$ is a convergent series, then its sequence of terms $\{a_{n}\}$ is a null sequence.
    \end{theo}

    \begin{defn}
        \textbf{Non-null Test}:\newline
        If $\{a_{n}\}$ is not a null sequence, then $\sum^{\infty}_{n=1}a_{n}$ is divergent.\newline
        \newline
        To show that a series is divergent using this test,\newline
        EITHER:\newline
        1. show that $\{|a_{n}|\}$ has a convergent subsequence with non-zero limit,\newline
        OR\newline
        2. show that $\{|a_{n}|\}$ has a subsequence which tends to infinity.\newline
        \newline
        \textbf{Warning} the Non-null Test cannot be used to prove that a series is convergent.
    \end{defn}

    \begin{defn}
        \textbf{Comparison Test}:\newline
        (a) If\newline
        $0 \le a_{n} \le b_{n},\quad n=1,2,... $ and $\sum^{\infty}_{n=1}b_{n}$ is convergent, then 
        $\sum^{\infty}_{n=1}a_{n}$ is convergent.\newline
        (b)\newline
        $0 \le b_{n} \le a_{n},\quad n=1,2,... $ and $\sum^{\infty}_{n=1}b_{n}$ is divergent, then 
        $\sum^{\infty}_{n=1}a_{n}$ is divergent.\newline
    \end{defn}

    \begin{defn}
        \textbf{Limit Comparison Test}:\newline
        Suppose that $\sum^{\infty}_{n=1}a_{n}$ and $\sum^{\infty}_{n=1}a_{n}$ have positive terms and that\newline
        $\frac{a_{n}}{b_{n}}\rightarrow L$ as $n\rightarrow\infty$,\newline
        where $L\neq 0$\newline
        (a) If $\sum^{\infty}_{n=1}b_{n}$ is convergent, then $\sum^{\infty}_{n=1}a_{n}$ is convergent.\newline
        (b) If $\sum^{\infty}_{n=1}b_{n}$ is divergent, then $\sum^{\infty}_{n=1}a_{n}$ is divergent.
    \end{defn}

    \begin{defn}
        \textbf{Ratio Test}:\newline
        Suppose that $\sum^{\infty}_{n=1}a_{n}$ has positive terms and that $\frac{a_{n+1}}{a_{n}}\rightarrow l$ as $n\rightarrow\infty$.\newline
        (a) If $0 \le l < 1$, then the series is convergent.\newline
        (b) If $1 < l$, then the series is divergent.\newline
        (c) If $l=0$, then the test is inconclusive.
    \end{defn}

    \begin{defn}
        \textbf{Absolute Convergence}:\newline
        The series $\sum^{\infty}_{n=1}a_{n}$ is \textbf{absolutely convergent} if $\sum^{\infty}_{n=1}|a_{n}|$ is 
        convergent.
    \end{defn}

    \begin{defn}
        \textbf{Absolute Convergence Test}:\newline
        If the series $\sum^{\infty}_{n=1}a_{n}$ is absolutely convergent then it is convergent.
    \end{defn}

    \begin{defn}
        \textbf{Alternating Test}:\newline
        Let $a_{n}=(-1)^{n+1}b_{n},\quad n=1,2,...$,\newline
        where $b_{n}$ is a decreasing null sequence with positive terms. Then\newline
        $\sum^{\infty}_{n=1}a_{n}=b_{1}-b_{2}+b_{3}-b_{4}+...$\newline
        is convergent.
    \end{defn}


    \begin{strt}
        To test the series $\sum a_{n}$ for convergence or divergence.
        \begin{enumerate}
            \item If you think that the sequence of terms $a_{n}$ is non-null, then try the \textbf{Non-null Test}.
            \item If $\sum a_{n}$ has all non-negative terms, then try one of the following:
            \begin{enumerate}
                \item Identify it as a combintation of \textbf{Basic series}
                \item \textbf{Comparison Test}
                \item \textbf{Limit Comparison Test}
                \item \textbf{Ratio Test}
            \end{enumerate}
            \item If $\sum a_{n}$ positive and negative terms, then try one of the follwing:
            \begin{enumerate}
                \item \textbf{Absolute Convergence Test}
                \item \textbf{Alternating Test}
            \end{enumerate}
        \end{enumerate}
    \end{strt}

    \begin{defn}
        \textbf{Cauchy Product}:\newline
        The \textbf{Cauchy Product} is the discrete convolution of two infinite series, and is equal to the product of 
        the two series. Given the series' $\sum_{n=0}^{\infty}a_{n}$ and $\sum_{n=0}^{\infty}b_{n}$,
        \[\left(\sum_{j=0}^{\infty}a_{j}\right)\cdot\left(\sum_{k=0}^{\infty}b_{k}\right)=\sum_{n=0}^{\infty}c_{n}\quad\textrm{where}\quad c_{n}=\sum_{m=0}^{n}a_{m}b_{n-m}\]
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.4\textwidth]{images/cauchy_product.png}
        \end{figure}
    \end{defn}

    \subsection{WIP - Continuity}

    \subsubsection{Continuity}

    \begin{defn}
        \textbf{Continuity}:\newline
        Let $f$ be a function with domain $X$ and let $c\in X$. Then $f$ is \textbf{continuous} at $c$ if: for any 
        $\epsilon>0$, there exists a $\delta>0$ such that $|f(c)-f(x)|<\epsilon$, for all $x\in X$ with $|c-x|<\delta$.
    \end{defn}

    \begin{defn}
        \textbf{Squeeze Rule}:\newline
        Let $f$, $g$, and $h$ be defined on an open interval $I$ and let $a\in I$. If 
        \begin{enumerate}
            \item $g(x)\le f(x) \le h(x)$, for $x\in I$
            \item $g(a)=f(a)=h(a)$
            \item $g,h$ are continuous at $a$
        \end{enumerate}
        then $f$ is continuous at $a$.
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.4\textwidth]{images/squeeze_rule_locally_continuous.png}
        \end{figure}
    \end{defn}

    \begin{theo}
        \textbf{Intermediate Value Theorem}:\newline
        Let $f$ be a function continuous on $[a,b]$ and $y$ be any value lying between $f(a)$ and $f(b)$. Then there
        exists a $c\in(a,b)$ such that $f(c)=y$
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.4\textwidth]{images/intermediate_value_theorem.png}                
        \end{figure}
    \end{theo}

    \begin{theo}
        \textbf{Extreme Value Theorem}:\newline
        Let $f$ be a function continuous on $[a,b]$. Then there
        exists numbers $c,d\in[a,b]$ such that $f(c)\le f(x)\le f(d)$ for all $x\in[a,b]$
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.4\textwidth]{images/extreme_value_theorem.png}
        \end{figure}
    \end{theo}

    \begin{theo}
        \textbf{Boundness Theorem}:\newline
        Let $f$ be a function continuous on a closed and bounded interval. Then $f$ if bounded on this interval and it 
        attains its bounds. That is there exists $m$ and $M$ such that $m\le f(x)\le M$ for all $x\in[a,b]$.
    \end{theo}

    \subsubsection{Absolute Continuity}
    
    \begin{defn}
        \textbf{Absolute Continuity}:\newline
        Let $f$ be a function defined on an interval $I\subseteq \mathbb{R}$. Then $f$ is \textbf{absolutely continuous} on 
        $I$ if: for any $\epsilon>0$, there exists a $\delta>0$ such that $\sum^{n}_{i=1}|f(b_{i})-f(a_{i})|<\epsilon$,
        for every finite collecton of pairwise disjoint intervals $(a_{1},b_{1}),(a_{2},b_{2}),...,(a_{n},b_{n})\subset I$ 
        with $\sum^{n}_{i=1}(b_{i}-a_{i})<\delta$ 
    \end{defn}

    \subsubsection{Uniform Continuity}
    
    \begin{defn}
        \textbf{Uniform Continuity}:\newline
        Let $f$ be a function defined on an interval $I\subset \mathbb{R}$. Then $f$ is \textbf{uniformly continuous} on 
        $I$ if: for any $\epsilon>0$, there exists $\delta>0$ such that, $|f(x)-f(y)|<\epsilon$, for all $x,y\in I$ 
        with $|x-y|<\delta$.
    \end{defn}

    \begin{theo}
        Let $f$ be defined on an interval $I$. Then $f$ is \textbf{not} uniformly continuous on $I$ iff there exists two
        sequences $\{x_{n}\}$ and $\{y_{n}\}$ in $I$, and $\epsilon>0$, such that:
        \begin{enumerate}
            \item $\lim_{n\rightarrow\infty}|x_{n}-y_{n}|=0$
            \item $|f(x_{n})-f(y_{n})|\ge\epsilon$ for all $n=1,2,...$
        \end{enumerate}
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.4\textwidth]{images/uniform_continuity.png}
        \end{figure}
    \end{theo}

    \begin{theo}
        Let $f$ be continuous on a bounded closed interval $[a,b]$. Then $f$ is uniformly continuous on the interval.
    \end{theo}

    \begin{theo}
        \textbf{Bolzano-Weierstrass Theorem}:\newline
        Any bounded sequence has a convergent subsequence.
    \end{theo}

    \subsubsection{Lipschitz Continuity}

    \begin{defn}
        \textbf{Local Lipschitz Continuity}:\newline
        Let $f$ be a function defined on a domain $X$ and let $c\in X$. Then $f$ is \textbf{locally Lipschitz continuous} on
        if: there is an open interval $I$ containing $c$ and a constant $C$ such that $|f(x)-f(y)|\le C|x-y|$ for all $x,y\in I$
    \end{defn}

    \begin{defn}
        \textbf{Lipschitz Continuity}:\newline
        Let $f$ be a function defined on an interval $I\subset \mathbb{R}$. Then $f$ is \textbf{Lipschitz continuous} on
        $I$ if: there is a constant $C$ such that $|f(x)-f(y)|\le C|x-y|$ for all $x,y\in I$
    \end{defn}

    \subsection{WIP - Limits}
    
    \begin{theo}
        \textbf{Weierstrass M Test}:\newline
        Let $\{f_{n}(x)\}$ be a sequence functions, where $f_{n}:X\rightarrow\mathbb{R}$. If the following two conditions hold,
        \begin{itemize}
            \item $|f_{n}(x)|\le M_{n}$ for all $x\in X$ and $n\ge 1$
            \item $\sum^{\infty}_{n=1}M_{n}<\infty$
        \end{itemize}
        then the series,
        \[\sum^{\infty}_{n=1}f_{n}(x)\]
        converges absolutely and uniformly on $X$ to some function, say $f(x)$.
    \end{theo}
    
    \subsection{WIP - Differentiation}

    \begin{defn}
        \textbf{Derivative}:\newline
        Let $f$ be defined on an open interval $I$, and $c\in I$. Then the \textbf{derivative} of $f$, given symbol $f'$, at $c$ is defined to be the limit,
        \[f'(c)=\lim_{x\rightarrow c}\frac{f(x)-f(c)}{x-c}=\lim_{h\rightarrow 0}\frac{f(c+h)-f(c)}{h}\]
    \end{defn}

    \begin{defn}
        \textbf{Locally Differentiable}:\newline
        A function $f$ is differentiable at a point $c$ if the derivative exists at $c$.
    \end{defn}

    \begin{defn}
        \textbf{Differentiable}:\newline
        A function $f$ is differentiable if it is differentiable at each point of its domain.
    \end{defn}

    \begin{theo}
        Let $f$ be differentiable at a point $c$. Then $f$ is continuous at $c$.
    \end{theo}

    \begin{coro}
        Let $f$ be discontinuous at a point $c$. Then $f$ is not differentiable at $c$.
    \end{coro}

    \begin{defn}
        \textbf{Local Extremum (Stationary Point) Theorem}:\newline
        If $f$ has a local extremum at $c$ and $f$ is differentiable there, then $f'(c)=0$. Such a point is also called a stationary point. 
    \end{defn}

    \begin{theo}
        \textbf{Mean Value Theorem}:\newline
        Let $f$ be continuous on a closed interval $[a,b]$ and differentiable on $(a,b)$. Then there exists a point $c$
        such that,
        \[f'(c)=\frac{f(b)-f(a)}{b-a}\]
        \begin{figure}[H]
            \centering
            \includegraphics[width=0.4\textwidth]{images/mean_value_theorem.png}
        \end{figure}
    \end{theo}

    \begin{defn}
        \textbf{Second Derivative Test}:\newline
        Let $f$ be twice differentiable on an open interval containing a stationary point $c$, and $f''$ is continuous 
        at $c$:\newline
        (a) If $f''(c)>0$, then $f(c)$ is a local minimum\newline
        (a) If $f''(c)<0$, then $f(c)$ is a local maximum\newline
        (c) If $f''(c)=0$, then the second derivative test is inconclusive\newline
    \end{defn}

    \begin{theo}
        \textbf{Cauchy's Mean Value Theorem}:\newline
        Let $f$ and $g$ be continuous on a closed interval $[a,b]$ and differentiable on $(a,b)$. Then there exists a point
        $c\in(a,b)$ such that,
        \[f'(c)(g(b)-g(a))=g'(c)(f(b)-f(a))\]
        in particular, if $g(b)\ne g(a)$ and $f(b)\ne f(a)$ then
        \[\frac{f'(c)}{g'(c)}=\frac{f(b)-f(a)}{g(b)-g(a)}\]
    \end{theo}

    \begin{theo}
        \textbf{Inverse Function Theorem}:\newline
        Let $f$ be continuously differentiable with a nonzero derivative at point $a$, then $f$ is invertable in the 
        neighborhood of $a$, the inverse is continuously differentiable, and the derivative of the inverse function at
        $b=f(a)$ is,
        \[(f^{-1})'(b)=\frac{1}{f'(f^{-1}(b))}=\frac{1}{f'(a)}\]
    \end{theo}

    Given an invertable function with continuous derivative, the inverse function theorem gives us a way to compute the
    derivative of the inverse function. Furthermore if $f$ is sufficiently differentiable then we can apply the theorem
    to compute higher order derivatives of inverse functions.

    \begin{exmp}
        \textit{Let $x=f(\xi)$ be invertable (so $\xi=f^{-1}(x)$) and have continuous second derivative, then using the inverse function theorem
        compute the second derivative of the inverse function}\newline\newline
        By repeated application of the chain rule we have,
        \begin{align*}
            (f^{-1})''(x)&=\frac{d}{dx}\left(\frac{1}{f'(f^{-1}(x))}\right)\\
            &=-\frac{1}{(f'(f^{-1}(x)))^2}\frac{d}{dx}(f'(f^{-1}(x)))\\
            &=-\frac{1}{(f'(f^{-1}(x)))^2}f''(f^{-1}(x))\frac{d}{dx}f^{-1}(x)\\
            &=-\frac{1}{(f'(f^{-1}(x)))^2}\frac{f''(f^{-1}(x))}{f'(f^{-1}(x))}\\
            &=-\frac{f''(f^{-1}(x))}{(f'(f^{-1}(x)))^3}\\
            &=-\frac{f''(\xi)}{(f'(\xi))^3}
        \end{align*}

    \end{exmp}

    \begin{defn}
        \textbf{L'Hopital's Rule}:\newline
        Let $f$ and $g$ be differentiable on an open interval $I$ containing $c$, and suppose that $f(c)=g(c)=0$. Then
        \[\lim_{x\rightarrow c}\frac{f(x)}{g(x)}=\lim_{x\rightarrow c}\frac{f'(x)}{g'(x)}\]
        provided that the latter limit exists.
    \end{defn}

    \subsection{WIP - Integration}
    
    \begin{theo}
        \textbf{Fundamental Theorem of Calculus}\newline
        \[\int^{b}_{a}\frac{d}{dx}f(x)dx=f(b)-f(a)\]
    \end{theo}

    \begin{theo}
        \textbf{Leibniz Integral Rule}\newline
        \[\frac{d}{dx}\int^{b(x)}_{a(x)}f(x,t)dt=f(x,b(x))\cdot\frac{d}{dx}b(x)-f(x,a(x))\cdot\frac{d}{dx}a(x)+\int^{b(x)}_{a(x)}\frac{\partial}{\partial x}f(x,t)dt\]\newline
        where $-\infty<a(x)$, $b(x)<\infty$
    \end{theo}
    
\end{document}