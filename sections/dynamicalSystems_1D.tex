\documentclass[../main.tex]{subfiles}

\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{Dynamical Systems: 1D}

    \subsection{Qualitative Analysis}
    
    Sometimes we cannot arrive at an analytic solution to a dynamical system, other time we can but still it may be difficult to gain any 
    understanding of its qualitative features – especially if it is in an implicit form. The key to qualitative analysis is 
    to identify and classify the equilibria of the system. 

    \begin{defn}
        \textbf{Equlibrium Point}:\newline
        Given $u'=f(u)$, the equilibrium solutions (or equilibria) are the solutions $u^{*}$ 
        such that $f(u^{*})=0$. Geometrically these are the points at which the function $f(u)$ intersects the u-axis.
    \end{defn}

    \subsubsection{Phase-Lines}
    
    A helpful way to examine the behaviour of equlibria is to construct a \textbf{phase-line}. Here we construct a line 
    indicating the flow of the solution $u(t)$ were we plot the equilibria points and inbetween these, arrows indicting
    the flow direction. If $u' >0$ at a given $u$ then the arrow points towards increasing $u$.
    If $u' <0$ at a given point $u$ then the arrow points in the decreasing $u$ direction. Else we are at an 
    equilibria. 
    \newline
    \newline
    It can be helpful to think of $u'$ as the velocity of $u$, hence we use the sign of $u'$ for direction.
    \newline
    \newline
    Below is an example of a phase-line with the graph $u'$ over $u$ plotter on top.

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.9\textwidth]{images/1d-phase-line-and-time-series.png}}
    \end{figure*}
    \FloatBarrier

    We can classify the equilibrium points analytically. Notice that the 'velocity function' $u'=f(y)$ so to study the 
    behaviour of $u'$ we can use $f(y)$. 
    
    \begin{defn}
        \textbf{1D Equlibrium Classification Test}:\newline
        Given an equilibrium point $y^{*}$ and some open interval $I=(a,b)$ containing $y^{*}$,    
        \begin{itemize}{}{}
            \item If $y<y^{*} \Rightarrow f(y)>0$ and $y>y^{*} \Rightarrow f(y)<0$ then $y^{*}$ is \textbf{stable}
            \item If $y<y^{*} \Rightarrow f(y)<0$ and $y>y^{*} \Rightarrow f(y)>0$ then $y^{*}$ is \textbf{unstable}
            \item Otherwise the test is \textbf{inconclusive}
        \end{itemize}
    \end{defn}

    \begin{defn}
        \textbf{1D Equlibrium Classification Test (using gradient)}:\newline
        Given an equilibrium point $y^{*}$,    
        \begin{itemize}{}{}
            \item If $f'(y^{*})<0$ then $y^{*}$ is an \textbf{stable} equilibria
            \item If $f'(y^{*})>0$ then $y^{*}$ is an \textbf{unstable} equilibria
            \item If $f'(y^{*})=0$ then the test is \textbf{inconclusive} (we need to consider higher order terms to figure of 
            the nature of the equilibria)
        \end{itemize}
    \end{defn}


    \subsubsection{Stability Analysis}

    Given some ODE we can classify and study the behaviour of equilibrium points by how perturbations around given 
    equilibria act. For classification this is usually only done if the classification by using $f'(y^{*})$ fails. I.e. 
    $f'(y^{*})=0$.

    We write the solution near an equilibrium point in terms of a perturbation function,
    
    \[y(t)=y_{eq}+Y(t)\]
    Then substituting this into the ODE gives,
    
    \[\frac{dy}{dt}=\frac{d}{dt}(y_{eq}+Y(t))=\frac{dY}{dt}\]
    The Taylor series of $f$ about $y_{eq}$ is,
    
    \[f(y_{eq}+Y)=f(y_{eq})+f'(y_{eq})Y+\frac{f''(y_{eq})}{2!}Y^{2}+...\]
    But we know that $f(y_{eq})=0$. And it is also possible that some $f^{(m)}(y_{eq})=0$. Therefore let $k$ be the 
    first non-zero derivative. The Taylor approximation is then,

    \[f(y_{eq}+Y)\approx\frac{f^{(k)}(y_{eq})}{k!}Y^{k}\]
    So,
    \[\frac{dY}{dt}\approx\frac{f^{(k)}(y_{eq})}{k!}Y^{k}\]
    At this point we can use this approximation along with the \textbf{classification test} above to determine the 
    stability of the point. We could also go on further to gain additional insights such as the rate of decay/growth if 
    we solve for $Y$ (say using separation of variables).

    \begin{exmp}
        \textit{Find the equilibrium points and analyise their stability for the following system,}\newline
        \begin{align*}
            &\frac{dy}{dt}=-(\ln(y))^{3}\\
            &y(0)=1.1
        \end{align*}
        \newline
        Let $f(y)=-(\ln(y))^{3}$. The equilibrium points are those $y$ that satisfy $f(y)=0$.
        \[-(\ln(y))^{3}=0\quad \Rightarrow\quad y=1\]
        The only equilibrium point is $y^{*}=1$.\newline
        Now we wish to analysis the stability of the equlibria. First note that,
        \[f'(y)=-3(\ln(y))^{2}/y\]
        And that $f'(y^{*})=0$ so we will need to linearise the system. Define a perturbation about the equilibrium 
        $y(t)=y^{*}+Y(t)$ then,
        \[\frac{dy}{dt}=\frac{d}{dt}(y^{*}+Y)=\frac{dY}{dt}=f(y^{*}+Y)=f(1+Y)\]
        where
        \[f(1+Y)=-(\ln(1+Y))^{3}\]
        Now we Taylor expand $\ln(1+Y)$ about 0 to get,
        \[f(1+Y)=-\left(Y-\frac{Y^{2}}{2}+...\right)^{3}\]
        Finally truncating at the first term,
        \[f(1+Y)\approx F(1+Y)=-Y^3\]
        At this point we can now look at the values of $F$ on either side of the equilibrium i.e. for $Y<0$ and $Y>0$ 
        and from this determine the stability of the equilibrium. In this case,
        \begin{itemize}
            \item for $Y<0$, $F>0$
            \item for $Y>0$, $F<0$
        \end{itemize}
        And so this suggests that the point is stable.
    \end{exmp}

    \subsubsection{Bifurcation Analysis}

    Identify where changes in the parameters of the system lead fundemental changes in its character. Such changes in 
    character can be, changes in the number of equilibria, changes in the stability of an equlibria etc.

    \begin{strt}
        \textbf{Finding Bifurcations}:\newline
        Fix all parameters of the model, expect the one of interest. Now look at how the number of equlibria change 
        with changes in the selected parameter.
    \end{strt}

    \begin{exmp}
        \textit{Find the bifurcation points of the parameters $h$, where $h>0$ in the following model,}
        \[\frac{du}{dt}=u(1-u)-h\]
        The equilibriums are found by solving,
        \[u(1-u)-h=0\]
        Hence the equlibria are given by,
        \[u^{*}=\frac{1\pm\sqrt{1-4h}}{2}\]
        From this we can see that we have:
        \begin{itemize}
            \item 2 equlibria when $0<h<\frac{1}{4}$
            \item 1 equilibrium when $h=\frac{1}{4}$
            \item 0 equilibriums when $h>\frac{1}{4}$ 
        \end{itemize}
        And the bifuration diagram for this is,
        \begin{center}
            \includegraphics[width=0.9\textwidth]{images/1d-bifurcation-diagram.png}
        \end{center}
    \end{exmp}


\end{document}