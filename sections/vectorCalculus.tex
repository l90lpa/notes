\documentclass[../main.tex]{subfiles}

\usepackage{multicol}

\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{Vector Calculus}
    \subsection{Gradient, Divergence, Curl}
    \begin{defn}
        \textbf{Gradient}:\newline
        Let $f(x,y,z)$ be a scalar-valued function then,
        \[
            \nabla f=
            \frac{\partial f}{\partial x}\textbf{i}+\frac{\partial f}{\partial y}\textbf{j}+\frac{\partial f}{\partial z}\textbf{k}
        \]
    \end{defn}

    \begin{defn}
        \textbf{Divergence}:\newline
        Let $\textbf{F}=F_{x}\textbf{i}+F_{y}\textbf{j}+F_{z}\textbf{k}$ be a vector valued function (here $F_{z}$ 
        denotes the $z$ component of $\textbf{F}$, not partial differentiation) then,
        \[
            \nabla \cdot \textbf{F}(x,y,z)=
            \frac{\partial F_{x}}{\partial x} +\frac{\partial F_{y}}{\partial y} +\frac{\partial F_{z}}{\partial z}
        \]
    \end{defn}

    \begin{defn}
        \textbf{Laplacian}:\newline
        Let $f(x,y,z)$ be a scalar-valued function then,
        \[
            \Delta f = \nabla \cdot (\nabla f)=
            \frac{\partial^{2} f}{\partial x^{2}}+\frac{\partial^{2} f}{\partial y^{2}}+\frac{\partial^{2} f}{\partial z^{2}}
        \]
    \end{defn}

    \begin{defn}
        \textbf{Curl (only valid in $\mathbb{R}^{3}$)}:\newline
        Let $\textbf{F}=F_{x}\textbf{i}+F_{y}\textbf{j}+F_{z}\textbf{k}$ be a vector valued function in 
        $\mathbb{R}^{3}$ (here $F_{z}$ denotes the $z$ component of $\textbf{F}$, not partial differentiation) then,
        \[
            \nabla \times \textbf{F}=
            \left|
            \begin{matrix}
                \textbf{i} & \textbf{j} & \textbf{k} \\
                \frac{\partial}{\partial x} & \frac{\partial}{\partial y} & \frac{\partial}{\partial z} \\
                F_{x} & F_{y} & F_{z}
            \end{matrix}
            \right|
        \]
    \end{defn}

    \subsubsection{Identities}
    
    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=1.0\textwidth]{images/vector-calculus-identities-1.png}}
    \end{figure*}
    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=1.0\textwidth]{images/vector-calculus-identities-2.png}}
    \end{figure*}
    \FloatBarrier

    \subsection{Jacobian}

    \begin{defn}
        \textbf{Jacobian}:\newline
        Let $x\in\mathbb{R}^n$, and $f:\mathbb{R}^n\rightarrow \mathbb{R}^m$ be a some vector-valued function such that $f(x)=(f_1(x),...,f_m(x))^T$, then the Jacobian of $f$ at $x$ is the linear map,
        \[
            J=\left(\begin{matrix}
                \frac{\partial f}{\partial x_1}(x) & ... & \frac{\partial f}{\partial x_n}(x)
            \end{matrix}\right)
            =\left(\begin{matrix}
                (\nabla f_1)^T(x)\\
                \vdots \\
                (\nabla f_m)^T(x)
            \end{matrix}\right)
            =\left(\begin{matrix}
                \frac{\partial f_1}{\partial x_1}(x) & ... & \frac{\partial f_1}{\partial x_n}(x)\\
                \vdots & \ddots & \vdots \\
                \frac{\partial f_m}{\partial x_1}(x) & ... & \frac{\partial f_m}{\partial x_n}(x)
            \end{matrix}\right)
        \]
        The following are equivalent alternative convensions for denoting the Jacobian of $f$ at $x$,
        \[J_f(x)\quad\textrm{or}\quad J_f|_x\quad\textrm{or}\quad \frac{\partial}{\partial x}f(x)\]
    \end{defn}
    \textbf{Remark}: $J_f(a)$ is the Jacobian of $f$ at $x=a$. The Jacobian is a linear map, and so can be represented as a matrix, thus if $f:\mathbb{R}^n\rightarrow \mathbb{R}^m$ then $J_f(a)\in\mathbb{R}^{m \times n}$. Let $y\in\mathbb{R}^n$, then the application (matrix-vector product) of the Jacobian on $y$ is $J_f(a)y$ (this gives an indication why it is usually more readable to simply denote the Jacobian by $J$ which leads to $Jy$).
    
    \subsubsection{Useful Rules for Computing the Jacobian}

    The following useful rules for computing the Jacobian, are essentially results from multivariate and matrix calculus.
    \begin{itemize}
        \item \textbf{Matrix-Vector Product Rule}: let $x\in\mathbb{R}^n$ and $L:\mathbb{R}^n\rightarrow \mathbb{R}^m$ be a linear operator, then $L$ can be represented by some matrix $A$ such that $L(x)=Ax$. The Jacobian of $L$ is then simply $A$,
        \[\frac{\partial}{\partial x}L(x)=\frac{\partial}{\partial x}Ax=A\]
        \item \textbf{Diagonal Rule}: let $x\in\mathbb{R}^n$ and $f:\mathbb{R}^n\rightarrow \mathbb{R}^n$ be a component-wise function, $f(x)=(f(x_1),...,f(x_n))^T$ e.g. $sin(x)=(sin(x_1),...,sin(x_n))^T$, then the Jacobian of $f$ is, 
        \[\frac{\partial}{\partial x}f(x)=\textrm{diag}(f'(x))=\left(\begin{matrix}
            f'(x_1) & 0 & ... & 0 \\
            0 & f'(x_2) && \vdots \\
            \vdots &&\ddots& 0 \\
            0 & ... & 0 & f'(x_n) 
        \end{matrix}\right)\]
        \item \textbf{Chain Rules}: let $x\in\mathbb{R}^n$, and $f:\mathbb{R}^n\rightarrow \mathbb{R}^m$ be a some vector-valued function.
        \begin{itemize}
            \item if $A\in\mathbb{R}^{k\times m}$ is some matrix, then the Jacobian of $Af(x)$ is,
            \[\frac{\partial}{\partial x}Af(x)=A\frac{\partial}{\partial x}\]
            suppose $f$ is in fact a component-wise function, then using the diagonal rule, this simplifies to,
            \[\frac{\partial}{\partial x}Af(x)=A\cdot\textrm{diag}(f'(x))\]
            \item if $B\in\mathbb{R}^{n\times n}$ is some matrix, then the Jacobian of $f(Bx)$ is,
            \[\frac{\partial}{\partial x}f(Bx)=\left(\frac{\partial f}{\partial x}(Ax)\right)A\]
            suppose $f$ is in fact a component-wise function, then using the diagonal rule, this simplifies to,
            \[\frac{\partial}{\partial x}f(Bx)=\textrm{diag}(f'(Ax))A\]
        \end{itemize}
        \item \textbf{Product Rule}: let $x\in\mathbb{R}^n$, and $f:\mathbb{R}^n\rightarrow \mathbb{R}^m$ and $g:\mathbb{R}^n\rightarrow \mathbb{R}^m$ be a some vector-valued functions, then the Jacobian of their component-wise product, $f(x)\;.*\;g(x)$ (where $f(x)\;.*\;g(x)=(f_1(x)g_1(x),...,f_m(x)g_m(x))^T$), is,
        \[\frac{\partial}{\partial x}(f(x)\;.*\;g(x))=\textrm{diag}(g(x))\frac{\partial f}{\partial x}+\textrm{diag}(f(x))\frac{\partial g}{\partial x}\]
    \end{itemize}

    \subsection{Integration}

    \subsubsection{Line Integral of a Scalar Field}

    Let $f(x_{1},x_{2},...,x_{n})$ be a scalar field and $C$ a curve that $f$ is defined on. If $C$ has the parameterisation $\vec{r}(t)$ for
    $t\in[a,b]$, then the integral of $f$ over the curve is,
    \[\int_{C}f\;ds=\int_{a}^{b}f(\vec{r}(t))|\vec{r}'(t)|\;dt\]
    where $f(\vec{r}(t))=f(r_{x_{1}}(t),...,r_{x_{n}}(t))$ and $r_{x_{j}}$ are the components of $\vec{r}$

    \subsubsection{Line Integral of a Vector Field}

    Let $\vec{f}$ be a vector field and $C$ curve that $\vec{f}$ is defined on. If $C$ has the parameterisation $\vec{r}(t)$ for
    $t\in[a,b]$, then the integral of $\vec{f}$ over the curve is,
    \[\int_{C}\vec{f}\cdot d\vec{r}=\int_{a}^{b}\vec{f}(\vec{r}(t))\cdot\vec{r}'(t)\;dt\]
    where $\vec{f}(\vec{r}(t))=\vec{f}(r_{x_{1}}(t),...,r_{x_{n}}(t))$ and $r_{x_{j}}$ are the components of $\vec{r}$

    \subsubsection{WIP - Surface Integrals}

    \subsubsection{WIP - Volume Integrals}

    \subsection{Einstein Notation (Suffix Notation)}

    Einstein summation notation is a convenient notation when working with vectors, matrices, or tensors. It is simply 
    a replacment of the usual summation notation by indices, with the context that repeated indices within a given term 
    implie summation over those indices of that term.\newline
    \newline
    The rules are,
    \begin{itemize}
        \item Repeated indices within a term represents a summation of that term over the repeated indices i.e. $A_{ij}x_{j}=\sum_{j}A_{ij}x_{j}$
        \item Each index can appear at most twice within any given term
        \item Each term must contain identical non-repeated indices (non-repeated indices are call free indices)
    \end{itemize}
    Here are some examples:\newline
    \begin{tabular}{|c|c|c|c|}
        \hline 
        Expression & Valid Notation & Invalid Notation & Remark \\
        \hline
        $Ax=y$ &  $A_{ij}x_{j}=y_{i}$ & $A_{ij}x_{j}=y_{k}$ & Miss match of free-index $i$ and $k$\\
        \hline
        $(Ax)\cdot y$ & $A_{ij}x_{j}y_{i}$ & $A_{ij}x_{j}y_{j}$ & $j$ index repeated more than twice \\
        \hline
    \end{tabular}

    \subsubsection{Useful Tensors}

    \textbf{Kronecker-$\delta$}:\newline
    This is a rank-2 tensor defined by,
    \[
        \delta_{ij}=
        \left\{
            \begin{matrix}
                1,&i=j\\
                0,&i\ne j
            \end{matrix}
        \right.
    \]

    In an expression the Kronecker-$\delta$ results in replacing one index with another an example is being,
    \begin{align*}
        \delta_{il}\delta_{jk}\varepsilon_{kl}&=\delta_{il}\varepsilon_{jl}\quad\textrm{for the sum over $k$, we only collect a term when $k=j$}\\
        &=\varepsilon_{ji}\quad\textrm{for the sum over $l$, we only collect a term when $l=i$}
    \end{align*}
    Furthermore the Kronecker-$\delta$ can be thought of as the identity matrix.\newline
    \newline
    \textbf{Levi-Civita symbol ($\epsilon$-tensor)}:\newline
    This is a rank-3 tensor defined by,
    \[\epsilon_{ijk}=\left\{\begin{tabular}{r l}
        1,&if the permutation $ijk$ is even\\
        -1,&if the permutation $ijk$ is odd\\
        0,&otherwise
    \end{tabular}\right.\]

    It is worth noting that the $\epsilon$-tensor and the Kronecker-$\delta$ are connected by the identity,
    \[\epsilon_{ijk}\epsilon_{klm}=\delta_{il}\delta_{jm}-\delta_{im}\delta_{jl}\]

    \subsubsection{Vector Operators}
    The common vector operators in Einstein notation are,
    \begin{align*}
        \vec{u}\cdot\vec{v}&=u_{i}v_{i}\\
        [\vec{u}\times\vec{v}]_{i}&=\epsilon_{ijk}u_{j}v_{k}\\
        \\
        [\textrm{grad} (f)]_{i}         &=[\nabla f]_{i}=\nabla_{i} f\\
        \textrm{div} (\vec{F}) &=\nabla\cdot\vec{F}=\nabla_{i}\vec{F}_{i}\\
        [\textrm{curl} (\vec{F})]_{i}&=[\nabla\times\vec{F}]_{i}=\epsilon_{ijk}\nabla_{j}\vec{F}_{k}\\
        \textrm{laplacian} (f)         &=\Delta f=\nabla_{i}(\nabla_{i}f)\\
    \end{align*}

    \subsection{Vector Field Properties}
    \begin{defn}
        \textbf{Conservative Vector Field}:\newline
        A vector field is conservative if all line integrals are path independent.\newline
        \newline
        In 2D and 3D, if 
        the domain is simply connected then we can test for conservativeness by checking if the curl is zero everywhere.
        \[\textrm{3D: }\nabla\times\vec{F}=0\]
        \[\textrm{2D: }[\nabla\times\vec{F}]_{k}=0\quad\Rightarrow\quad \partial xF_{x}-\partial yF_{y}=0\]
    \end{defn}

    \begin{defn}
        \textbf{Hamiltonian Vector Field}:\newline
        A vector field is Hamiltonian if the divergence of the vector field is zero everywhere i.e. $\nabla\cdot\vec{F}=0$
    \end{defn}

    \subsection{Integral Theorems}
    
    \begin{defn}
        \textbf{Divergence Theorem}:\newline
        \[\int_{V}\nabla\cdot\vec{F}\;dV=\int_{S}\vec{F}\cdot\vec{n}\;dS\]
        where, $\vec{F}$ is a vector field, $V$ a compact volume, $S$ the piecewise smooth closed surface of the volume,
        and $\vec{n}$ the outward pointing normal of the surface at each point.
    \end{defn}

    \begin{defn}
        \textbf{Green's Identities}:\newline
        Let $u$ and $v$ be scalar fields, then Green's three identities are:\newline
        \begin{tabular}{l l}
            First Identity& $ \int_{V}\nabla v \cdot \nabla u + v \Delta u\; dV = \int_{S} v(\nabla u \cdot \vec{n})\;dS $\\
            Second Identity& $ \int_{V} v\Delta u - u\Delta v\; dV = \int_{S} v(\nabla u \cdot \vec{n})-u(\nabla v \cdot \vec{n})\;dS $\\
            Third Identity& $ \int_{V} \Delta u \;dV = \int_{S} \nabla u \cdot \vec{n}\;dS $\\
        \end{tabular}\newline
        \textit{Note: these come from applying the divergence theorem to the vector fields $\{v\nabla u, v\nabla u -u\nabla v, \nabla u\}$ respectively}
    \end{defn}

    \begin{defn}
        \textbf{Stoke's Theorem}:\newline
        \[\int_{S}(\nabla\times\vec{F})\cdot\vec{n}\;dS=\int_{C}\vec{F}\cdot\;d\vec{r}\;\]
        where, $\vec{F}$ is a vector field, $S$ a surface, $C$ the piecewise smooth closed curve of the surfaces 
        boundary/opening, and $\vec{n}$ the outward pointing normal of the surface at each point.
    \end{defn}

    
    \subsection{WIP - Curvilinear Coordinates}

    \subsubsection{WIP - Orthogonal Curvilinear Coordinates}

    \subsubsection{WIP - Operator is Orthogonal Curvilinear Coordinates}

    \subsubsection{WIP - Cylinderical Coordinates}
    
    \begin{align*}
        &x=r\cos(\phi),\quad y=r\sin(\phi),\quad z=z\\
        &r=\sqrt{x^{2}+y^{2}},\quad \cos(x/r),\quad \sin(y/r)\\
        &r\ge 0,\quad -\pi<\phi\le\pi,\quad z\in\mathbb{R}
    \end{align*}

    \begin{align*}
        &\vec{e}_{r}=\vec{i}\cos(\phi)+\vec{j}\sin(\phi)=\frac{x\vec{i}+y\vec{j}}{\sqrt{x^{2}+y^{2}}}\\
        &\vec{e}_{\phi}=-\vec{i}\sin(\phi)+\vec{j}\cos(\phi)=\frac{-y\vec{i}+x\vec{j}}{\sqrt{x^{2}+y^{2}}}\\
        &\vec{e}_{z}=\vec{k}
    \end{align*}
    
    \subsubsection{WIP - Spherical Coordinates}

    \begin{align*}
        &x=r\cos(\phi)\sin(\theta),\quad y=r\sin(\phi)\sin(\theta),\quad z=r\cos(\theta)\\
        &r=\sqrt{x^{2}+y^{2}+z^{2}}\\
        &r\ge 0,\quad -\pi<\phi\le\pi,\quad 0\le\theta\le\pi
    \end{align*}
    \newline
    Note that $\phi=0$ is on the x-axis with positive values counter-clockwise, and $\theta=0$ is on the z-axis.
    
    \begin{align*}
        &\vec{e}_{r}=\vec{i}\cos(\phi)\sin(\theta)+\vec{j}\sin(\phi)\sin(\theta)+\vec{k}\cos(\theta)\\
        &\vec{e}_{\phi}=-\vec{i}\sin(\phi)+\vec{j}\cos(\phi)\\
        &\vec{e}_{\theta}=\vec{i}\cos(\phi)\cos(\theta)+\vec{j}\sin(\phi)\cos(\theta)-\vec{k}\sin(\theta)\\
    \end{align*}

    \subsection{WIP - Cartesian Tensors}

    \begin{defn}
        \textbf{Double Inner Product}:\newline
        ...
    \end{defn}

    \begin{defn}
        \textbf{Divergence Theorem}:\newline
        The divergence theorem of vector calculus extends to arbitrary order tensors. Let $T_{ij...k}(\vec{x},t)$ be an
        arbitrary tensor field differentiable on the domain $D$ with boundary $B$ then,
        \[\int_{D}\nabla_{k}T_{ij...k}\;dV=\int_{B}T_{ij...k}n_{k}\;dS\]
    \end{defn}

\end{document}