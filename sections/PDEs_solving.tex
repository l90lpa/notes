\documentclass[../main.tex]{subfiles}

\graphicspath{{\subfix{../images/}}}

\begin{document}
    \section{PDEs: Solving}
    \subsection{Method of Characteristics (Quasilinear)}

    The method of characteristics is suitable for many (but not all) first order PDEs of the form

    \[a(x,y,u)u_{x}+b(x,y,u)u_{y}=c(x,y,u)\]
    If we let $z=u(x,y)$ then $(x,y,u(x,y))$ is a surface in $\mathbb{R}^{3}$. The geomtric idea behind the method of 
    characteristics is to find a family of disjoint curves that when unioned together form the solution surface.\newline
    \newline
    First we note that we can write the PDE as, $(a(x,y,u),b(x,y,u),c(x,y,u))\cdot(u_{x}(x,y),u_{y}(x,y),-1)=0$. 
    From calculus we know that $(u_{x},u_{y},-1)$ is the surface normal at each point. And following from this 
    $(a,b,c)$ must lie in the tangent plane at each point.\newline
    \newline
    Now we will look to find one curve $(x(\tau),y(\tau),u(\tau))$ (parameterised by $\tau$). Suppose that we are given
    a point $(x_{0},y_{0},u_{0})$ that lies in the solution surface, then one constrain that we can impose on the curve
    is that $(x(0),y(0),u(0))=(x_{0},y_{0},u_{0})$. Next, at $\tau=0$ the tangent to the curve is, $(x'(0),y'(0),u'(0))$
    and this (it can be shown) must lie in the tangent plane of the solution. To ensure that it does lie in the tangent 
    plane we could choose it to be a scalar multiple of $(a(x(0),y(0),u(0)),b(x(0),y(0),u(0)),c(x(0),y(0),u(0)))$ giving,
    \newline
    \[(x'(0),y'(0),u'(0))=(a(x(0),y(0),u(0)),b(x(0),y(0),u(0)),c(x(0),y(0),u(0)))\]
    If we make the same argument about the tangent of the curve for all $\tau$ then we get the following system of ODEs 
    that can be used to find the curve,

    \begin{align*}
        &x'(\tau)=a(x(\tau),y(\tau),u(\tau))\\
        &y'(\tau)=b(x(\tau),y(\tau),u(\tau))\\
        &u'(\tau)=c(x(\tau),y(\tau),u(\tau))\\
        & \\
        &s.t.\quad x(0)=x_{0},\quad y(0)=y_{0},\quad u(0)=u_{0}
    \end{align*}
    This systems results in a single curve the choice of which is parameterised by the $(x_{0},y_{0},u_{0})$.\newline
    \newline
    Finally then, if instead of just being given a single point on the solution surface, we are given an curve 
    (the initial condition) and we parameterise this curve to as $(x_{0}(s),y_{0}(s),u_{0}(s))$ then we will have the 
    same system but with conditions,

    \begin{align*}
        &x'(\tau)=a(x(\tau),y(\tau),u(\tau))\\
        &y'(\tau)=b(x(\tau),y(\tau),u(\tau))\\
        &u'(\tau)=c(x(\tau),y(\tau),u(\tau))\\
        & \\
        &s.t.\quad x(0)=x_{0}(s),\quad y(0)=y_{0}(s),\quad u(0)=u_{0}(s)
    \end{align*}
    Solving this will give a parametric equation for the surface in terms of $(s,\tau)$ that can be rewritten in terms 
    of $(x,y)$.
    
    \begin{strt}
        \textbf{Method of Characteristics (Quasilinear)}:\newline
        Given the PDE,
        \[ a(x,y,u)\frac{\partial u}{\partial x}+b(x,y,u)\frac{\partial u}{\partial y}=c(x,y,u) \]
        The graph of the solution is $z=u(x,y)$. To solve this the general approach is:
        \begin{itemize}
            \item Parameterise the initial curve $\Gamma$,
            \[
                \Gamma=\left\{
                \begin{matrix}
                    x=x_{0}(s)\\
                    y=y_{0}(s)\\
                    z=z_{0}(s)\\
                \end{matrix}
                \right.
            \]
            \item For each $s$, find the streamline of $F$ that passes through $\Gamma(s)$. That is solve,
            \[ \frac{dx}{d\tau}=a(x,y,z),\quad \frac{dy}{d\tau}=b(x,y,z),\quad \frac{dz}{d\tau}=c(x,y,z) \]
            subject to ICs $x(0)=x_{0}(s),\quad y(0)=y_{0}(s),\quad z(0)=z_{0}(s)$
            giving the solutions,
            \[ x=X(s,\tau),\quad y=Y(s,\tau),\quad z=Z(s,\tau) \]
            \item Find $s$ and $\tau$ in terms of $x$ and $y$, using the parameterised solutions $x=X(s,\tau)$ and $y=Y(s,\tau)$
            \[ s=S(x,y),\quad \tau=T(x,y) \]
            \item Substitute for $s$ and $\tau$ in $Z(s,\tau)$ to give $u(x,y)$
            \[ u(x,y)=Z(S(x,y),T(x,y)) \]
        \end{itemize}
        \textit{Note: sometimes the system of ODEs can be coupled (e.g. $\{x'=y,\;y'=x\}$), in these cases it can be 
        useful to take second derivatives of the ODEs which will result in first derivatives of the variables which we know from the original system (e.g. $\{x''=y',\;y''=x'\}\Rightarrow \{x''=x,\;y''=y\}$) }

    \end{strt}

    \begin{exmp}
        \textit{Given the following PDE find a solution using the method of characteristics}\newline
        \[ xu_{x}-2yu_{y}=u^2,\quad \textrm{ subject to }\quad u(x,x)=x^{3} \]
        Parameterising the initial curve (i.e the IC) as, $x=s$, $y=x=s$, $z=s^{3}$.\newline
        Now we find the characteristics curves. First,
        \[ \frac{dx}{d\tau}=x \quad\Rightarrow\quad x=ce^{\tau}\]
        and by the IC $x(0)=x_{0}(s)=s$ we get,
        \[x=se^{\tau}\]
        Next,
        \[ \frac{dy}{d\tau}=-2y \quad\Rightarrow\quad y=ce^{-2\tau}\]
        and by the IC $y(0)=y_{0}(s)=s$ we get,
        \[y=se^{-2\tau}\]
        Finally,
        \begin{align*}
            \frac{dz}{d\tau}=-z^{2} \quad&\Rightarrow\quad \frac{1}{z^{2}}dz=d\tau\\
            &\Rightarrow\quad z=-\frac{1}{\tau+c}
        \end{align*}
        and by the IC $z(0)=z_{0}(s)=s^{3}$ we get,
        \[z(0)=-\frac{1}{c}=s^{3} \quad\Rightarrow\quad c=-\frac{1}{s^{3}}\]
        hence $z$ is,
        \[z=-\frac{1}{\tau-1/s^{3}}=\frac{s^{3}}{1-\tau s^{3}}\]
        Now we find $s$ and $\tau$ in terms of $x$ and $y$. First we find $\tau$, from the equations for $x$ and $y$ we have,
        \[s=xe^{-\tau},\quad s=ye^{2\tau}\]
        hence,
        \begin{align*}
            xe^{-\tau}=ye^{2\tau} \quad&\Rightarrow\quad \frac{x}{y}=e^{3\tau}\\
            &\Rightarrow\quad \ln\left(\frac{x}{y}\right)=3\tau\\
            &\Rightarrow\quad \tau=\frac{1}{3}\ln\left(\frac{x}{y}\right)
        \end{align*}
        Next we find $s$, from the equation for $x$ we have $e^{\tau}=\frac{x}{s}$, and substituting into $y$ we get,
        \[ y=s\left(\frac{x}{s}\right)^{-2} \quad\Rightarrow\quad s=x^{2/3}y^{1/3} \]
        Now we can substitute for $s$ and $\tau$ in $z$ to get,
        \begin{align*}
            u(x,y)&=z\\
            &=\frac{(x^{2/3}y^{1/3})^{3}}{1-(x^{2/3}y^{1/3})^{3}\frac{1}{3}\ln\left(\frac{x}{y}\right)}\\
            &=\frac{x^{2}y}{1-\frac{1}{3}x^{2}y\ln\left(\frac{x}{y}\right)}
        \end{align*}

    \end{exmp}

    \subsubsection{Advection Equation}

    Suppose that the PDE involves time, $t$, the initial condition is given at $t=0$, and the coefficient of $u_{t}$ is 1. 
    In this case when we parameterise the initial curve the parametertic form of $t=t_{0}(s)$ is simply $t=0$. Furthermore 
    the characteristic for $t$ is found from,
    \[\frac{dt}{d\tau}=1 \quad \textrm{ subject to }\quad t(0)=t_{0}(s)=0\]
    And from this we get $t=\tau$. Thus we could simplify the remainder of the process above by using $t$ in place of $\tau$. 
    In this case the PDE has the following form which is called the \textbf{advection equation},

    \[\frac{\partial u}{\partial t}+a(u)\frac{\partial u}{\partial x}=c(x,y,u),\quad\textrm{s.t.}\quad u(x,0)=f(x)\]
    
    \begin{exmp}
        \textit{Given the following PDE find a solution using the method of characteristics}\newline
        \[ u_{t}+xu_{x}=-tu,\quad \textrm{ subject to }\quad u(x,0)=\cos(x) \]
        Since PDE involves time, $t$, the initial condition is given at $t=0$, and the coefficient of $u_{t}$ is 1, 
        we will use $t$ inplace of $s$.\newline
        Now we find the characteristics for $x$ and $z$. First for $x$,
        \[ \frac{dx}{dt}=x \quad\Rightarrow\quad x=ce^{t}\]
        and by the IC $x(0)=x_{0}(s)=s$ we get,
        \[x=se^{t}\]
        Next for $z$,
        \begin{align*}
            \frac{dz}{dt}=-tz \quad&\Rightarrow\quad \frac{1}{z}dz=-t\:dt\\
            &\Rightarrow\quad \ln(z)=-\frac{1}{2}t^{2}+c\\
            &\Rightarrow\quad z=ce^{-t^{2}/2}
        \end{align*}
        and by the IC $z(0)=z_{0}(s)=\cos(s)$ we get,
        \[z=c=\cos(s)\]
        hence,
        \[z=\cos(s)e^{-t^{2}/2}\]
        Now we want to write $s$ in terms of $x$ and $t$, from $x$ we get,
        \[s=xe^{-t}\]
        Finally substituting this into $z$ we get the solution,
        \[u(x,t)=\cos(xe^{-t})e^{-t^{2}/2}\]
    \end{exmp}

    If $a(x,t,u)$ actually depends on $u$ then the problem is non-linear. In this case it is likely that the solution 
    will only be valid for some finite time. The time at which the solution is no longer valid is called the 
    \textbf{breaking time}.

    If we assume that $a(u)>0$, $f(x)\ge 0$, and $f'(x)<0$ then the breaking time is found by solving the 
    minimization problem,
    \[t_{b}=\min_{s}\frac{-1}{s'(f(s))f'(s)}\]

    \subsection{WIP - Similarity Method}

    \subsection{WIP - Separation of Variables}

    Separation of variables is a technique for solving PDEs which entails attempting to rewrite the PDE a system of independent ODEs, then forming the solution of the PDE out of the solutions of the ODEs. 
    \newline
    \newline
    Suppose $u(x,y,...)$ is the solution of some PDE with dependent variables $x,y,...$. Now, assuming a solution of the form $u(x,y,...)\equiv X(x)Y(y)...$ (that is we assume that the variables are multiplicatively separated), we plug this assumed solution into the PDE and try to rewrite the equation so as to find a superpostion of independent ODEs. Then we solve for the solutions of these independent ODEs, and thus construct the solution of the PDE. Note, this is not the only form the assumed solution can take, in some cases it can be better to assume the variables are additively separated leading to a solution of the form $u(x,y,...)\equiv X(x)+Y(y)...$.

    \begin{exmp}
        \textit{Solve the following heat equation IBVP}\newline
        \begin{align*}
            &u_{t}=ku_{xx},\quad 0\le x\le L,\quad t>0\\
            &u(0,t)=0,\quad u(L,t)=0,\quad t\ge 0\\
            &u(x,0)=f(x),\quad 0\le x \le L
        \end{align*}
        Assume a solution of the form,
        \[u(x,t)=X(x)T(t)\]
        Substituting this into the PDE and rearraging we get,
        \[XT'=kX''T\quad\Leftrightarrow\quad\frac{T'}{kT}=\frac{X''}{X}\]
        Now we introduce the separation constant (which we choose to have a negative sign for convenience),
        \[\frac{T'}{kT}=-\lambda=\frac{X''}{X}\quad\Leftrightarrow\quad \begin{matrix}
            X''+\lambda X=0\\
            T'+\lambda k T=0
        \end{matrix}\]
        Finally, we substitute the assumed solution into the BCs. From, $u(0,t)=0$ we get $X(0)T(t)=0$ but since we are looking for a nontrivial $T(t)$ then $X(0)=0$. In a similar manor, $u(0,t)=0\;\Rightarrow\; X(L)=0$. The BC, $u(x,0)=f(x)$ tells us nothing we didn't already know which is that $T(0)=\textrm{constant}$.
        \newline
        \newline
        Solving the ODE problem, $X''+\lambda X=0,\;X(0)=0,\;X(L)=0$.

        If $\lambda\le 0$ then it is straight forward to show that only the trivial solution satisfies the system. While if $\lambda > 0$ then the characteristic equation has purely complex roots and so the general solution is,
        \[X(x)=A\cos(\sqrt{\lambda}x)+B\sin(\sqrt{\lambda}x)\]
        Applying the BCs we first find that $X(0)=0\;\Rightarrow\; A=0$. Following from this, $X(L)=0\;\Rightarrow\; B\sin(\sqrt{\lambda}L)=0$, and for a nontrivial solution we need,
        \[\sqrt{\lambda}L=\pi n\quad\Rightarrow\quad \lambda_n=\frac{\pi^2 n^2}{L^2},\quad\textrm{where}\;\; n\in\{1,2,...\}\]
        Choosing $B=1$ the solutions are then,
        \[X_n(x)=\sin\left(\frac{\pi n}{L}x\right)\]
        \newline
        \newline
        The ODE problem in $T$ now becomes, $T_n'+\lambda_n k T_n=0$, and solving this - by the integrating factor method - we get,
        \[T_n(t)=e^{-\frac{\pi^2 n^2}{L^2}kt}\]
        \newline
        \newline
        The solutions to the PDE are therefore,
        \[u_n(x,t)=X_n(x)T_n(t)=\sin\left(\frac{\pi n}{L}x\right)e^{-\frac{\pi^2 n^2}{L^2}kt}\]
        \newline
        \newline
        Finally, because the PDE is linear then there is a superposition of specific solutions that is also a solution. Let the superposition of solutions be,
        \[u(x,t)=\sum^\infty_{n=1}b_n\sin\left(\frac{\pi n}{L}x\right)e^{-\frac{\pi^2 n^2}{L^2}kt}\]
        To find $b_n$, we apply the BC $u(x,0)=f(x)$,
        \[\sum^\infty_{n=1}b_n\sin\left(\frac{\pi n}{L}x\right)=f(x)\]
        From this we see that $f$ must have a sine series representation, and that $b_n$ are the fourier coefficients of this sine series, i.e. $b_n=\int_0^L\sin\left(\frac{\pi n}{L}x\right)f(x)\;dx$
    \end{exmp}

    \begin{exmp}
        \textit{Find the general solution of the Hamilton-Jacobi equation (where $H$ is some nonlinear function),}
        \[u_t+H(u_x)=0,\quad \mathbb{R}^n\times(0,\infty)\]
        Assume a solution of the form,
        \[u(x,t)=X(x) + T(t)\]
        Substituting this into the PDE and rearraging we get (where we have introduced the separation constant, $\lambda$),
        \[T'+H(X')=0\quad\Leftrightarrow\quad-T'=\lambda=H(X')\]
        First we find that $-T'=\lambda\;\Rightarrow\;T(t)=-\lambda t+b_1$, where $b_1$ is some unknown constant.
        \newline
        \newline
        Next, for $H(X'(x))=\lambda$ to be true for any $x$ we must have that $X'(x)=a$ where $a$ is some constant. Therefore $X(x)=ax+b_2$, where $b_2$ is some unknown constant.
        \newline
        \newline
        Combining these results we get that the general solution is,
        \begin{align*}
            u(x,t)&=ax + \lambda t+b,\quad\textrm{where }b=b_1+b_2\\
            &=ax + H(a)t+b
        \end{align*}
    \end{exmp}

    \subsection{WIP - Duhamel's Principle}

    Duhamel's Principle has wide application among linear ODEs and PDEs. It can be used to find the solution of an inhomogeneous problem, from the solution of the associated homogeneous, initial value problem. 

    \subsection{Integral Transform Methods}
    
    \subsubsection{WIP - Fourier Transform Method}

    \subsubsection{WIP - Laplace Transform Method}


\end{document}