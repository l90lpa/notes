\documentclass[../main.tex]{subfiles}

\graphicspath{{\subfix{../images/}}}

\begin{document}
    \section{ODEs: Perturbation Methods (Global Analysis)}
    
    Perturbation methods are techniques for finding approximate solutions to problems when the model equations have 
    terms that are small. By a perturbation solution we mean an approximate solution that is the first few terms of a 
    Taylor-like expansion (power series) in the small parameter $\epsilon$. Perturbation methods apply to differential equations 
    (ordinary and partial), algebraic equations, integral equations, and all of the other types of equations
    encountered in applied mathematics. Although here we focus on ODEs.
    
    \subsection{Regular Perturbation Problems}

    A problem is considered to be a \textbf{regular perturbation} problem if, the power series in $\epsilon$ converges 
    with a nonzero radius of convergence. In regular perturbation problems, the asymptotic solution smoothly approaches 
    the exact solution over the whole domain.
    
    \begin{strt}
        \textbf{Regular Perturbation Method}:\newline
        Let $F\left(x,y,y',...,y^{n}\right)=0$ be an nth-order ODE and $\epsilon$ be the small parameter of the ODE.
        \begin{itemize}
            \item Assume that the solution to the problem has the form,
            \[y=y_{0}+y_{1}\epsilon+y_{2}\epsilon^{2}+y_{3}\epsilon^{3}+...\]
            \item Substitute the power series into the problem, 
            \[F\left(x,(y_{0}+y_{1}\epsilon+...),(y'_{0}+y'_{1}\epsilon+...),...,(y^{(n)}_{0}+y^{(n)}_{1}\epsilon+...)\right)=0\]
            \item Collect like terms of $\epsilon^{j}$ for all $j=0,1,2,...$, resulting in something of the form,
            \[A_{0}(x,y_{0},...,y^{(n)}_{0},y_{1},...,y^{(n)}_{1},...)+A_{1}(x,y_{0},...,y^{(n)}_{0},y_{1},...,y^{(n)}_{1},...)\epsilon+...=0\] 
            where $A_{j}(x,y_{0},...,y^{(n)}_{0},y_{1},...,y^{(n)}_{1},...)$ are simplying functions of their parameters, although not all parameters a 
            necessarily used i.e. we might actually have $A_{0}(x,y_{0},...,y^{(n)}_{0},y_{1},...,y^{(n)}_{1},...)=A_{0}(x,y_{0},...,y^{(n)}_{0})$. Furthermore, note if
            the problem was inhomogeneous then this should also be collected into the like terms.
            \item Equate the coefficient of each $\epsilon^{j}$ to 0 and solve the resulting system of equations to determine power series coefficients $y_{j}$,
            \[\left\{
            \begin{matrix}
                A_{0}(x,y_{0},...,y^{(n)}_{0},y_{1},...,y^{(n)}_{1},...)=0\\
                A_{1}(x,y_{0},...,y^{(n)}_{0},y_{1},...,y^{(n)}_{1},...)=0\\
                A_{2}(x,y_{0},...,y^{(n)}_{0},y_{1},...,y^{(n)}_{1},...)=0\\
                ...
            \end{matrix}    
            \right.
            \]
            Note, it is quitely that $A_{j}$ will depend on $A_{j-1}$ but not the other way, that is they are likely not coupled.
            \item A necessary condition for the success of this method is that the leading order problem
            ($A_{0}=0$) has a non-trivial solution.
        \end{itemize}
    \end{strt}

    \begin{exmp}
        \textit{Given the following equation, where the parameter $\epsilon$ is considered to be small, find a regular 
        perturbation solution}
        \[y^{2}+2\epsilon y-3=0\]
        Assume that a solution of the form,
        \[y=y_{0}+y_{1}\epsilon+y_{2}\epsilon^{2}+...\]
        exists. Substituting this into the problem we get,
        \begin{align*}
            &(y_{0}+y_{1}\epsilon+y_{2}\epsilon^{2}+...)^{2}+2\epsilon (y_{0}+y_{1}\epsilon+y_{2}\epsilon^{2}+...)-3=0\\
            \Rightarrow&(y_{0}^{2}+2y_{0}y_{1}\epsilon+y_{1}^{2}\epsilon^{2}+2y_{0}y_{2}\epsilon^{2}+...)+(2y_{0}\epsilon+2y_{1}\epsilon^{2}+...)-3=0\\
            \Rightarrow&(y_{0}^{2}-3)+(2y_{0}(y_{1}+1))\epsilon+(y_{0}^{2}+2y_{0}y_{1}+2y_{1})\epsilon^{2}+O(\epsilon^{3})=0
        \end{align*}
        Now we equate the coefficients of $\epsilon^{j}$ to zero and solve for $y_{i}$,
        \begin{align*}
            &y_{0}^{2}-3=0\\
            &2y_{0}(y_{1}+1)=0\\
            &y_{0}^{2}+2y_{0}y_{1}+2y_{1}=0\\
            &...
        \end{align*}
        From this we get,
        \[y_{0}=\pm\sqrt{3},\quad y_{1}=-1,\quad y_{2}=\pm\frac{1}{2\sqrt{3}},\quad ...\]
        Therefore we obtain two approximate solutions,
        \[y=\sqrt{3}-\epsilon+\frac{\epsilon^{2}}{2\sqrt{3}}+...\]
        \[y=-\sqrt{3}-\epsilon-\frac{\epsilon^{2}}{2\sqrt{3}}+...\]
    \end{exmp}

    \begin{exmp}
        \textit{Given the following IVP, where the parameter $\epsilon$ is considered to be small, find a regular 
        perturbation solution}
        \[y''+(y')^{2}+\epsilon y=0,\quad t>0,\quad 0<\epsilon<<1,\quad y(0)=0,\quad y'(0)=1\]
        Assume that a solution of the form,
        \[y=y_{0}+y_{1}\epsilon+...\]
        exists. Substituting this into the problem the ODE becomes,
        \begin{align*}
            &\frac{d^{2}}{dt^{2}}(y_{0}+y_{1}\epsilon+...)+\left(\frac{d}{dt}(y_{0}+y_{1}\epsilon+...)\right)^{2}+\epsilon(y_{0}+y_{1}\epsilon+...)=0\\
            \Rightarrow&(y''_{0}+y''_{1}\epsilon+...)+((y'_{0})^{2}+2y'_{0}y'_{1}\epsilon+...)+(y_{0}\epsilon+...)=0\\
            \Rightarrow&(y''_{0}+(y'_{0})^{2})+(y''_{1}+2y'_{0}y'_{1}+y_{0})\epsilon+O(\epsilon^{2})=0
        \end{align*}
        And the ICs become,
        \[y(0)=0\quad\Rightarrow\quad y_{0}(0)+y_{1}(0)\epsilon+...=0\quad\Rightarrow\quad y_{j}(0)=0\]
        \[y'(0)=1\quad\Rightarrow\quad y'_{0}(0)+y'_{1}(0)\epsilon+...=1\quad\Rightarrow\quad y'_{0}(0)=1,y'_{j>0}(0)=0\]
        Now equating the coefficients $e_{j}$ terms to 0 in the ODE we get,
        \begin{align*}
            &y''_{0}+(y'_{0})^{2}=0,\quad y_{0}(0)=0,y'_{0}(0)=1\\
            &y''_{1}+2y'_{0}y'_{1}+y_{0}=0,\quad y_{1}(0)=0,y'_{1}(0)=0 \textrm{ and } y_{0} \textrm{ is know from the preceeding problem}\\
            &...
        \end{align*}
    \end{exmp}

    \begin{defn}
        \textbf{Residual Error}:\newline
        Suppose that the problem has the form,
        \[F(t,y,y',y'',...)=0\]
        And that $y_{p}(t,\epsilon)$ is the (possibly truncated) perturbation solution, then the residual error is some function,
        \[r(t,\epsilon)=F(t,y_{p},y'_{p},y''_{p},...)\]
        that results from substituting $y_{p}$ into the problem.
    \end{defn}
    We can then use the residual error as some guide to the error in the approximate. We can do this by looking for the 
    uppoer and lower order of the residual error in terms of $\epsilon$ and for what interval the residual error is 
    uniformly convergent.

    \begin{defn}
        \textbf{Big Oh}:\newline
        \[f(t,\epsilon)=O(g(t,\epsilon)) \textrm{ as } \epsilon\rightarrow 0\]
        if there exists a positive constant $M$ such that,
        \[|f(t,\epsilon)|\le M|g(t,\epsilon)|\]
        Sometimes we might do this on a bounded domain of $t$ and $\epsilon$
    \end{defn}
    \begin{defn}
        \textbf{Little oh}:\newline
        \[f(t,\epsilon)=o(g(t,\epsilon)) \textrm{ as } \epsilon\rightarrow 0\]
        if,
        \[\lim_{\epsilon\rightarrow 0}\left|\frac{f(t,\epsilon)}{g(t,\epsilon)}\right|\]
        Sometimes we might do this on a bounded domain of $t$ and $\epsilon$
    \end{defn}

    \begin{exmp}
        From the example above, the leading order solution is $y_{0}(t)=\ln(t+1)$. Substituting this into the ODE we 
        get, $r(t,\epsilon)=\epsilon\ln(t+1)$.
        We can see that $r(t,\epsilon)=O(\epsilon)$ as $\epsilon\rightarrow 0$, but only if we restrict $t$ to a 
        finite domain say $t\in[0,T]$.
    \end{exmp}
    
    \subsection{WIP - Singular Perturbation Problems}

    A \textbf{singular perturbation} problem is one for which the regular perturbation method fails, because it cannot
    be uniformly approximated by a single asymptotic expansion over the whole domain. This means that the underlying 
    exact solution to the problem must have regions in which the scale of the problem is drastically different (or put
    another way the underlying exact solution has small regions where it changes rapidly).\newline
    \newline
    
    \begin{defn}
        \textbf{Boundary Layer}:\newline
        A boundary layer is a small region of the domain where the solution changes rapidly.
    \end{defn}
    \begin{defn}
        \textbf{Outer Layer}:\newline
        An outer layer is a large region of the domain where the solution changes 'slowly'.
    \end{defn}

    \subsubsection{Method of Matched Asymptotic Expansions}

    The Method of Matched Asymptotic Expansions looks to form an approximation to singular perturbation problems valid 
    over the whole domain of the problem, by finding an approximation for each bounadry/outer layer and then combining 
    these together. The combintation process is called \textbf{matching}.

    \begin{defn}
        \textbf{Inner Solution}:\newline
        An asymptotic approximation solution valid in a boundary layer.
    \end{defn}
    \begin{defn}
        \textbf{Outer Solution}:\newline
        An asymptotic approximation solution valid in an outer layer.
    \end{defn}

    \begin{figure*}[!htb]
        \centering
        \fbox{\includegraphics[width=0.9\textwidth]{images/singular_perturbation_method.png}}
    \end{figure*}
    \FloatBarrier

    \begin{strt}
        \textbf{Finding an Outer Solution}:\newline
        Use the regular perturbation method to obtain a solution for the problem. This is then the outer solution. Only 
        BCs that are present in the domain that the outer solution is valid should be applied.
    \end{strt}

    \begin{strt}
        \textbf{Finding an Inner Solution}:\newline
        Let $F(x,y,y',...,y^{(n)})=0$ be an ODE, and $x_{0}$ be the location of a boundary layer.
        \begin{itemize}
            \item \textbf{Rescale the problem}: Let $x=x_{0}+\delta(\epsilon)\xi$, and substitute into the problem 
            leading to $y(x_{0}+\delta(\epsilon)\xi)=Y(\xi)$, $\frac{d}{dx}y(x_{0}+\delta(\epsilon)\xi)=\frac{Y'(\xi)}{\delta(\epsilon)}$, etc.
            \[F\left(\xi,Y,\frac{Y'}{\delta(\epsilon)},...,\frac{Y^{(n)}}{\delta(\epsilon)}\right)=0\]
            \item \textbf{Use dominant balancing to find $\delta(\epsilon)$}: for each pair of coefficients in the problem, choose $\delta(\epsilon)$ so that the pair are of the same order. The pair is 
            a valid dominant balancing if the remaining coefficients (using the choosen $\delta(\epsilon)$) are of smaller order.\newline
            \newline
            For example: Suppose a problem has coefficients $c_{1}=\epsilon/\delta(\epsilon)$, $c_{2}=1/\delta(\epsilon)$, and $c_{3}=1$.
            \begin{itemize}
                \item Consider the pair $\{c_{1},c_{2}\}$. $\epsilon/\delta(\epsilon)\sim 1/\delta(\epsilon)\Rightarrow\delta(\epsilon)=\epsilon$. Looking at the remaining coefficient we have $c_{1}=1/\epsilon > 1=c_{3}$ so this is a dominant balancings pair.
                \item Consider the pair $\{c_{1},c_{3}\}$. $\epsilon/\delta(\epsilon)\sim 1\Rightarrow\delta(\epsilon)=\sqrt{\epsilon}$. Looking at the remaining coefficient we have $c_{1}=1 < 1/\sqrt{\epsilon}=c_{2}$, which is inconsistent and so this is not a dominant balancings pair.
            \end{itemize}
            \item Use the regular perturbation method to obtain a solution for the scaled problem. This is then the 
            inner solution. Note only the BC in the boundary layer should be applied to the inner solution.
        \end{itemize}
    \end{strt}

    
    \begin{exmp}
        \textit{Given the following problem, which has a boundary layer at $x=0$, find an inner solution for the boundary layer}
        \[\epsilon y''+(1+\epsilon)y'+y=0,\quad x\in(0,1),\quad y(0)=0,\quad y(1)=1\]
        Let $\xi=\frac{x}{\delta(\epsilon)}$, and so $Y(\xi)=y(\delta(\epsilon)\xi)$ then the ODE becomes,
        \[ \frac{\epsilon}{\delta(\epsilon)^{2}}Y''+\frac{1+\epsilon}{\delta(\epsilon)}Y'+Y=0 \]
        So the coefficients are,
        \[ \frac{\epsilon}{\delta(\epsilon)^{2}},\quad \frac{\epsilon}{\delta(\epsilon)},\quad \frac{1}{\delta(\epsilon)},\quad 1 \]
        If we look at all of the dominant balancings we find,
        \begin{itemize}
            \item Consider $\frac{\epsilon}{\delta(\epsilon)^{2}}$ and $\frac{\epsilon}{\delta(\epsilon)}$. There is
            no $\delta(\epsilon)$ that can make them the same order so this is inconsistent.
            \item Consider $\frac{\epsilon}{\delta(\epsilon)^{2}}$ and $\frac{1}{\delta(\epsilon)}$. We choose 
            $\delta(\epsilon)=\epsilon$ and so the pair have order $O(1/\epsilon)$. Since $\epsilon$ is small the order 
            is large and much bigger than the other coefficients.
            \item Consider $\frac{\epsilon}{\delta(\epsilon)^{2}}$ and 1. We choose $\delta(\epsilon)=\sqrt{\epsilon}$ 
            and so the dominant pair have order $O(1)$. But the other coefficients become 
            $\frac{\epsilon}{\sqrt{\epsilon}}$ and $\frac{1}{\sqrt{\epsilon}}$ which are of order bigger than $O(1)$ and
            so this is inconsistent (since the considered pair is not dominant).
            \item Continuing we find that there are no more dominant pairs.
        \end{itemize}
        Using the dominant balancing $\delta(\epsilon)=\epsilon$ the problem becomes,
        \[\frac{1}{\epsilon}Y''+(1+\epsilon)\frac{1}{\epsilon}Y'+Y=0\quad\Rightarrow\quad Y''+(1+\epsilon)Y'+\epsilon Y=0\]
        Now we can use the regular perturbation method to find a perturbation solution to this new problem. Assume a 
        solution of the form $Y_{0}+Y_{1}\epsilon+...$ and substituting in we get,
        \begin{align*}
            &(Y''_{0}+Y''_{1}\epsilon+...)+(1+\epsilon)(Y'_{0}+Y'_{1}\epsilon+...)+\epsilon(Y_{0}+Y_{1}\epsilon+...)=0\\
            \Rightarrow&(Y''_{0}+Y'_{0})+(Y''_{1}+Y'_{0}+Y'_{1}+Y_{0})\epsilon+O(\epsilon^{2})=0\\
        \end{align*}
        Equating the coefficients 0 we get,
        \begin{align*}
            &Y''_{0}+Y'_{0}=0\\
            &Y''_{1}+Y'_{0}+Y'_{1}+Y_{0}=0\\
            &...
        \end{align*}
        The leading order problem then has general solution,
        \[Y_{0}=c_{1}+c_{2}e^{-\xi}\]
    \end{exmp}

    \begin{prop*}
        \textbf{Matching Principles}:\newline        
        There are various matching principles and here we name some. First to setup some notation, let $x_{1}$ the 
        location of the boundary layer, let $Y=Y_{0}+Y_{1}\epsilon+...$ be the inner solution valid on the boundary layer
        $[c_{\epsilon},x_{1}]$ and $y=y_{0}+y_{1}\epsilon+...$ be the outer solution valid on the outer layer 
        $[x_{0},c_{\epsilon}]$. So here the outer layer is "to the left" of the boundary layer but it can be on the right.
        \begin{itemize}
            \item \textbf{Prandtl's Rule (leading-order matching)}: only valid when we truncate to the leading-order approximation.
            The matching principle is: \textit{the limit of the inner solution in the direction of the outer layer equals the limit
            of the outer solution in the direction of the boundary layer}. I.e. using the setup above,
            \[\lim_{\xi\rightarrow-\infty}Y_{0}=\lim_{x\rightarrow x_{1}}y_{0}\]
            \item \textbf{Intermediate Layer}:
            \item \textbf{Van Dyke pq-qp Rule}:
        \end{itemize}
    \end{prop*}

    \begin{strt}
        \textbf{Method of Matched Asymptotic Expansions}:\newline
        For each boundary layer (assuming that the boundary layer is ):
        \begin{itemize}
            \item Find the outer solution (applying a BC if there is an appropriate one).
            \item Find the inner solution (applying a BC if there is an appropriate one).
            \item Use a matching principle to choose coefficients of the solutions so they can be patched together.
            \item Form a composite solution using the inner and outer solutions, and there common limit.
        \end{itemize}
    \end{strt}

    \begin{prop*}
        \textbf{Locating the Boundary Layers}:\newline
        A singular perturbation problem may have multiple boundary layers, left, right, both, or interior. Sometimes we can
        identify the boundary layers from the problem itself. But frequently we can only find them through trail-and-error. 
        One assumes a boundary layer to exist at a location $x_{0}$, constructs general inner and outer solutions based 
        on this, and then looks at the limits of the solutions as they cross the boundary layer. If these limits can be 
        made equal/consistent using the remaining undetermined coefficients then a boundary layer exists, otherwise it doesn't.
    \end{prop*}

    \begin{exmp}
        \textit{Given the following ODE problem, find a leading order perturbation solution that is valid over the 
        whole domain,}\newline
        \[\epsilon y''+(1+\epsilon)y'+y=0,\quad y(0)=0,\quad y(1)=1\]
        Using the regular perturbation method we can show that the leading order outer solution has the general form,
        \[y=ce^{-x}\]
        And from a previous example using the singular perturbation method we found the leading order inner solution 
        to have the general form,
        \[Y=c_{1}+c_{2}e^{-\xi},\quad \textrm{where}\quad \xi=x/\epsilon\]
        Now to identify boundary layers and form inner solutions were appropriate,
        \begin{itemize}
            \item \textbf{Assume there is a boundary layer is at $x=0$}. The scaling factor is $x=\delta(\epsilon)\xi$ and 
            the problem becomes,
            \[ \epsilon \frac{Y''}{\delta^{2}}+(1+\epsilon)\frac{Y'}{\delta}+Y=0 \]
            This has dominant balancing $\delta(\epsilon)=\epsilon$ and we get,
            \[ Y''+(1+\epsilon)Y'+\epsilon Y=0 \]
            And the leading-order solution is,
            \[Y_{0}=c_{1}+c_{2}e^{-\xi}\]
            Now we consider $\lim_{x\rightarrow 0}y(x)=c$ and $\lim_{\xi\rightarrow \infty}Y(\xi)=c_{1}$. These can be 
            made to be consistent and so there is indeed a boundary layer at $x=0$
            \item \textbf{Assume there is a boundary layer is at $x=1$}. Here we will use $\bar{\xi}$ and $\bar{Y}$ to 
            show that these are a different variable and solution. The scaling factor is $x=1+\delta(\epsilon)\bar{\xi}$ and 
            the problem again becomes,
            \[ \epsilon \frac{\bar{Y}''}{\delta^{2}}+(1+\epsilon)\frac{\bar{Y}'}{\delta}+\bar{Y}=0 \]
            This has the same dominant balancing $\delta(\epsilon)=\epsilon$ and we get,
            \[ \bar{Y}''+(1+\epsilon)\bar{Y}'+\epsilon \bar{Y}=0 \]
            And the leading-order solution is,
            \[\bar{Y}_{0}=c_{1}+c_{2}e^{-\bar{\xi}}\]
            Now we consider the limits $\lim_{x\rightarrow 1}y(x)=c/e$ and $\lim_{\bar{\xi}\rightarrow -\infty}\bar{Y}(\bar{\xi})=\infty$. 
            These cannot be made consistently and so we don't have a boundary layer at $x=1$
        \end{itemize}
        Having identified the boundary layer we now apply the appropriate BC to each solution giving,
        \[y(1)=1\quad\Rightarrow\quad y(x)=e^{1-x}\]
        \[Y(0)=0\quad\Rightarrow\quad Y(\xi)=c_{1}(1-e^{-\xi})\]
        Since we are only matching the leading order solutions then we simply need to find $c_{1}$ to satisify,
        \[\lim_{x\rightarrow 0}y(x)=\lim_{\xi\rightarrow \infty}Y(\xi)\quad\Rightarrow\quad e=c_{1}\]
        Our inner and outer solutions respectively are,
        \[Y(\xi)=e(1-e^{-\xi}),\quad y(x)=e^{1-x}\]
        Finally to form a uniform approximation that is valid over the whole domain, we simply add the inner and outer 
        solutions and subtract the common limit
        \[y_{u}=y_{inner}+y_{outer}-common\;limit\]
        In our case the common limit is $common\;limit=e$ and so we get,
        \[y_{u}=e(1-e^{1-x/\epsilon})+e^{1-x}-e=e^{1-x/\epsilon}+e^{1-x}\]
    \end{exmp}



\end{document}