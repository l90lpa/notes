\documentclass[../main.tex]{subfiles}

\graphicspath{{\subfix{../images/}}}

\begin{document}

    \section{Analysis 2}

    \subsection{Convergence of sequences and series' of functions}

    \begin{theo}
        \textbf{Weierstrass M-Test}:\newline
        Let $\{f_{n}(x)\}$ be a sequence of real or complex valued functions defined on a set $D$. If the following two conditions hold,
        \begin{itemize}
            \item there exists constant bounds $M_n$ such that, $|f_{n}(x)|\le M_{n}$, for all $x\in D$ and $n\ge 1$
            \item $\sum^{\infty}_{n=1}M_{n}<\infty$
        \end{itemize}
        then the series,
        \[\sum^{\infty}_{n=1}f_{n}(x)\]
        converges absolutely and uniformly on $D$ to some function, say $f(x)$.
    \end{theo}
    \begin{coro}
        \textbf{Ratio Test}:\newline
        Let $\{f_{n}(x)\}$ be a sequence of real or complex valued functions defined on a set $D$. If there exists some 
        $M$ such that for all $x\in D$,
        \[\left|\frac{f_{n+1}(x)}{f_{n}(x)}\right| \le M<1\]
        then (it can be shown by the Weierstrass M-Test) the series,
        \[\sum^{\infty}_{n=1}f_{n}(x)\]
        converges absolutely and uniformly on $D$ to some function, say $f(x)$.
    \end{coro}
    \textbf{Remark}: if a finite number of bounded terms do not satisfy the hypothesis (ratio less than M) then these can be 
    ignored for the test and added to the series separately.

    \subsection{Term by term integration and differentiation}

    \begin{theo}
        Suppose that $f_n:[a, b]\rightarrow\mathbb{R}$, for each $n = 1, 2, ...$, is integrable on $[a, b]$ and that 
        $f_n(x) \rightarrow f(x)$ uniformly on $[a, b]$ as $n \rightarrow \infty$. Then the limit function $f(x)$ is 
        integrable on $[a, b]$ and $\int^b_af(x)dx = \lim_{n\rightarrow\infty} \int^b_af_n(x)dx$.
        \label{theo:termwise_integration_1}
    \end{theo}
    \textbf{Remark}: in theorem (\ref{theo:termwise_integration_1}) the condition of uniform continuity can be 
    replaced by the weaker requirement that $|f_n(x)|<1$ for $n$ and $x\in[a,b]$.
    \begin{theo}
        \textbf{Dominated Convergence Theorem}\newline
        Suppose that $f_{n}(x)$ is a sequence of continuous real or complex valued functions that converge pointwise to $f(x)$. 
        Also suppose that $f_{n}(x)$ is dominated by an integrable function $g(x)$ in the sense that, $|f_{n}(x)|<g(x)$
        for all $n$ in the index and $x\in S$. Then $f$ is integrable (in the sense of Lesbegue) and 
        \[\lim_{n\rightarrow\infty}\int_{S}|f_{n}-f| \; dx=0\]
        which implies
        \[\lim_{n\rightarrow\infty}\int_{S}f_{n} \; dx=\int_{S}f \; dx\]
    \end{theo}
    \textbf{Remark}: practically theorem (\ref{theo:termwise_integration_1}) and the Dominated Convergence Theorem give 
    sufficient conditions for being able to interchange the limit and the integral, i.e. 
    $\lim_{n\rightarrow\infty}\int_{S}f_{n} \; dx=\int_{S}\lim_{n\rightarrow\infty}f_{n} \; dx$

    \begin{theo}
        (\textbf{Termwise integration})\newline
        Suppose that $u_k:[a, b]\rightarrow\mathbb{R}$, for each $k = 1, 2, ...$, is integrable on $[a, b]$ and that 
        $\sum^\infty_{k=1}u_k(x)$ converges uniformly on $[a, b]$ to $f(x)$. Then the limit function $f(x)=\sum^\infty_{k=1}u_k(x)$ is 
        integrable on $[a, b]$ and $\int^b_af(x)dx = \sum^\infty_{k=1} \int^b_au_k(x)dx$.
        \label{theo:termwise_integration_2}
    \end{theo}

    \begin{theo}
        (\textbf{Termwise differentiation})\newline
        Suppose that $u_k:[a, b]\rightarrow\mathbb{R}$, for each $k = 1, 2, ...$, has continuous derivative on $[a, b]$ 
        (at the end-points a and b this means one-sided derivative) and also that:
        \begin{itemize}
            \item the series $\sum^\infty_{k=1}u_k(x_0)$ converges at some point $x_0\in[a,b]$
            \item the series of derivatives $\sum^\infty_{k=1}u'_k(x)$ converges uniformly on $[a,b]$, to say $f(x)=\sum^\infty_{k=1}u'_k(x)$
        \end{itemize} 
        Then,
        \begin{enumerate}
            \item the series $\sum^\infty_{k=1}u_k(x)$ converges at every point $x\in[a,b]$ and the sum $F(x)=\sum^\infty_{k=1}u_k(x)$ is
            differentiable with $F'(x)=f(x)$ for each $x\in[a,b]$
            \item the convergence of $\sum^\infty_{k=1}u_k(x)$ to $F(x)$ is uniform on $[a,b]$
        \end{enumerate}
        \label{theo:termwise_differentiation}
    \end{theo}

    \begin{coro}
        Suppose that $f_n : [a, b] \rightarrow \mathbb{R}$, for each $n = 1, 2, ...$, has continuous derivative on
        $[a, b]$ and $f_n(x) \rightarrow f(x)$ point-wise for each $x \in [a, b]$. If $f'_n(x) \rightarrow \psi(x)$ 
        uniformly on $[a, b]$ then the function $f$ is differentiable with $f'(x) = \psi(x)$ for each $x \in [a, b]$. 
        Moreover, $f'_n \rightarrow f'$ uniformly on $[a, b]$.
        \label{coro:termwise_differentiation}
    \end{coro}
    \textbf{Remark}: the condition that the derivatives, $u'_k(x)$, in theorem (\ref{theo:termwise_differentiation}) 
    (and respectively $f'_n(x)$, in corollary (\ref{coro:termwise_differentiation})) be continuous can be dropped. See Rudin's book.


    \subsection{Metric Spaces}

    \begin{defn}
        \textbf{Cauchy Sequence (Metric Space)}\newline
        The sequence $\{x_n\}$ in the metric space $(X,\rho)$ is called a \textbf{Cauchy sequence} if, for each $\epsilon >0$, there exists an index $N$ such that,
        \[n,m\ge N\quad\Rightarrow\quad\rho(x_n,x_m)<\epsilon\]
    \end{defn}
    Note that in a metric space any convergent sequence is a Cauchy sequence, and any Cauchy sequence is bounded.

    \begin{defn}
        \textbf{Complete}\newline
        A metric space, $(X,\rho)$, is said to be \textbf{complete}, so long as every Cauchy sequence in $X$ converges to a point in $X$.
    \end{defn}

    \begin{defn}
        \textbf{Lipschitz Continuity (Metric Space)}:\newline
        Given the metric spaces $(X,\rho_X)$ and $(Y,\rho_Y)$ (with metrics, $\rho_X$ and $\rho_Y$ respectively), and the function $T:X\rightarrow Y$, if there is a constant $C\ge 0$ such that $\rho_Y(T(x_1),T(x_2))\le C\rho_X(x_1,x_2)$ for all $x_1,x_2\in X$, then $T$ is called \textbf{Lipschitz continuous} (or \textbf{Lipschitz mapping}/\textbf{Lipschitz}). The smallest $C$ for which this holds is called the \textbf{Lipschitz constant}.
    \end{defn}

    \begin{defn}
        \textbf{Contraction Mapping}\newline
        Let $T$ be a Lipschitz mapping from the metric space $(X,\rho)$ onto itself, with Lipschitz constant $C$. If $C< 1$ then $T$ is called a \textbf{contraction mapping} (or a \textbf{contraction}).
    \end{defn}

    \begin{theo}
        \textbf{Banach Fixed Point Theorem (Contraction Mapping Theorem)}\newline
        Let $X$ be a complete metric space and the function $T:X\rightarrow X$ a contraction. Then $T$ has exactly one fixed point i.e $x\in X$ such that $T(x)=x$ is unique.
    \end{theo}

    \subsection{Banach Space}

    \begin{defn}
        \textbf{Banach Space}\newline
        A normed vector space (aka normed linear space), that is also a complete metric space with the metric induced by the norm, is called a \textbf{Banach space}.
    \end{defn}
    Let $\Vert\cdot\Vert$ be the norm of some Banach space, the induced metric is then defined to be, $\rho(x,y)=\Vert x-y \Vert$

    \subsection{Gateaux differentials and Frechet derivatives}

    \subsubsection{Gateaux differential}

    \begin{defn}
        \textbf{Gateaux differential}\newline
        Let $V$ and $U$ be  locally convex topological vector spaces, and $f : V \rightarrow U$. The Gateaux differential $d_hf$ of $f$ at $x\in V$ in the direction of $h\in V\backslash \{0\}$ is defined as,
        \[d_hf=\lim_{\epsilon\rightarrow 0}\frac{f(x+\epsilon h)-f(x)}{\epsilon}\]
    \end{defn}
    The Gateaux differential can be thought of as a generalization of the directional derivative.
    \newline
    \newline
    The Gateaux differential satisfies analogous rules to those of ordinary calculus. Let $c$ be a constant, and $f$ and $g$ be functionals, then these rules are:
    \begin{itemize}
        \item \textbf{differential of a constant}: $d_hc=0$
        \item \textbf{sum rule}: $d_h(f\pm g)=d_hf\pm d_hg$
        \item \textbf{product rule}: $d_h(fg)=(d_hf)g + f(d_hg)$
        \item \textbf{inner product rule}: $d_h\langle f,g \rangle=\langle f,d_hg\rangle + \langle d_hf,g\rangle$
        \item \textbf{chain rule}: $d_h(f(g(x))=d_{d_hg}f(g)$
    \end{itemize}
    \begin{exmp}
        \textit{Find the Gateaux differential for each of the following:}\newline
        \textit{(a)} $f(x)=\langle x,p \rangle$, \textit{where} $x$ \textit{and} $p$ \textit{are vectors in an inner product}\newline
        \textit{(b)} $e^u$, \textit{where} $u(x)$ \textit{is a function}\newline
        \textit{(c)} $f(x)=|x|$, \textit{where} $x\in\mathbb{R}$\newline
        \textit{(d)} $\frac{du}{dx}$\newline
        \textit{(e)} $J:H^1(\Omega)\rightarrow\mathbb{R}$ \textit{where} $J[u]=\int_\Omega\left[\frac{1}{2}u^2_x+\frac{1}{2}u^2\right]\;dx$\newline
        \newline
        (a) \newline
        \begin{align*}
            d_hf&=\lim_{\epsilon\rightarrow 0}\frac{\langle x+\epsilon h,p \rangle-\langle x,p \rangle}{\epsilon}\\
            &=\lim_{\epsilon\rightarrow 0}\frac{\langle x,p \rangle + \epsilon\langle h,p \rangle-\langle x,p \rangle}{\epsilon}\\
            &=\langle h,p \rangle
        \end{align*}
        (b) \newline
        \begin{align*}
            d_h(e^u)&=\lim_{\epsilon\rightarrow 0}\frac{e^{u+\epsilon h}-e^u}{\epsilon}\\
            &=e^{u}\lim_{\epsilon\rightarrow 0}\frac{e^{\epsilon h}-1}{\epsilon}\\
            &=e^u\lim_{\epsilon\rightarrow 0}h\left(1+\epsilon h+\frac{(\epsilon h)^2}{2!}+...\right),\quad\textrm{using }e^x=\sum^\infty_{n=0}\frac{x^n}{n!}\\
            &=he^{u}
        \end{align*}
        (c) \newline
        First note that, $|x|=(x^2)^{1/2}$. Now let $p(x)=x^{1/2}$ and $q(x)=x^2$, then $|x|=p(q(x))$. Next the Gateaux differential of these are,
        \[d_hp=\frac{1}{2}hx^{-1/2}\] 
        \[d_hq=2hx\]
        For $x\neq 0$, by the chain rule we get,
        \begin{align*}
            d_h(p(q(x)))&=d_{d_hq}p(q)\\
            &=d_{2hx}q^{1/2}\\
            &=\frac{1}{2}2hxq^{-1/2}\\
            &=h\frac{x}{|x|},\quad\textrm{using }q=x^2\Rightarrow q^{-1/2}=\frac{1}{|x|}
        \end{align*}
        For $x=0$, we get,
        \[d_h|x|=\lim_{\epsilon\rightarrow 0}\frac{|x+\epsilon h|-|x|}{\epsilon}=\lim_{\epsilon\rightarrow 0}\frac{\epsilon|h|}{\epsilon}=|h|,\quad\textrm{using }x=0\]
        (d) \newline
        \begin{align*}
            d_h\left(\frac{du}{dx}\right)&=\lim_{\epsilon\rightarrow 0}\frac{u_x+\epsilon h_x-u_x}{\epsilon}\\
            &=\frac{dh}{dx}
        \end{align*}
        (e) \newline
        \begin{align*}
            d_hJ&=\lim_{\epsilon\rightarrow 0}\frac{\int_\Omega\left[\frac{1}{2}(u^2_x+2\epsilon h_xu_x+\epsilon^2 h_x^2)-\frac{1}{2}u_x^2+\frac{1}{2}(u^2+2\epsilon hu+\epsilon^2 h^2)-\frac{1}{2}u^2\right]\;dx}{\epsilon}\\
            &=\lim_{\epsilon\rightarrow 0}\frac{\epsilon\int_\Omega\left[h_xu_x+ hu\right]\;dx+\frac{1}{2}\epsilon^2\int_\Omega\left[h_x^2+h^2\right]\;dx}{\epsilon}\\
            &=\int_\Omega\left[h_xu_x+ hu\right]\;dx
        \end{align*}
    \end{exmp}

    \subsubsection{Frechet derivative}

    \begin{defn}
        \textbf{Frechet derivative}\newline
        Let $V$ and $U$ be normed vector space, and $f : V \rightarrow U$. The function $f$ is said to be Frechet differentiable at $x\in V$ if there exists a bounded linear operator $A:V\rightarrow U$ such that,
        \[\lim_{\Vert k\Vert\rightarrow 0}\frac{\Vert f(x+k)-f(x)-Ak\Vert_{U}}{\Vert k\Vert_{V}}=0\]
        Equivalently, in little-o notation this condition can be written as,
        \[f(x+k)=f(x)+Ak+o(k)\]
        If $A$ exists then it is called the \textbf{Frechet derivative} and we write, $Df=A$
    \end{defn}
    The Frechet derivative can be thought of as a generalization of the gradient operator (like the Jacobian can be thought of as a generalization of the gradient operator from scalar-valued to vector-valued functions).
    \newline
    \newline
    The relationship bewtween Frechet derivative and the Gateaux differential can be seen from the following. Let $k=\epsilon h$, then in the limit $\epsilon\rightarrow 0$ we have $(Df)h=d_hf$. Therefore, if $d_hf$ has the form $Ah$, then we can identify $Df=A$. It is hence worth noting that this shows that not all Gateaux differentiable functions are Frechet differentiable.
    \begin{theo}
        The Frechet derivative exists at $x=a$, if and only if, all the Gateaux differentials are continuous functions of $x$ at $x=a$.
    \end{theo}
    \begin{theo}
        If the Frechet derivative exists at $x=a$ for function $f$, then the Frechet derivative is unique.
    \end{theo}

    \subsubsection{Frechet derivative: Finite Dimensional Spaces}
    The Frechet derivative in finite-dimensional spaces can be represented in coordinates by the Jacobian matrix. 
    \newline
    \newline
    For example, let $f:V\subseteq\mathbb{R}^n\rightarrow\mathbb{R}^m$, where $V$ is an open set, be Frechet differentiable at $a\in V$. The Frechet derivative of $f$ is then the mapping $Df(a):\mathbb{R}^n\rightarrow\mathbb{R}^m$ such that $Df(a)(v)=J_f(a)v$, where $J_f(a)\in\mathbb{R}^{m\times n}$ is the Jacobian matrix of $f$ at $a$. Furthermore the partial derivatives of $f$ are,
    \[\frac{df}{dx_i}(a)=Df(d)(e_i)=J_f(a)e_i\]
    where $e_i$ are vectors of the canonical $\mathbb{R}^n$ basis.
    \newline
    \newline
    Note that, when $m=1$ the Jacobian matrix reduces to the gradient vector $(\nabla f(a))^T$.

    \subsubsection{WIP - Frechet derivative: Tangent Hyperplane}
    Two functions are tangent at a point if they concide at that point and their respective Frechet derivative's are equal at that point.

    Consider the scalar-valued function $f:V\rightarrow \mathbb{R}$, and suppose that the Frechet derivative of $f$ at $x=a$ exists. To first order approximation we have,
    \[f(a+h)\approx f(a)+(Df(a))h,\quad\textrm{by definition}\]  
    Let $g(h)=f(a)+(Df(a))h$, then since $Df(a)$ is a linear operator $g$ is affine and so it defines a hyperplane, specifically the set,
    \[H=\left(\begin{matrix}
        h-a\\
        g(h)
    \end{matrix}\right)+\{h\;|\;(Df(a))h=0\}\]
    From this we see that, $g(0)=f(a)$, and $Dg(0)=Df(a)$ therefore the hyperplane is tangential to the surface defined by $f$ at $x=a$.

    \subsection{WIP - Newton's Method in Banach Spaces}


\end{document}