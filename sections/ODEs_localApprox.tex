\documentclass[../main.tex]{subfiles}

\graphicspath{{\subfix{../images/}}}

\begin{document}
    \section{ODEs: Local Approximation Methods (Local Analysis)}

    Many ODEs cannot be solved in closed-form, or their closed form solutions aren't insightful. Here, we present some methods for approximating the solution of an ODE local to some point.

    The approximation will be expressed as an infinite series expansion. And, the general approach to finding this approximation is to, 1) classify the point, 2) choose a suitable form for the infinite series expansion based on the classification of the point, and 3) solving for the unknown coefficients in the series.

    \subsection{Classification of Points of Homogenous Linear ODEs}

    \begin{defn}
        Given some linear homogenous ODE,
        \[y^{(n)}(x)+p_{n-1}y^{(n-1)}(x)+\cdots+p_{1}y^{(1)}(x)+p_{0}y(x)=0\]
        A point $x_0$ can be classified as one of the following:
        \begin{itemize}
            \item \textbf{Ordinary Point}: the point $x_0$ is an ordinary point if, all the coefficient functions, $p_k(x)$, are analytic in some neighborhood about $x_0$ in the complex plane.
            \item \textbf{Regular Singular Point}: the point $x_0$ is a regular singular point if, some of the coefficient functions, $p_k(x)$, are not analytic in some neighborhood about $x_0$ in the complex plane, but all of the functions 
            \[(x-x_0)^np_0(x),(x-x_0)^{n-1}p_1(x),...,(x-x_0)p_{n-1}(x)\]
            are analytic in a neighborhood about $x_0$ in the complex plane.
            \item \textbf{Irregular Singular Point}: the point $x_0$ is an irregular singular point, if it is neither an ordinary point or a regular singular point.
        \end{itemize}
    \end{defn}

    \subsection{WIP - Local Approximation Methods}
    
    \subsubsection{Power Series Method - Ordinary Points}

    Given some homogenous linear ODE,
    \[y^{(n)}(x)+u(x)y^{(n-1)}(x)+\cdots+v(x)y^{(1)}(x)+w(x)y(x)=0\]
    And, some an ordinary point, $x_0$, about which we want to construct a local approaximation. We construct the local approximation as follows:

    \begin{itemize}
        \item Replace the coefficient functions by their respective Taylor series about the point $x_0$,
        \[y^{(n)}(x)+y^{(n-1)}(x)\sum^{\infty}_{m=0}u_m(x-x_0)^m+\cdots+y^{(1)}(x)\sum^{\infty}_{m=0}v_m(x-x_0)^m+y(x)\sum^{\infty}_{m=0}w_m(x-x_0)^m=0\]
        \item Assume that the solution to the problem has the form,
        \[y(x)=\sum^{\infty}_{m=0}a_m(x-x_0)^m\]
        \item Differentiate the power series for $y(x)$, and substitute into the ODE, 
        \[y^{(k)}(x)=\frac{d^{k}}{dx^k}\sum^{\infty}_{m=0}a_m(x-x_0)^m=\sum^{\infty}_{m=0}\frac{m!}{(m-k)!}a_m(x-x_0)^{m-k}\]
        \item Solve for the coefficients of the series. This usually take the form:
        \begin{itemize}
            \item Re-index sums as necessary to combine terms and simplify the expression. Generally a good choice of re-indexing is to choose the initial summation index such that the exponent of $x$ has no additive/subtractive constants. E.g. re-index $\sum^{\infty}_{m=0}x^{m+1}$ to $\sum^{\infty}_{m=1}x^{m}$.
            \item Collect coefficients of like powers of $x$ and equate these equal to zero. This should establish a collection of recurrence relations that define the unknown constants.
            \item Find the closed form for each the recurrence relations (i.e. the defintions of the coefficients).
        \end{itemize}
    \end{itemize}

    \begin{exmp}
        \textit{Given the following ODE find a solution using the Power Series Method}
        \[x^2y''+xy'+x^2y=0\]
        First we assume a solution of the form,
        \[y(x)=\sum^{\infty}_{m=0}a_mx^m\]
        From this we have that,
        \[y'(x)=\sum^{\infty}_{m=1}ma_mx^{m-1},\quad y''(x)=\sum^{\infty}_{m=2}m(m-1)a_mx^{m-2}\]
        Substituting this into the ODE we get,
        \begin{align*}
            &x^2\sum^{\infty}_{m=2}m(m-1)a_mx^{m-2}+x\sum^{\infty}_{m=1}ma_mx^{m-1}+x^2\sum^{\infty}_{m=0}a_mx^m=0\\
            \Rightarrow&\sum^{\infty}_{m=2}m(m-1)a_mx^{m}+\sum^{\infty}_{m=1}ma_mx^{m}+\sum^{\infty}_{m=0}a_mx^{m+2}=0\\
            \Rightarrow&\sum^{\infty}_{m=2}m(m-1)a_mx^{m}+\sum^{\infty}_{m=1}ma_mx^{m}+\sum^{\infty}_{m=2}a_{m-2}x^{m}=0\\
            \Rightarrow&\sum^{\infty}_{m=2}m(m-1)a_mx^{m}+a_1x+\sum^{\infty}_{m=2}ma_mx^{m}+\sum^{\infty}_{m=2}a_{m-2}x^{m}=0\\
            \Rightarrow&a_1x+\sum^{\infty}_{m=2}(m(m-1)a_m+a_{m-2}+ma_m)x^{m}=0
        \end{align*}
        For this to be true for all $x$ then we must have,
        \[a_1=0\]
        \[m(m-1)a_m+a_{m-2}+ma_m=0,\quad\textrm{for }n=0,1,2,...\]
        Considering the second equation we find the recurrence relation,
        \[m(m-1)a_m+a_{m-2}+ma_m=0\quad\Rightarrow\quad a_m=-\frac{1}{m^2}a_{m-2}\]
        Since $a_1=0$ then, by the recurrence relation, all of the odd terms must be zero. Furthermore, it is simple to see that the recurrence relation has the closed form,
        \[a_{2k}=\frac{(-1)^k}{(2)^{2k}(k!)^2}a_0\]
        Therefore the general solution has the form,
        \[y(x)=\sum^{\infty}_{m=0}\frac{(-1)^m}{(2)^{2m}(m!)^2}a_0x^{2m}\]
    \end{exmp}

    \subsubsection{Frobenius Method - Regular Singular Points}

    The Frobenius method, is an extension of the power series method to finding a local approaximation to an ODE about a regular singluar point.

    Given some homogenous linear ODE,
    \[y^{(n)}(x)+u(x)y^{(n-1)}(x)+\cdots+v(x)y^{(1)}(x)+w(x)y(x)=0\]
    And, some an regular singular point, $x_0$, about which we want to construct a local approaximation. We construct the local approximation as follows:

    \begin{itemize}
        \item First, because we are at a regular singluar point, then we know that the coefficient fucntions are analytic if multiplied by an appropriate functions. Therefore we rewrite the ODE as,
        \[y^{(n)}(x)+\frac{U(x)}{(x-x_0)}y^{(n-1)}(x)+\cdots+\frac{V(x)}{(x-x_0)^{n-1}}y^{(1)}(x)+\frac{W(x)}{(x-x_0)^n}y(x)=0\]
        where,
        \[u(x)=\frac{U(x)}{(x-x_0)},\quad v(x)=\frac{V(x)}{(x-x_0)^{n-1}},\quad w(x)=\frac{W(x)}{(x-x_0)^n}\]
        \item Replace the functions, $U(x),V(x)\textrm{ and }W(x)$, by their respective Taylor series about the point $x_0$,
        \[y^{(n)}(x)+y^{(n-1)}(x)\sum^{\infty}_{m=0}U_m(x-x_0)^{m-1}+\cdots+y^{(1)}(x)\sum^{\infty}_{m=0}V_m(x-x_0)^{m-(n-1)}+y(x)\sum^{\infty}_{m=0}W_m(x-x_0)^{m-n}=0\]
        \item Assume that the solution to the problem has the form,
        \[y(x)=(x-x_0)^r\sum^{\infty}_{m=0}a_m(x-x_0)^m=\sum^{\infty}_{m=0}a_m(x-x_0)^{m+r}\]
        \item Differentiate the power series for $y(x)$, and substitute into the ODE, 
        \[y^{(k)}(x)=\frac{d^{k}}{dx^k}\sum^{\infty}_{m=0}a_m(x-x_0)^{m+r}=\sum^{\infty}_{m=0}\frac{(m+r)!}{(m+r-k)!}a_m(x-x_0)^{m+r-k}\]
        \item Solve for the coefficients of the series. This usually take the form:
        \begin{itemize}
            \item Re-index sums as necessary to combine terms and simplify the expression. Generally a good choice of re-indexing is to choose the initial summation index such that the exponent of $x$ has no additive/subtractive constants. E.g. re-index $\sum^{\infty}_{m=0}x^{m+1}$ to $\sum^{\infty}_{m=1}x^{m}$.
            \item Collect coefficients of like powers of $x$ and equate these equal to zero. This should establish a collection of recurrence relations that define the unknown constants.
            \item Find the closed form for each the recurrence relations (i.e. the defintions of the coefficients).
        \end{itemize}
    \end{itemize}

    \begin{exmp}
        \textit{Given the following ODE, that has a regular singular point at $x_0$, find its solutions using the Frobenius Method about $x_0$}
        \[a(x)y'''+b(x)y''+c(x)y'+d(x)y=0\]
        Because it has a regular singular point at $x_0$, we can rewrite the coefficient functions such that ODE has the form,
        \[y'''+\frac{B(x)}{(x-x_0)}y''+\frac{C(x)}{(x-x_0)^2}y'+\frac{D(x)}{(x-x_0)^3}y=0\]
        where, $B=b/a$, $C=c/a$, and $D=d/a$ are all analytic in some neighbourhood about $x_0$. Now, we assume a solution of the form,
        \[y(x)=\sum^{\infty}_{m=0}y_m(x-x_0)^{m+r}\]
        From this we have that,
        \[y^{(k)}(x)=\sum^{\infty}_{m=0}\frac{(m+r)!}{(m+r-k)!}a_m(x-x_0)^{m+r-k}\]
        Substituting te assumed solution, as well as the power series for $A$, $B$, $C$, and $D$ about $x_0$ into the ODE we get,
        \begin{align*}
            \sum^{\infty}_{m=0}\frac{(m+r)!}{(m+r-3)!}y_m(x-x_0)^{m+r-3}+\\
            \frac{1}{(x-x_0)}\left(\sum^{\infty}_{m=0}B_m(x-x_0)^{m}\right)\left(\sum^{\infty}_{m=0}\frac{(m+r)!}{(m+r-2)!}y_m(x-x_0)^{m+r-2}\right)+\\
            \frac{1}{(x-x_0)^2}\left(\sum^{\infty}_{m=0}C_m(x-x_0)^{m}\right)\left(\sum^{\infty}_{m=0}\frac{(m+r)!}{(m+r-1)!}y_m(x-x_0)^{m+r-1}\right)+\\
            \frac{1}{(x-x_0)^3}\left(\sum^{\infty}_{m=0}D_m(x-x_0)^{m}\right)\left(\sum^{\infty}_{m=0}y_m(x-x_0)^{m+r}\right)=0
        \end{align*}
        And by the Cauchy product formula this becomes,
        \begin{align*}
            \sum^{\infty}_{m=0}\frac{(m+r)!}{(m+r-3)!}y_m(x-x_0)^{m+r-3}+\\
            \sum^{\infty}_{m=0}(x-x_0)^{m+r-3}\sum^{m}_{k=0}\frac{(k+r)!}{(k+r-2)!}B_{m-k}y_{k}+\\
            \sum^{\infty}_{m=0}(x-x_0)^{m+r-3}\sum^{m}_{k=0}\frac{(k+r)!}{(k+r-1)!}C_{m-k}y_{k}+\\
            \sum^{\infty}_{m=0}(x-x_0)^{m+r-3}\sum^{m}_{k=0}D_{m-k}y_{k}=0
        \end{align*}
        Which simplifies to,
        \[
            \sum^{\infty}_{m=0}(x-x_0)^{m+r-3}\left(\frac{(m+r)!}{(m+r-3)!}y_m+\sum^{m}_{k=0}\left(
            \frac{(k+r)!}{(k+r-2)!}B_{m-k}+
            \frac{(k+r)!}{(k+r-1)!}C_{m-k}+
            D_{m-k}\right)y_k\right)=0
        \]
        When $m=0$ the coefficient of $(x-x_0)^{r-3}$ is,
        \begin{align*}
            &\left(r(r-1)(r-2)+r(r-1)B_{0}+rC_{0}+D_{0}\right)y_0\\
            \Rightarrow&\left((r^3-3r^2+2r)+(r^2-r)B_{0}+rC_{0}+D_{0}\right)y_0\\
            \Rightarrow&\left(r^3+(B_{0}-3)r^2+(C_{0}+B_{0}+2)r+D_{0}\right)y_0
        \end{align*}
        And for $m=1,2,3,...$ the coefficients of $(x-x_0)^{m+r-3}$ are,
        \begin{align*}
            \left((m+r)^3+(B_{0}-3)(m+r)^2+(C_{0}+B_{0}+2)(m+r)+D_{0}\right)y_m+\\
            \sum^{m-1}_{k=0}\left(B_{m-k}(m+r)^2+(C_{m-k}-B_{m-k})(m+r)+D_{m-k}\right)y_k
        \end{align*}
        The solution, when plugged into the ODE must equal zero for aribtrary $x$, hence we must have,
        \[\left(r^3+(B_{0}-3)r^2+(C_{0}+B_{0}+2)r+D_{0}\right)y_0=0\]
        and 
        \begin{align*}
            \left((m+r)^3+(B_{0}-3)(m+r)^2+(C_{0}+B_{0}+2)(m+r)+D_{0}\right)y_m+\\
            \sum^{m-1}_{k=0}\left(B_{m-k}(m+r)^2+(C_{m-k}-B_{m-k})(m+r)+D_{m-k}\right)y_k=0
        \end{align*}
        From the first equation, if we assume that $y_0$ is non-zero then find have that,
        \[r^3+(B_{0}-3)r^2+(C_{0}+B_{0}+2)r+D_{0}=0\]
        This is called the indicial equation, and we will define it as $P(r)=0$. Using this we can write the second equation as,
        \[P(m+r)y_m=-\sum^{m-1}_{k=0}\left(B_{m-k}(m+r)^2+(C_{m-k}-B_{m-k})(m+r)+D_{m-k}\right)y_k\]
        At this point we will enumerate the possible solution structures, under the assumption that the roots satisfy $Re(r_1)\ge Re(r_2)\ge Re(r_3)$:
        \begin{itemize}
            \item first, there is always a solution in the form of a Frobenius series for $r_1$.
            \item if $r_1-r_2$ is not an integer: then there is a second linearly independent solution in the form of a Frobenius series for $r_2$.
            \item if $r_1-r_2$ is an integer:
            \begin{itemize}
                \item if $r_1-r_2=0$: then a second linearly independent solution can be constructed from the first solution by differentiating the first solution wrt $r$ and setting $r=r_1$.
                \item if $r_1-r_2=1,2,3,...$: then a second linearly independent solution can be constructed 
            \end{itemize} 
        \end{itemize}
    \end{exmp}
\end{document}